-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Nov 15, 2010 at 07:42 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `booix`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(50) default NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `registration_date` date default NULL,
  `last_login_date` date default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `users`
-- 

INSERT INTO `users` VALUES (1, NULL, '', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL);
INSERT INTO `users` VALUES (2, NULL, '', 'seba@gmail.com', '1165b05ff34126296e4c3aab150ffdcb', NULL, NULL);
INSERT INTO `users` VALUES (3, NULL, '', 'seba2@gmail.com', 'a44c69e0de90cd776b9f36adab701d98', NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `webpages`
-- 

DROP TABLE IF EXISTS `webpages`;
CREATE TABLE `webpages` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `subdomain` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

-- 
-- Dumping data for table `webpages`
-- 

INSERT INTO `webpages` VALUES (14, 2, 'seba');
INSERT INTO `webpages` VALUES (26, 3, 'seba2');
