# Booix

## Description
Booix is a website builder experiment using GWT and PHP backend. At the time of working on this I wasn't sure
of what to use for the backend, and jumped into implementing it in PHP. I think that was  bad decision. Bet option
is java, and now looking at what heroku has to offer, it would be great to change it to a java server app.

Anyways, feel free to play with this code and if you want to collaborate and make it better just let me know

## Story behind booix experiment
This project was an attempt to come up with something when I learned about www.jimdo.com. I wanted to try and see what
I could come up with. Later I realized it was a huge project and working few hours a day I would not get that far.

## Demo
I have a very rustic demo of this app running here: www.booix.com.

## Contact Info
email: sebapereyro@gmail.com