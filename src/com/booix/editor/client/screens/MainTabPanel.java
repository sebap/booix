package com.booix.editor.client.screens;

import com.booix.editor.client.AppService;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.StackPanel;


public class MainTabPanel extends Composite {
	
	private StackPanel tabPanel = new StackPanel();
	private TemplatesTabPanel templatesTabPanel = new TemplatesTabPanel();
	private WidgetsTabPanel widgetsTabPanel =  new WidgetsTabPanel();

	@UiConstructor
	public MainTabPanel() {
		initWidget(tabPanel);
		tabPanel.add(templatesTabPanel, AppService.get().getMessages().getTemplates());
		tabPanel.add(widgetsTabPanel, AppService.get().getMessages().getComponents());
	}
	
	public TemplatesTabPanel getTemplatesTabPanel() {
		return templatesTabPanel;
	}
	
	public WidgetsTabPanel getWidgetsTabPanel() {
		return widgetsTabPanel;
	}

}
