package com.booix.editor.client.screens;

import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.event.TemplateRetrievedEvent;
import com.booix.editor.client.event.TemplateRetrievedEventHandler;
import com.booix.editor.client.event.WebPageRetrievedEvent;
import com.booix.editor.client.event.WebPageRetrievedEventHandler;
import com.booix.editor.client.screens.settings.AccountSettingsScreen;
import com.booix.editor.client.screens.settings.WebPageSettingsScreen;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class AppScreen extends Composite implements TemplateRetrievedEventHandler, WebPageRetrievedEventHandler{

	private static AppScreenUIBinder uiBinder = GWT.create(AppScreenUIBinder.class);

	interface AppScreenUIBinder extends UiBinder<Widget, AppScreen> {
	}
	@UiField protected MenuBar menuBar;
	@UiField protected MenuItem accountMenuItem;
	//@UiField protected MenuItem webPagesMenuItem; //all the webpages for this account
	@UiField protected MenuItem webPageMenuItem; //current webpage
	@UiField protected MenuItem webPageReloadMenuItem;
	@UiField protected MenuItem webPageSaveMenuItem;
	@UiField protected MenuItem webPageSettingsMenuItem;
	
	@UiField protected MenuItem helpMenuItem;
	@UiField protected MenuItem aboutMenuItem;
	@UiField protected MenuItem signOutMenuItem; 
	
	@UiField protected MenuItem developmentMenuItem;
	@UiField protected MenuItem showHideDivLoggerMenuItem;
	@UiField protected TopPanel topPanel;
	@UiField protected HTML breadcrumbsTxt;
	
	//@UiField protected Button showHideBtn; 
	@UiField protected SimplePanel mainPanel;
	@UiField protected SimplePanel leftPanel;
	
	protected WebPageWidgetEditor webPageEditor;
	
	private AccountSettingsScreen accountSettingsScreen = null;
	private WebPageSettingsScreen webPageSettingsScreen = null;
	
	@UiConstructor
	public AppScreen() {
		initWidget(uiBinder.createAndBindUi(this));
		AppService.get().getEventBus().addHandler(TemplateRetrievedEvent.TYPE,this);
		AppService.get().getEventBus().addHandler(WebPageRetrievedEvent.TYPE,this);
		
		leftPanel.getElement().getStyle().setBackgroundColor("red");

		webPageSettingsMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				showWebPageSettingsScreen();
			}
		});
		
		webPageReloadMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				AppService.get().loadWebPage(AppService.get().getCurrentWebPage().getId());
			}
		});
		
		webPageSaveMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				storeWebPage();
			}
		});
		
		accountMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				showAccountSettingsScreen();
			}
		});
		
		
		helpMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				Window.open(GWT.getModuleBaseURL()+"help.html", "_blank", "location=yes,menubar=yes,resizable=yes");
			}
		});
		
		aboutMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				AppController.get().getAboutDialogBox().show();
			}
		});
		
		signOutMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				if(Window.confirm("Are you sure you want to sign out?")){
					//AppFacebookAPI.get().signout();
					AppController.get().signOut();
				}
				
			}
		});
		
		if(Log.isDebugEnabled()){
			showHideDivLoggerMenuItem.setCommand(new Command() {
				@Override
				public void execute() {
					AppUtils.showHideLogger();
				}
			});
			AppUtils.setLoggerVisible(false);
		}else{
			developmentMenuItem.setVisible(false);
			showHideDivLoggerMenuItem.setVisible(false);
		}
		//signOutMenuItem.setHTML( "<fb:login-button autologoutlink='true' />" );
		topPanel.updateWelcomeMsg();
		
		showAccountSettingsScreen();
	}
	
	@Override
	public void onWebPageRetrieved(WebPageRetrievedEvent event) {
		AppController.get().getLoadingDialogBox().hide();
		Log.debug("WebPage Retrieved");
		breadcrumbsTxt.setHTML("Editing: <b>" + event.getWebPage().getSubDomain() + ".booix.com</b>");
		AppService.get().loadTemplate(event.getWebPage().getTemplate().getId());
	}

	@Override
	public void onTemplateRetrieved(TemplateRetrievedEvent event) {
		Log.debug("Template received");
		AppService.get().setNewCSSStyleFile(event.getTemplate().getId());
		webPageEditor.update();
	}
	
	public WebPageWidgetEditor getWebPageEditor(){
		return webPageEditor;
	}
	
	@Override
	public void onBrowserEvent(Event event){
		super.onBrowserEvent(event);
		try{
			Element el = DOM.eventGetTarget(event);
			switch (DOM.eventGetType(event)) {
			    case Event.ONCLICK:{
			    	break;
			    }
			    case Event.ONDBLCLICK:{
			    	break;
			    }
			    case Event.ONMOUSEUP:
			    case Event.ONMOUSEMOVE:
			    		System.out.println("Mousemove");
			    case Event.ONMOUSEOVER:{
			    	break;
			    }
			    case Event.ONMOUSEOUT: {
			    	break;
			    }
			    case Event.ONKEYUP:{
			    	
			    	
			    	break;
			    }
			}
		}catch(Exception e){
			Log.error(e.toString());
		}
		
	}
	
	public void createTitleWidget(){
		webPageEditor.createTitleWidget();
	}
	
	public void createTextWidget(){
		webPageEditor.createTextWidget();
	}
	
	public void createImageTextWidget(){
		webPageEditor.createImageTextWidget();
	}
	
	public void createVideoTextWidget(){
		webPageEditor.createVideoTextWidget();
	}
	
	public void showWebPageSettingsScreen(){

		if(webPageSettingsScreen == null){
			webPageSettingsScreen = new WebPageSettingsScreen();
		}
		webPageSettingsScreen.show();
		webPageSettingsScreen.center();
	}
	
	public void showAccountSettingsScreen(){
		if(accountSettingsScreen == null){
			accountSettingsScreen = new AccountSettingsScreen();
		}
		mainPanel.setWidget(accountSettingsScreen);
		configureMenu(accountSettingsScreen);
		accountSettingsScreen.refresh();
		breadcrumbsTxt.setHTML("Account Screen");
	}
	
	private void configureMenu(Composite screen){
		
		if(screen instanceof AccountSettingsScreen){
			accountMenuItem.setVisible(false);
			webPageMenuItem.setVisible(false); //current webpage
		}else if (screen instanceof WebPageWidgetEditor){
			accountMenuItem.setVisible(true);
			webPageMenuItem.setVisible(true); //current webpage
		}
	}
	
	public void storeWebPage(){
		AppService.get().storeWebPage(true);
	}
	
	public void showWebPageEditorScreen(){
		if(webPageEditor == null){
			webPageEditor = new WebPageWidgetEditor();
		}
		
		MainTabPanel mainTabPanel = new MainTabPanel();
		leftPanel.setWidget(mainTabPanel );
		
		mainPanel.setWidget(webPageEditor);
		configureMenu(webPageEditor);
	}
}
