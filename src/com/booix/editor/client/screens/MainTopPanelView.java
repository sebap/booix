package com.booix.editor.client.screens;

import com.booix.editor.client.AppService;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

public class MainTopPanelView extends Composite {
	
	interface TopPanelImages extends ClientBundle {
		/*@Source("../images/logo.png")
		ImageResource logo();*/
	}
	public static TopPanelImages images = (TopPanelImages) GWT.create( TopPanelImages.class );
	
	private HorizontalPanel  mainPanel = null;
	private Button previewBtn = null;
	private Button publishBtn = null;
	private Button settingsBtn = null;
	private Button logoutBtn = null;
	
	public MainTopPanelView() {
		mainPanel = new HorizontalPanel();
		//Image logo = new Image(images.logo());
		Image logo = new Image();
		Label spacer = new Label();
		previewBtn = new Button(AppService.get().getMessages().getPreview());
		previewBtn.addStyleName("booix-Button");
		publishBtn = new Button(AppService.get().getMessages().getPublish());
		settingsBtn = new Button(AppService.get().getMessages().getSettings());
		logoutBtn = new Button(AppService.get().getMessages().getLogout());
		
		mainPanel.add(logo);
		mainPanel.add(spacer);
		mainPanel.add(previewBtn);
		mainPanel.add(publishBtn);
		mainPanel.add(settingsBtn);
		mainPanel.add(logoutBtn);
		
		mainPanel.setCellWidth(spacer, "90%");
		
		mainPanel.setWidth("100%");
		initWidget(mainPanel);
	}
	

}
