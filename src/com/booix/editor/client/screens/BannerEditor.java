package com.booix.editor.client.screens;

import com.booix.editor.client.AppService;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class BannerEditor extends DialogBox implements IScreenButtonsPanel {
	private TextBox logoTxt;
	private TextBox sloganTxt;

	public BannerEditor() {
		setHTML("Banner Editor");
		
		setAnimationEnabled(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(true) ;
		
		FlexTable flexTable = new FlexTable();
		setWidget(flexTable);
		
		Label lblLogoText = new Label("Text:");
		flexTable.setWidget(0, 0, lblLogoText);
		
		logoTxt = new TextBox();
		flexTable.setWidget(0, 1, logoTxt);
		
		Label lblSlogan = new Label("Slogan:");
		flexTable.setWidget(1, 0, lblSlogan);
		
		sloganTxt = new TextBox();
		flexTable.setWidget(1, 1, sloganTxt);
		
		flexTable.setWidget(2,0,new ScreenButtonsPanel(this));
		flexTable.getFlexCellFormatter().setColSpan(2, 0, 2);
		
	}
	
	@Override
	public void show() {
		logoTxt.setText(AppService.get().getCurrentWebPage().getBannerWidget().getLogoText());
		sloganTxt.setText(AppService.get().getCurrentWebPage().getBannerWidget().getSlogan()) ;
		super.show();
	}

	@Override
	public void refresh() {
		hide();
	}

	@Override
	public void save() {

		if(!logoTxt.getText().trim().equals("")){
			AppService.get().getCurrentWebPage().getBannerWidget().setBannerText(logoTxt.getText());
		}else{
			AppService.get().getCurrentWebPage().getBannerWidget().setBannerText("Logo");
		}
		
		AppService.get().getCurrentWebPage().getBannerWidget().setSlogan(sloganTxt.getText());
		BannerEditor.this.hide();
		
	}
	
	
	

}
