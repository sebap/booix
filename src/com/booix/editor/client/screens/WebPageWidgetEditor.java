package com.booix.editor.client.screens;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.allen_sauer.gwt.dnd.client.drop.VerticalPanelDropController;
import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppService.DropControllerEnum;
import com.booix.editor.client.AppUtils.HTML_SUPPORTED_DIV_ID;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.screens.WidgetsTabPanel.WidgetButton;
import com.booix.editor.client.widgets.DnDWidgetContainer;
import com.booix.editor.client.widgets.IWidget;
import com.booix.editor.client.widgets.ImageTextWidget;
import com.booix.editor.client.widgets.MenuWidget;
import com.booix.editor.client.widgets.TextWidget;
import com.booix.editor.client.widgets.TitleWidget;
import com.booix.editor.client.widgets.VideoTextWidget;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class WebPageWidgetEditor extends Composite{
	
	private DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX);
	
	private MainTabPanel mainTabPanel = new MainTabPanel();
	
	private HTMLPanel templateHTMLPanel = new HTMLPanel("");
	private ScrollPanel editorScrollPanel = new ScrollPanel(templateHTMLPanel);

	//These are the container Widgets used to display the current InnerPage widgets.
	//this is the list of pages from the webpage
	private MenuWidget menuWidget = new MenuWidget(); // navigation content
	
	private SimplePanel mainContentContainerSpacer = new SimplePanel();
	private VerticalPanel mainContentContainer = new VerticalPanel(); // main content
	private SimplePanel sideContentContainerSpacer = new SimplePanel();
	private VerticalPanel sideContentContainer = new VerticalPanel();; // side content
	
	private VerticalPanelDropController mainDropController = new CustomVerticalPanelDropController( mainContentContainer, DropControllerEnum.MAIN);
	private VerticalPanelDropController sideDropController = new CustomVerticalPanelDropController( sideContentContainer, DropControllerEnum.RIGHT);
	//VerticalPanelDropController leftDropController = new CustomVerticalPanelDropController( leftContainer, DropControllerEnum.LEFT);
	
	private BannerEditor bannerEditor = null;
	private MenuEditor menuEditor = null;
	
	public WebPageWidgetEditor() {

		initWidget(mainPanel);
		mainPanel.addWest(mainTabPanel, 190);
		mainPanel.add(editorScrollPanel);
		mainPanel.setWidth("100%");
		mainPanel.setHeight("100%");
		
		mainContentContainerSpacer.setStyleName( AppUtils.CSS_PREFIX + "ContainerSpacer");
		mainContentContainer.getElement().setId(HTML_SUPPORTED_DIV_ID.MAIN_CONTENT.getId());
		
		sideContentContainerSpacer.setStyleName(AppUtils.CSS_PREFIX + "ContainerSpacer");
		sideContentContainer.getElement().setId(HTML_SUPPORTED_DIV_ID.SIDE_CONTENT.getId());
		
		AppController.get().getDragController().registerDropController(mainDropController);
		AppController.get().getDragController().registerDropController(sideDropController);

		AppController.get().getDragController().setBehaviorBoundaryPanelDrop(false);
		AppController.get().getDragController().addDragHandler(new PickupDragHandler());
		AppController.get().getDragController().setBehaviorScrollIntoView(true);
	}
	
	public MenuWidget getMenu(){
		return menuWidget;
	}
	
	/**
	 * We will use this to load template, navigation to the template. 
	 * TODO: we will need to load the background here
	 */
	public void update() {
		
		AppController.get().getLoadingDialogBox().show("Updating Web Page Editor");
		
		try{
			menuWidget.removeFromParent();
			
			mainContentContainer.removeFromParent();
			mainContentContainer.clear();
			
			sideContentContainer.removeFromParent();
			sideContentContainer.clear();
			
			//This is required for IE, otherwise it will fail.
			AppService.get().getCurrentWebPage().getBannerWidget().removeFromParent();
			
			templateHTMLPanel.getElement().setInnerHTML(AppService.get().getCurrentWebPage().getTemplate().getHtml());
			
			templateHTMLPanel.addAndReplaceElement(AppService.get().getCurrentWebPage().getBannerWidget(), HTML_SUPPORTED_DIV_ID.BANNER.getId());
			templateHTMLPanel.addAndReplaceElement(menuWidget, HTML_SUPPORTED_DIV_ID.MENU.getId());
			templateHTMLPanel.addAndReplaceElement(mainContentContainer, HTML_SUPPORTED_DIV_ID.MAIN_CONTENT.getId());
			templateHTMLPanel.addAndReplaceElement(sideContentContainer, HTML_SUPPORTED_DIV_ID.SIDE_CONTENT.getId());

			updateContainers();
			
		}catch(Exception e){
			Log.error(e.toString());
		}

		AppController.get().getLoadingDialogBox().hide();
	}

	/**
	 * Helper method for updateWebpage() This method will re-attach the widgets
	 * This is needed when we regenerate the contents after receiving a new template
	 */
	private void updateContainers() throws Exception {
		
		menuWidget.updateContent();

		mainContentContainer.add(mainContentContainerSpacer);
		mainContentContainer.addStyleName(AppService.get().getMessages().getDropWidgetCSSName());
		for (Widget current : AppService.get().getCurrentInnerPageWidget().getMainContentWidgets()) {
			mainContentContainer.remove(mainContentContainerSpacer);
			mainContentContainer.removeStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			mainContentContainer.add((DnDWidgetContainer) current);
			((DnDWidgetContainer) current).setContainer(mainContentContainer);
			AppController.get().getDragController().makeDraggable((DnDWidgetContainer) current, ((DnDWidgetContainer) current).getTitleLabel());
		}
		
		sideContentContainer.add(sideContentContainerSpacer);
		sideContentContainer.addStyleName(AppService.get().getMessages().getDropWidgetCSSName());
		for (Widget current : AppService.get().getCurrentInnerPageWidget().getSideContentWidgets()) {
			sideContentContainer.remove(sideContentContainerSpacer);
			sideContentContainer.removeStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			sideContentContainer.add(((DnDWidgetContainer) current));
			((DnDWidgetContainer) current).setContainer(sideContentContainer);
			AppController.get().getDragController().makeDraggable((DnDWidgetContainer) current,((DnDWidgetContainer) current).getTitleLabel());
		}
	}

	/**
	 * Used to remove a widget from the webpage
	 * 
	 * @param widgetToRemove
	 */
	public void removeFromWebPage(DnDWidgetContainer widgetToRemove) {

		mainContentContainer.remove(widgetToRemove);
		AppService.get().getCurrentInnerPageWidget().getMainContentWidgets().remove(widgetToRemove);

		sideContentContainer.remove(widgetToRemove);
		AppService.get().getCurrentInnerPageWidget().getSideContentWidgets().remove(widgetToRemove);

		update();
	}

	public void insertWidgetInMainContainer(DnDWidgetContainer dndWidget, int index){
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().add(index,dndWidget);
		mainContentContainer.insert(dndWidget, index);
		AppController.get().getDragController().makeDraggable(dndWidget, dndWidget.getTitleLabel());
	}
	
	public void insertWidgetInRightContainer(DnDWidgetContainer dndWidget, int index){
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getSideContentWidgets().add(index,dndWidget);
		sideContentContainer.insert(dndWidget, index);
		AppController.get().getDragController().makeDraggable(dndWidget, dndWidget.getTitleLabel());
	}

	/*****************************************************************************
	 * Methods that will create components from the Tabbed Panel
	 *****************************************************************************/
	public void createTitleWidget() {
		IWidget title = new TitleWidget();
		DnDWidgetContainer titleContainer = new DnDWidgetContainer(title, mainContentContainer );
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().add(titleContainer);
		mainContentContainer.add(titleContainer);
		AppController.get().getDragController().makeDraggable(titleContainer, titleContainer.getTitleLabel());
		editorScrollPanel.scrollToBottom();
	}

	public void createTextWidget() {
		IWidget text = new TextWidget();
		DnDWidgetContainer textContainer = new DnDWidgetContainer(text, mainContentContainer );
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().add(textContainer);
		mainContentContainer.add(textContainer);
		AppController.get().getDragController().makeDraggable(textContainer, textContainer.getTitleLabel());
		editorScrollPanel.scrollToBottom();
	}
	
	public void createImageTextWidget() {
		IWidget imageTextWidget = new ImageTextWidget();
		DnDWidgetContainer imageTextContainer = new DnDWidgetContainer(imageTextWidget, mainContentContainer );
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().add(imageTextContainer);
		mainContentContainer.add(imageTextContainer);
		AppController.get().getDragController().makeDraggable(imageTextContainer, imageTextContainer.getTitleLabel());
		editorScrollPanel.scrollToBottom();
	}

	public void createVideoTextWidget() {
		IWidget videoText = new VideoTextWidget();
		DnDWidgetContainer videoTextContainer = new DnDWidgetContainer(videoText, mainContentContainer );
		AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().add(videoTextContainer);
		mainContentContainer.add(videoTextContainer);
		AppController.get().getDragController().makeDraggable(videoTextContainer, videoTextContainer.getTitleLabel());
		editorScrollPanel.scrollToBottom();
	}
	
	
	public void showBannerEditor(){
		
		if(bannerEditor == null){
			bannerEditor = new BannerEditor();
		}
		bannerEditor.center();
		bannerEditor.show();
	}
	
	public void showMenuEditor(){
		if(menuEditor == null){
			menuEditor = new MenuEditor();
		}
		menuEditor.center();
		menuEditor.show();
	}

	
	/***************************************************************************************************
	 * Inner Classes: This section contains all the inner classes that are used
	 * only in the WebPageWidget.
	 ****************************************************************************************************/

	/**
	 * Used for extending the VerticalPanelDrop Controllers This class is the
	 * actual container of widget in the editor. This is used mainly for moving
	 * the scroll and for detecting which container is being dropped a widget on
	 */
	private class CustomVerticalPanelDropController extends
			VerticalPanelDropController {

		private DropControllerEnum id;

		public CustomVerticalPanelDropController(VerticalPanel dropTarget, DropControllerEnum id) {
			super(dropTarget);
			this.id = id;
		}

		@Override
		protected Widget newPositioner(DragContext context) {
			Widget w = super.newPositioner(context);
			((SimplePanel) w).getWidget().setWidth("100%");
			return w;
		}

		/**
		 * Uses to return the Container
		 * 
		 * @return
		 */
		public DropControllerEnum getId() {
			return id;
		}
		
		@Override
		public void onDrop(DragContext context) {
			super.onDrop(context);
			
			if(context.draggable instanceof WidgetButton){
				int index = dropTarget.getWidgetIndex(context.draggable);
				WidgetButton wb = (WidgetButton)context.draggable;
				IWidget w = null;
				
				if(wb.getType() == WidgetType.TITLE){
					w = new TitleWidget();
				}else if(wb.getType() == WidgetType.TEXT){
					w = new TextWidget();
				}else if(wb.getType() == WidgetType.IMAGE_TEXT){
					w = new ImageTextWidget();
				}else if(wb.getType() == WidgetType.VIDEO_TEXT){
					w = new VideoTextWidget();
				}
				
				DnDWidgetContainer dndWidgetContainer = new DnDWidgetContainer(w, null); 
				AppController.get().getDragController().makeDraggable(dndWidgetContainer, dndWidgetContainer.getTitleLabel());
				
				dropTarget.remove(index);
				dropTarget.insert(dndWidgetContainer, index);
				
				mainTabPanel.getWidgetsTabPanel().reloadTab();
				
			}
		}
		
		
		@Override
		public void onPreviewDrop(DragContext context) throws VetoDragException {
			// TODO Auto-generated method stub
			super.onPreviewDrop(context);
		}

		@Override
		public void onMove(DragContext context) {
			
			super.onMove(context);
			
			int topShit =(Window.getClientHeight() - editorScrollPanel.getOffsetHeight());
			int middle = (editorScrollPanel.getOffsetHeight()/2);

			/*if (( context.mouseY - topShit ) < middle) {
				AppController.get().getAppScreen().getScrollPanel().setScrollPosition(AppController.get().getAppScreen().getScrollPanel().getScrollPosition() - 5);
			} else if ((context.mouseY - topShit) > middle) { // when moving up
				AppController.get().getAppScreen().getScrollPanel().setScrollPosition(AppController.get().getAppScreen().getScrollPanel().getScrollPosition() + 5);
			}*/
			/*
			System.out.println("TS:" + topShit) ;
			System.out.println("Middle:" + middle) ;
			System.out.println("TS + Middle:" + (topShit + middle)) ;
			System.out.println("Y:" + context.mouseY) ;
			System.out.println("Diff:"+ ((topShit + middle)- context.mouseY));*/
			int diff = ((topShit + middle)- context.mouseY);
			if(diff > 100){
				editorScrollPanel.setScrollPosition(editorScrollPanel.getScrollPosition() - 5);
			}else if(diff < -100){
				editorScrollPanel.setScrollPosition(editorScrollPanel.getScrollPosition() + 5);
			}
			
			

			
			//*System.out.println("Y:" + context.mouseY);
			/*System.out.println("Top Shit:" + (Window.getClientHeight() - AppController.get().getAppScreen().getScrollPanel().getOffsetHeight()));
			System.out.println("Window Size:" + Window.getClientHeight());
			System.out.println("ScrollPanel Size:" + );*/
			
			//We are doing this to force the vew to display the positioner*/
			/*VerticalPanel panel = ((VerticalPanel)context.dropController.getDropTarget());
			for (int i = 0; i < panel.getWidgetCount(); i++) {
				Widget w = panel.getWidget(i);
				if(w.getStyleName().contains("positioner")){
					w.getElement().scrollIntoView();
					//AppController.get().getAppScreen().getScrollPanel().setScrollPosition((w.getAbsoluteTop() +  10));
					break;
				}
			}*/
		}
	}
	
	
	

	/**
	 * This class is used to extend the handler of the Main DragController in
	 * AppMainView. Basically this class is used to listen for the dragEnd
	 * event. When the drag has ended we will update the list of widgets for the
	 * inner page that is currently being used @See
	 * AppService.getInstance().getCurrentInnerPage()
	 */
	private class PickupDragHandler implements DragHandler {

		@Override
		public void onPreviewDragStart(DragStartEvent event)
				throws VetoDragException {


		}

		@Override
		public void onPreviewDragEnd(DragEndEvent event)
				throws VetoDragException {
		}

		@Override
		public void onDragStart(DragStartEvent event) {
			
			
			if(event.getContext().draggable instanceof DnDWidgetContainer){
				((DnDWidgetContainer)event.getContext().draggable).stopEditing();
			}
			
			
			// mainContainer.remove((WidgetContainerView)event.getContext().draggable);
			// innerPageWidgetList.get(0).getMainContainerWidgets().remove((WidgetContainerView)event.getContext().draggable);
		}

		@Override
		public void onDragEnd(DragEndEvent event) {
			
			//Window.alert("on drag end");
			CustomVerticalPanelDropController dropC = (CustomVerticalPanelDropController) event.getContext().finalDropController;
			if (dropC != null) {
				/*if (dropC.getId().ordinal() == DropControllerEnum.MAIN.ordinal()) {
					refreshInnerPagesWidgetList();
				} else if (dropC.getId().ordinal() == DropControllerEnum.RIGHT.ordinal()) {
					
				}*/

				refreshInnerPagesWidgetList();
			}
		}

		/**
		 * This method will update the list of widgets based on the new position
		 * of the widgets in the screen. We need to mantain in synch both
		 * the innerPageWidgetList with the containers.
		 * 
		 * This is needed when there are new changes on the current widgets layout.
		 */
		public void refreshInnerPagesWidgetList() {
			
			//Work on the main container
			AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getMainContentWidgets().clear();
			
			mainContentContainer.remove(mainContentContainerSpacer);
			mainContentContainer.removeStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			
			for (int i = 0; i < mainContentContainer.getWidgetCount(); i++) {
				AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId())
						.getMainContentWidgets().add(
								(DnDWidgetContainer) mainContentContainer
										.getWidget(i));
				((DnDWidgetContainer) mainContentContainer.getWidget(i)).setContainer(mainContentContainer);
			}
			if(mainContentContainer.getWidgetCount()==0){
				mainContentContainer.add(mainContentContainerSpacer);
				mainContentContainer.addStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			}
			
			
			
			//Work on the right container

			AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId())
					.getSideContentWidgets().clear();
			
			
			sideContentContainer.remove(sideContentContainerSpacer);
			sideContentContainer.removeStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			
			
			for (int i = 0; i < sideContentContainer.getWidgetCount(); i++) {
				AppService.get().getCurrentWebPage().getInnerPageList().get(AppService.get().getCurrentInnerPageId()).getSideContentWidgets().add(
								(DnDWidgetContainer) sideContentContainer
										.getWidget(i));
				((DnDWidgetContainer) sideContentContainer.getWidget(i)).setContainer(sideContentContainer);
			}
			if(sideContentContainer.getWidgetCount()==0){
				sideContentContainer.add(sideContentContainerSpacer);
				sideContentContainer.addStyleName(AppService.get().getMessages().getDropWidgetCSSName());
			}
			
		}
	}

}
