package com.booix.editor.client.screens;

import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.ui.Composite;

public class WebPagesList extends Composite {
	
	private CellTable<WebPageWidget> webPageListTable = new CellTable<WebPageWidget>(); 

	public WebPagesList() {
		initWidget(webPageListTable);
	}

}
