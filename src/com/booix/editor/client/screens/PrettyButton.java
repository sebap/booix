package com.booix.editor.client.screens;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;

/**
 * This calss is used to create a button with an image and text
 * @author Owner
 *
 */
public class PrettyButton extends Composite{
	private HorizontalPanel mainPanel = new HorizontalPanel();
	
	private PushButton button ;
	public PrettyButton(Image image, String buttonText){
		FlowPanel fPanel = new FlowPanel();
		fPanel.add(image);
		Label label = new Label(buttonText);
		label.getElement().getStyle().setColor("black");
		label.addStyleName("floatRight");
		fPanel.add(label);
		button = new PushButton();
		button.setHTML(fPanel.getElement().getInnerHTML());
		button.getElement().getStyle().setPaddingTop(5, Unit.PX);
		button.getElement().getStyle().setMargin(3, Unit.PX);
		mainPanel.add(button);
		this.initWidget(mainPanel);
	}
	
	public PushButton getButton(){
		return button;
	}
}