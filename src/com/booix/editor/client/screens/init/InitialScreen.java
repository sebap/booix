package com.booix.editor.client.screens.init;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;

public class InitialScreen extends Composite {
	
	private DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX); 
	private HorizontalPanel innerPanel = new HorizontalPanel();
	
	

	public InitialScreen() {
		
		initWidget(mainPanel);
	
		innerPanel.add(new SignUpScreen());
		innerPanel.add(new LoginScreen());
		innerPanel.getElement().getStyle().setProperty("margin", "0 auto");
		innerPanel.setWidth("500px");
		
		mainPanel.addNorth(innerPanel, 400);
	}

}
