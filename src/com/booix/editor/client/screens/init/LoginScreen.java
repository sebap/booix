package com.booix.editor.client.screens.init;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils.USER_TYPE;
import com.booix.editor.client.event.LoginEvent;
import com.booix.editor.client.event.LoginEvent.LoginEventType;
import com.booix.editor.client.event.LoginEventHandler;
import com.booix.editor.client.model.User;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginScreen extends Composite implements LoginEventHandler {

	private static LoginScreen2UiBinder uiBinder = GWT
			.create(LoginScreen2UiBinder.class);

	interface LoginScreen2UiBinder extends UiBinder<Widget, LoginScreen> {
	}
	
	public LoginScreen() {
		initWidget(uiBinder.createAndBindUi(this));
		usernameTxt.setText("admin@gmail.com");
		passwordTxt.setText("admin");
		loginBtn.setText(AppService.get().getMessages().getLogin());
		AppService.get().getEventBus().addHandler(LoginEvent.TYPE, this);
	}

	@UiField Button loginBtn;
	@UiField TextBox usernameTxt;
	@UiField PasswordTextBox passwordTxt;
	@UiField Label messageTxt;
	
	@UiHandler("loginBtn")
	void onLoginClick(ClickEvent event){

		DeferredCommand.addCommand(new Command() {
			@Override
			public void execute() {
				login();
			}
		});
	}
	
	private void login(){
		messageTxt.setText(""); 
		User user = new User(USER_TYPE.BOOIX);
		user.setEmail(usernameTxt.getText());
		user.setPassword(passwordTxt.getText(), true);
		AppService.get().login(user);
	}
	
	public void setMessage(String msg){
		messageTxt.setText(msg);
	}

	@Override
	public void onLoginEvent(LoginEvent event) {
		if(event.getLoginEventType() == LoginEventType.SUCCESS){
			AppController.get().showAppScreen();
		}else{
			setMessage("Invalid Credentials");
		}
	}

}
