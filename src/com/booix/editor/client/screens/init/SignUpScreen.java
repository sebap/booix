package com.booix.editor.client.screens.init;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;

import com.booix.editor.client.AppUtils.USER_TYPE;
import com.booix.editor.client.model.User;
import com.booix.editor.client.utils.Validator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class SignUpScreen extends Composite {

	private static SignUpUiBinder uiBinder = GWT.create(SignUpUiBinder.class);

	interface SignUpUiBinder extends UiBinder<Widget, SignUpScreen> { }
	
	@UiField TextBox emailTxt;
	@UiField PasswordTextBox passwordTxt;
	@UiField Label messageTxt;
	@UiField Button saveBtn;

	public SignUpScreen() {
		initWidget(uiBinder.createAndBindUi(this));
		saveBtn.setText(AppService.get().getMessages().getSave());
	}


	
	@UiHandler("saveBtn")
	void onSaveCLick(ClickEvent event){
		AppController.get().getLoadingDialogBox().show("Please wait we save your information...");
		
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				signup();
			}
		});
	}
	
	private void signup(){
		
		if(emailTxt.getText().trim().equals("") || passwordTxt.getText().equals("")){
			AppController.get().getLoadingDialogBox().hide();
			messageTxt.setText("Please complete the form.");
			return;
		}else if(!Validator.isEmailValid(emailTxt.getText())){
			AppController.get().getLoadingDialogBox().hide();
			messageTxt.setText("Invalid Email please make sure you use a valid email");
			return;
		}
		messageTxt.setText("");
		User user = new User(USER_TYPE.BOOIX);
		user.setEmail(emailTxt.getText());
		user.setPassword(passwordTxt.getText(), true);
		AppService.get().signup(user);
	}
	
	
	
	/***
	 * Here we need to listen for user logged in
	 * @param msg
	 */
	
	public void setMessage(String msg){
		messageTxt.setText(msg);
	}

}
