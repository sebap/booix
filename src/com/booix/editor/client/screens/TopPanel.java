package com.booix.editor.client.screens;


import com.booix.editor.client.AppController;
import com.booix.editor.client.AppFacebookAPI;
import com.booix.editor.client.AppService;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class TopPanel extends Composite {

  interface Binder extends UiBinder<Widget, TopPanel> { }
  private static final Binder binder = GWT.create(Binder.class);

  @UiField HTML welcomeMsg;
  @UiField Button notYouLink;
  
  @UiConstructor
  public TopPanel(){
	  initWidget(binder.createAndBindUi(this));
	  notYouLink.addClickHandler(new ClickHandler() {
		
		@Override
		public void onClick(ClickEvent event) {
			//AppFacebookAPI.get().signout();
			AppController.get().signOut();
		}
	});
  }

  public void updateWelcomeMsg(){
	  String welcomeName = "";
	  if(AppService.get().getCurrentUser().getName() != null ){
		  welcomeName = AppService.get().getCurrentUser().getName(); 
	  }else{
		  welcomeName = AppService.get().getCurrentUser().getEmail();
	  }
	  
	  welcomeMsg.setHTML("Welcome <b> " + welcomeName + "</b>");
  }
  
}
