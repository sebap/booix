package com.booix.editor.client.screens;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.event.TemplateRetrievedEvent;
import com.booix.editor.client.event.TemplateRetrievedEventHandler;
import com.booix.editor.client.model.Template;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class TemplatesTabPanel extends Grid implements ResizeHandler, TemplateRetrievedEventHandler {
	
	private VerticalPanel templateButtonContainersPanel = new VerticalPanel();
	private ScrollPanel templateButtonContainersPanelScrollP = new ScrollPanel(templateButtonContainersPanel);
	private List<TemplateButtonContainer> templateButtonContainers = new ArrayList<TemplateButtonContainer>();
	private int SCROLL_RIGHT_MARGIN = 22 ;
	
	public TemplatesTabPanel() {
		resize(1, 1);

		templateButtonContainersPanel.addStyleName( AppUtils.CSS_PREFIX + "TemplateButtonsPanel");
		
		for (int i = 0; i < AppService.NUMBER_OF_TEMPLATES; i++ ){
			TemplateButtonContainer temp = new TemplateButtonContainer(i + 1);
			templateButtonContainers.add( temp );
			templateButtonContainersPanel.add(temp);
		}
		
		//templateButtonContainersPanelScrollP.setWidth(Window.getClientWidth() - SCROLL_RIGHT_MARGIN + "px");
		templateButtonContainersPanelScrollP.setHeight("140px");
		
		//setWidget(0, 0, templateButtonContainersPanelScrollP);
		
		setWidget(0, 0, templateButtonContainersPanel);
		
		registerListener();
		
	}
	
	private void registerListener(){
		//Attaching this panel to the Resize handler
		Window.addResizeHandler(this);
		
		AppService.get().getEventBus().addHandler(TemplateRetrievedEvent.TYPE,this);
		
		Iterator<TemplateButtonContainer> iter = templateButtonContainers.iterator();
		while(iter.hasNext()){
			final TemplateButtonContainer current = iter.next();
			current.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					int id = current.getButton().getTemplateId();
					AppService.get().loadTemplate(id);
				}
			});
		}
	}
	
	
	
	@Override
	public void onResize(ResizeEvent event) {
		//templateButtonContainersPanelScrollP.setWidth(Window.getClientWidth() - SCROLL_RIGHT_MARGIN + "px");
	}


	public class TemplateButtonContainer extends Composite implements HasClickHandlers{
		private TemplateButton button;
	    private VerticalPanel containerLayout;
	    TemplateButtonContainer(int id){
	    	containerLayout = new VerticalPanel();
			int templateId = id;
			Image buttonImage = new Image(AppService.ABSOLUTE_PATH_TO_TEMPLATES + "/template"+ id +"/images/preview140x105.gif");
			button = new TemplateButton(templateId, buttonImage);
			containerLayout.add(button);
			initWidget(containerLayout);
		}

	    @Override
		public HandlerRegistration addClickHandler(ClickHandler handler) {
			return button.addClickHandler(handler);
		}
		
		public TemplateButton getButton(){
			return button;
		}
	}
	
	public class TemplateButton extends PushButton{
		private int templateId = 0;
		TemplateButton(int id, Image image){
			super(image);
			this.templateId = id;
		}
		public int getTemplateId(){
			return templateId;
		}
	}

	@Override
	public void onTemplateRetrieved(TemplateRetrievedEvent event) {
		updateSelectedTemplateButton(event.getTemplate());
	}
	
	/**
	 * Helper method for updateWebpage() This method will clean all the buttons
	 * off of the selected class and will set the selected to the template that
	 * has been loaded.
	 * 
	 * @param template
	 */
	private void updateSelectedTemplateButton(Template template) {
		Iterator<TemplateButtonContainer> iter = templateButtonContainers.iterator();
		while (iter.hasNext()) {
			TemplateButton current = iter.next().getButton();
			current.removeStyleName(AppUtils.CSS_PREFIX + "SelectedTemplateButton");
			if (current.getTemplateId() == template.getId()) {
				current.addStyleName(AppUtils.CSS_PREFIX + "SelectedTemplateButton");
			}
		}
	}
}