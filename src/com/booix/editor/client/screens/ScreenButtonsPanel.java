package com.booix.editor.client.screens;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;


public class ScreenButtonsPanel extends HorizontalPanel{
	
	public interface IScreenButtonsPanel {
		public void save();
		public void refresh();
	}
	
	private PrettyButton saveBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.saveIcon()), AppService.get().getMessages().getSave());
	private PrettyButton refreshBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.refreshIcon()), AppService.get().getMessages().getRefresh());
	private IScreenButtonsPanel screen = null;
	
	public ScreenButtonsPanel( IScreenButtonsPanel iScreen ){
		screen = iScreen;
		add(saveBtn);
		add(refreshBtn);
		
		saveBtn.getButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				screen.save();
			}
		});
		
		refreshBtn.getButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				screen.refresh();
			}
		});
		getElement().getStyle().setMarginTop(10, Unit.PX);
	}
}