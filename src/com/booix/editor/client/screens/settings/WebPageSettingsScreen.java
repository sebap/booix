package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.screens.PrettyButton;
import com.booix.editor.client.screens.VerticalTabPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

public class WebPageSettingsScreen extends DialogBox{
	private FlexTable mainPanel = new FlexTable();
	private Label msgL = new Label();
	private VerticalTabPanel verticalTabPanel = new VerticalTabPanel();
	private PrettyButton closeBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.cancelIcon()), "Close");
	private WebPageSettingsURLTab urlTab = new WebPageSettingsURLTab(this); 
	private WebPageSettingsContentTab webPageTab = new WebPageSettingsContentTab();

	public WebPageSettingsScreen() {
		setHTML("Web Page Settings");
		setAnimationEnabled(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(false) ;
		add(mainPanel);
		createDisplay();
	}
	
	
	/**
	 * Used to create and recreate the edit is opened.
	 */
	private void createDisplay(){
		mainPanel.setWidget(0, 0, verticalTabPanel);
		
		verticalTabPanel.add(urlTab, "URL");
		verticalTabPanel.add(webPageTab, "Content");
		//verticalTabPanel.add(new SettingsAccountDataTab(), "Google Analytics");
		verticalTabPanel.selectTab(0);
		mainPanel.setWidget(1,0, msgL);
		mainPanel.setWidget(2,0, closeBtn );
		closeBtn.getButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				cancel();
			}
		});
		mainPanel.getFlexCellFormatter().setHorizontalAlignment(2,0, HasHorizontalAlignment.ALIGN_RIGHT);
		msgL.getElement().getStyle().setProperty("color", "red");
	}
	
	@Override
	public void show() {
		msgL.setText("");
		urlTab.refresh();
		webPageTab.refresh();
		super.show();
	}
	
	public void cancel(){
		
		super.hide();
	}
}
