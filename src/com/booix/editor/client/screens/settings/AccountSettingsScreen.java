package com.booix.editor.client.screens.settings;

import com.booix.editor.client.screens.VerticalTabPanel;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;

public class AccountSettingsScreen extends Composite {
	private DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX);
	
	private VerticalTabPanel verticalTabPanel = new VerticalTabPanel();
	
	private AccountSettingsMainDataTab mainTab = new AccountSettingsMainDataTab();
	private AccountSettingsWebPagesTab webPagesTab = new AccountSettingsWebPagesTab();
	

	public AccountSettingsScreen() {
		initWidget(mainPanel);
		createDisplay();
	}

	/**
	 * Used to create and recreate the edit is opened.
	 */
	private void createDisplay(){
		mainPanel.addNorth(verticalTabPanel,500);
		mainPanel.setWidth("100%");
		mainPanel.setHeight("100%");
		
		verticalTabPanel.add(webPagesTab, "WebPages");
		verticalTabPanel.add(mainTab, "Account Info");
		//verticalTabPanel.add(new SimplePanel(), "Upgrade");
		
		verticalTabPanel.selectTab(0);
	}
	
	public void refresh() {
		mainTab.refresh();
		webPagesTab.refresh();
	}

}
