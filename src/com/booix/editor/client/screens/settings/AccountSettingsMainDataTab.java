package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.ACCOUNT_TYPE;
import com.booix.editor.client.screens.PrettyButton;
import com.booix.editor.client.screens.ScreenButtonsPanel;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

public class AccountSettingsMainDataTab extends Composite implements IScreenButtonsPanel {
	
	private FlexTable layoutPanel = new FlexTable();
	private ScrollPanel mainPanel = new ScrollPanel(layoutPanel);
	private ScreenButtonsPanel buttonsPanel = new ScreenButtonsPanel(this);
	
	private PrettyButton upgradeBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.upIcon()), "Upgrade");
	private PrettyButton deleteBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.deleteIcon()), "Delete");
	
	private RadioButton freeRB = new RadioButton("AccountType", "Free");
	private RadioButton proRB = new RadioButton("AccountType", "Pro");
	private TextBox emailTxt = new TextBox();
	private TextBox oldPasswordTxt = new PasswordTextBox();
	private TextBox newPasswordTxt = new PasswordTextBox();
	private TextBox firstNameTxt = new TextBox();
	private TextBox lastNameTxt = new TextBox();
	/*private DateTimeFormat dateFormat = DateTimeFormat.getFormat("MM/dd/yyyy");
    private DateBox birthdayTxt = new DateBox();*/
	private TextBox birthdayTxt = new TextBox();
	
	public AccountSettingsMainDataTab() {
		initWidget(mainPanel);
		createDisplay();
	}
	
	private void createDisplay(){
		
		layoutPanel.getElement().getStyle().setProperty("margin", "0 auto");
		
		int row = 0;
		layoutPanel.setWidget(row, 0, new HTML("Account Type:"));
		HorizontalPanel accountTypeHP = new HorizontalPanel();
		accountTypeHP.add(freeRB);
		accountTypeHP.add(proRB);
		accountTypeHP.setCellVerticalAlignment(freeRB, HasVerticalAlignment.ALIGN_MIDDLE);
		accountTypeHP.setCellVerticalAlignment(proRB, HasVerticalAlignment.ALIGN_MIDDLE);
		layoutPanel.setWidget(row, 1, accountTypeHP);
		
		layoutPanel.setWidget(++row, 0, new HTML("Email:"));
		layoutPanel.setWidget(row, 1, emailTxt);
		emailTxt.setWidth("200px");
		emailTxt.setEnabled(false);
		
		
		layoutPanel.setWidget(++row, 0, new HTML("Old Password:"));
		layoutPanel.setWidget(row, 1, oldPasswordTxt);
		
		layoutPanel.setWidget(++row, 0, new HTML("New Password:"));
		layoutPanel.setWidget(row, 1, newPasswordTxt);
		
		layoutPanel.setWidget(++row, 0, new HTML("First Name:"));
		layoutPanel.setWidget(row, 1, firstNameTxt);
		
		layoutPanel.setWidget(++row, 0, new HTML("Last Name:"));
		layoutPanel.setWidget(row, 1, lastNameTxt);
		
		layoutPanel.setWidget(++row, 0, new HTML("Birthday:"));
		layoutPanel.setWidget(row, 1, birthdayTxt);
		//birthdayTxt.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		layoutPanel.setWidget(++row, 0, new HTML("Country:"));
		layoutPanel.setWidget(row, 1, new TextBox());
		
		layoutPanel.setWidget(++row, 0, new HTML("City:"));
		layoutPanel.setWidget(row, 1, new TextBox());
		
		layoutPanel.setWidget(++row, 0, new HTML("Address:"));
		layoutPanel.setWidget(row, 1, new TextArea());
		
		buttonsPanel.insert(deleteBtn, 0);
		buttonsPanel.insert(upgradeBtn, 0);
		layoutPanel.setWidget(++row, 0, buttonsPanel );
		layoutPanel.getFlexCellFormatter().setColSpan(row, 0, 2);
		layoutPanel.getFlexCellFormatter().setHorizontalAlignment(row,0,HasHorizontalAlignment.ALIGN_CENTER);
		
		upgradeBtn.getButton().setEnabled(false);
		
		deleteBtn.getButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(Window.confirm("Are you sure you want to delete your account? This Action is NOT reversible, and will delete all your created webpages.")){
					AppService.get().deleteCurrentUser();
				}
			}
		});
		
	}

	@Override
	public void refresh(){
		if(AppService.get().getCurrentUser().getAccountType() == ACCOUNT_TYPE.FREE){
			setAccountTypeFree(true);
		}else{
			setAccountTypeFree(false);
		}
		emailTxt.setText(AppService.get().getCurrentUser().getEmail());
	}
	
	private void setAccountTypeFree(boolean val){
		freeRB.setValue(val);
		freeRB.setVisible(val);
		proRB.setValue(!val);
		proRB.setEnabled(!val);
	}
	
	@Override
	public void save() {
		
		if(oldPasswordTxt.getText().equals("")){
			
		}
		
		if(newPasswordTxt.getText().equals("")){
			
		}
		//AppController.get().getLoadingDialogBox().show("Saving personal info");
	}

}
