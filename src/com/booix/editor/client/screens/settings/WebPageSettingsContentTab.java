package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils.WEBPAGE_TYPE;
import com.booix.editor.client.screens.ScreenButtonsPanel;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

public class WebPageSettingsContentTab extends Composite implements IScreenButtonsPanel {
	
	private FlexTable layoutPanel = new FlexTable();
	private ScrollPanel mainPanel = new ScrollPanel(layoutPanel);
	
	private RadioButton personalRB = new RadioButton("WebPageType", "Personal");
	private RadioButton businessRB = new RadioButton("WebPageType", "Business");
	private TextBox businessTypeTxt = new TextBox();
	private TextBox titleTxt = new TextBox();
	private TextArea descriptionTxt = new TextArea();
	private TextArea keywordsTxt = new TextArea();
	
	public WebPageSettingsContentTab() {
		initWidget(mainPanel);
		createDisplay();
	}
	
	private void createDisplay(){
		
		layoutPanel.getElement().getStyle().setProperty("margin", "0 auto");

		personalRB.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(personalRB.getValue()){
					businessTypeTxt.setEnabled(false);
					businessTypeTxt.setText("");
				}
			}
		});
		businessRB.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(businessRB.getValue()){
					businessTypeTxt.setEnabled(true);
					businessTypeTxt.setText(AppService.get().getCurrentWebPage().getTypeDescription());
				}
			}
		});
		layoutPanel.setWidget(0, 0, new HTML("Type:"));
		HorizontalPanel webPageTypeHP = new HorizontalPanel();
		webPageTypeHP.add(personalRB);
		webPageTypeHP.add(businessRB);
		layoutPanel.setWidget(0, 1, webPageTypeHP);
		
		layoutPanel.setWidget(1, 0, new HTML("Business Type:"));
		layoutPanel.setWidget(1, 1, businessTypeTxt );
		
		/*layoutPanel.setWidget(2, 0, new HTML("Language"));
		layoutPanel.setWidget(2, 1, new ListBox());*/
		
		layoutPanel.setWidget(2, 0, new HTML("Title:"));
		layoutPanel.setWidget(2, 1, titleTxt);
		
		layoutPanel.setWidget(3, 0, new HTML("Description:"));
		layoutPanel.setWidget(3, 1, descriptionTxt);
		descriptionTxt.setSize("400px", "100px");
		descriptionTxt.getElement().getStyle().setProperty("resize", "none");
		
		layoutPanel.setWidget(4, 0, new HTML("Keywords:"));
		layoutPanel.setWidget(4, 1, keywordsTxt);
		keywordsTxt.setSize("400px", "100px");
		keywordsTxt.getElement().getStyle().setProperty("resize", "none");
		
		layoutPanel.setWidget(5,0, new ScreenButtonsPanel(this));
		layoutPanel.getFlexCellFormatter().setColSpan(5,0,3);
		layoutPanel.getFlexCellFormatter().setHorizontalAlignment(5,0,HasHorizontalAlignment.ALIGN_CENTER);

	}
	
	@Override
	public void refresh(){
		
		if(AppService.get().getCurrentWebPage().getType() == WEBPAGE_TYPE.PERSONAL ){
			personalRB.setValue(true);
			businessTypeTxt.setText("");
			businessTypeTxt.setEnabled(false);
		}else{
			businessRB.setValue(true);
			businessTypeTxt.setEnabled(true);
			businessTypeTxt.setText(AppService.get().getCurrentWebPage().getTypeDescription());
		}
		
		titleTxt.setText(AppService.get().getCurrentWebPage().getTitle());
		descriptionTxt.setText(AppService.get().getCurrentWebPage().getDescription());
		keywordsTxt.setText(AppService.get().getCurrentWebPage().getKeywords());
	}

	@Override
	public void save() {
		if(personalRB.getValue()){
			AppService.get().getCurrentWebPage().setType(WEBPAGE_TYPE.PERSONAL);
			AppService.get().getCurrentWebPage().setTypeDescription("");
		}else{
			AppService.get().getCurrentWebPage().setType(WEBPAGE_TYPE.BUSINESS);
			AppService.get().getCurrentWebPage().setTypeDescription(businessTypeTxt.getText().trim());
		}
		
		AppService.get().getCurrentWebPage().setTitle(titleTxt.getText().trim());
		AppService.get().getCurrentWebPage().setDescription(descriptionTxt.getText().trim());
		AppService.get().getCurrentWebPage().setKeywords(keywordsTxt.getText().trim());
		
		AppService.get().storeWebPage(false);
	}

}
