package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils.ACCOUNT_TYPE;
import com.booix.editor.client.event.WebPageCreateEvent;
import com.booix.editor.client.event.WebPageCreateEventHandler;
import com.booix.editor.client.event.WebPageCreateEvent.WebPageCreateType;
import com.booix.editor.client.screens.ScreenButtonsPanel;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.booix.editor.client.screens.settings.AccountSettingsWebPagesTab.WebPageItem;
import com.booix.editor.client.utils.Validator;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class WebPageCreatorScreen extends DialogBox implements IScreenButtonsPanel, WebPageCreateEventHandler {
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private WebPageItemCreator itemCreator = new WebPageItemCreator();
	private HTML msgL = new HTML();
	private ScreenButtonsPanel buttonsPanel = new ScreenButtonsPanel(this);
	
	WebPageCreatorScreen(){
		setHTML("Create Web Page");
		setAnimationEnabled(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(true) ;
		add(mainPanel);
		mainPanel.add(itemCreator);
		mainPanel.add(msgL);
		mainPanel.add(buttonsPanel);
		
		AppService.get().getEventBus().addHandler(WebPageCreateEvent.TYPE, this);
	}
	
	@Override
	public void show(){
		itemCreator.getPageNameTxt().setText("");
		msgL.setText("");
		super.show();
	}
	
	private class WebPageItemCreator extends Composite {
		private TextBox pageNameTxt = new TextBox();
		public WebPageItemCreator() {
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			initWidget(horizontalPanel);
			horizontalPanel.add(new HTML("URL:"));
			horizontalPanel.add(pageNameTxt);
			horizontalPanel.add(new HTML(".booix.com"));
			pageNameTxt.setWidth("200px");
		}
		public TextBox getPageNameTxt(){
			return pageNameTxt;
		}
	}

	@Override
	public void refresh() {
		msgL.setText("");
		
	}

	@Override
	public void save() {
		if(AppService.get().getCurrentUser().getAccountType() == ACCOUNT_TYPE.FREE){
			if(AppService.get().getCurrentUser().getWebPageList().size() >= ACCOUNT_TYPE.FREE.getMaxWebPages()){
				msgL.setHTML("You can't create more Web Pages with your free account.<br/> Please upgrade for unlimited Web Pages.");
				return;
			}
		}
		if(itemCreator.getPageNameTxt().getText().trim().equals("")){
			msgL.setText("Please add you page name");
			return;
		}
		final String subdomain = itemCreator.getPageNameTxt().getText();
		if(!Validator.isSubdomainValid(subdomain)){
			msgL.setText("Invalid subdomain");
			return;
		}
		
		
		 AppService.get().createWebPage(subdomain);
		 
		 /*
		// Create a new timer that calls Window.alert().
	    Timer t = new Timer() {
	      public void run() {
	    	  AppService.get().createWebPage(subdomain);
	      }
	    };

	    AppController.get().getLoadingDialogBox().show("Calling Create Web Page Create");
	    // Schedule the timer to run once in 5 seconds.
	    t.schedule(5000);*/

	}
	
	@Override
	public void onWebPageCreate(WebPageCreateEvent event) {
		
		if(event.getWebPageCreateType() == WebPageCreateType.SUCCESS){
			AppService.get().getCurrentUser().getWebPageList().add(event.getWebPage());
			msgL.setText("");
			this.hide();
		}else if(event.getWebPageCreateType() == WebPageCreateType.DOMAIN_NOT_AVAILABLE){
			msgL.setText("Domain Is not Available");
		}else{
			msgL.setText("Failed to create web page pelase try again later.");
		}
		
		AppController.get().getAppScreen().showAccountSettingsScreen();
	}

}
