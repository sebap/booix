package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppService;
import com.booix.editor.client.event.DomainEvent;
import com.booix.editor.client.event.DomainEventHandler;
import com.booix.editor.client.event.DomainEvent.DomainEventType;
import com.booix.editor.client.screens.ScreenButtonsPanel;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.booix.editor.client.utils.Validator;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;

public class WebPageSettingsURLTab extends Composite implements IScreenButtonsPanel, DomainEventHandler {
	
	private FlexTable layoutPanel = new FlexTable();
	private ScrollPanel mainPanel = new ScrollPanel(layoutPanel);
	
	private TextBox webPageURLTxt = new TextBox();
	private Label msgL = new Label();
	
	private String subdomainToSave = "";
	private WebPageSettingsScreen parent = null;
	
	public WebPageSettingsURLTab(WebPageSettingsScreen parent) {
		this.parent = parent;
		initWidget(mainPanel);
		createDisplay();
		AppService.get().getEventBus().addHandler(DomainEvent.getType(), this);
	}
	
	private void createDisplay(){
		
		layoutPanel.getElement().getStyle().setProperty("margin", "0 auto");
		
		layoutPanel.setWidth("400px");
		
		layoutPanel.setWidget(0, 0, new HTML("URL:"));
		HorizontalPanel hp = new HorizontalPanel();
		hp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		hp.add(webPageURLTxt);
		webPageURLTxt.setWidth("250px");
		hp.add(new HTML(".booix.com"));
		layoutPanel.setWidget(0, 1, hp);
		layoutPanel.setWidget(1, 0, msgL);
		layoutPanel.getFlexCellFormatter().setColSpan(1,0,3);
		
		layoutPanel.setWidget(2,0, new ScreenButtonsPanel(this));
		layoutPanel.getFlexCellFormatter().setColSpan(2,0,3);
		layoutPanel.getFlexCellFormatter().setHorizontalAlignment(2,0,HasHorizontalAlignment.ALIGN_CENTER);
		
		webPageURLTxt.addKeyUpHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				String subdomain = webPageURLTxt.getText().trim().toLowerCase();
				if(!Validator.isSubdomainValid(subdomain)){
					msgL.setText("Invalid subdomain");
				}else{
					msgL.setText("");
				}
				
			}
		});
		
	}
	
	@Override
	public void refresh(){
		webPageURLTxt.setText(AppService.get().getCurrentWebPage().getSubDomain());
		msgL.setText("");
	}

	
	@Override
	public void save() {

		String subdomain = webPageURLTxt.getText().trim().toLowerCase();

		if(!Validator.isSubdomainValid(subdomain)){
			Window.alert("Not empty, without spaces, longer than 4");
			return;
		}
		
		//we will call this and then after checking we will process the result 
		//once we get the answer from the server
		subdomainToSave = subdomain;
		AppService.get().checkDomain(subdomainToSave);
	}

	@Override
	public void onDomainCheckEvent(DomainEvent event) {
		if(event.getDomainEventType() == DomainEventType.VALID){
			//we can continue here because the domain is available
			AppService.get().getCurrentWebPage().setSubDomain(subdomainToSave);
			AppService.get().storeWebPage(false);
		}else if(event.getDomainEventType() == DomainEventType.INVALID){
			//Window.alert("Subdomain Is not available. Please try another one.");
			msgL.setText("Subdomain is already in use, please chooose another one.");
		}
	}

}
