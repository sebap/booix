package com.booix.editor.client.screens.settings;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.event.WebPageDeleteEvent;
import com.booix.editor.client.event.WebPageDeleteEventHandler;
import com.booix.editor.client.event.WebPageRetrievedEvent;
import com.booix.editor.client.event.WebPageRetrievedEventHandler;
import com.booix.editor.client.screens.PrettyButton;
import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AccountSettingsWebPagesTab extends Composite 
implements WebPageRetrievedEventHandler, WebPageDeleteEventHandler
{
	private FlexTable layoutPanel = new FlexTable();
	private ScrollPanel mainPanel = new ScrollPanel(layoutPanel);
	private VerticalPanel webPagePanel = new VerticalPanel();
	private PrettyButton createBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.pageAddIcon()), "Create Web Page");
	private HTML msgL = new HTML();
	
	private WebPageCreatorScreen webPageCreatorScreen = new WebPageCreatorScreen();
	
	public AccountSettingsWebPagesTab() {
		initWidget(mainPanel);
		createDisplay();
		
		AppService.get().getEventBus().addHandler(WebPageRetrievedEvent.TYPE, this);
		AppService.get().getEventBus().addHandler(WebPageDeleteEvent.TYPE, this);
	}
	
	private void createDisplay(){
		
		layoutPanel.getElement().getStyle().setProperty("margin", "0 auto");
		layoutPanel.setWidget(0, 0, webPagePanel);
		
		createBtn.getButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				webPageCreatorScreen.show();
				webPageCreatorScreen.center();
			}
		});
		
		layoutPanel.setWidget(1,0, createBtn);
		layoutPanel.getFlexCellFormatter().setColSpan(5,0,3);
		layoutPanel.getFlexCellFormatter().setHorizontalAlignment(5,0,HasHorizontalAlignment.ALIGN_CENTER);

	}

	public void refresh(){
		webPagePanel.clear();
		for (WebPageWidget currentWebPage : AppService.get().getCurrentUser().getWebPageList()) {
			WebPageItem webPageItem = new WebPageItem(currentWebPage.getId());
			webPageItem.getWebPageLabel().setText(currentWebPage.getSubDomain()+".booix.com");
			webPagePanel.add(webPageItem);
		}
	}

	public class WebPageItem extends Composite {
		private Label webPageLabel = new Label();
		private PrettyButton deleteBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.pageDeleteIcon()),"Delete");
		private PrettyButton editBtn = new PrettyButton(new Image(AppUtils.APP_IMAGE_BUNDLE.upIcon()), "Edit");
		
		private int id = 0;

		public WebPageItem(int id) {
			this.id = id;
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			initWidget(horizontalPanel);
			horizontalPanel.add(webPageLabel);
			horizontalPanel.add(deleteBtn);
			horizontalPanel.add(editBtn);
			webPageLabel.setWidth("209px");
			
			deleteBtn.getButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					if(Window.confirm("Are you sure you want to delete this web page?")){
						webPagePanel.remove(WebPageItem.this);
						msgL.setText("");
						AppService.get().deleteWebPage(WebPageItem.this.getWebPageId());
						
						/*
						// Create a new timer that calls Window.alert().
					    Timer t = new Timer() {
					      public void run() {
					    	  
					      }
					    };

					    AppController.get().getLoadingDialogBox().show("Calling Delete Web Page...");
					    // Schedule the timer to run once in 5 seconds.
					    t.schedule(5000);*/
					}
				}
			});
			
			editBtn.getButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					AppController.get().getAppScreen().showWebPageEditorScreen();
					AppService.get().loadWebPage(WebPageItem.this.getWebPageId());
				}
			});
		}
		
		public Label getWebPageLabel(){
			return webPageLabel;
		}
		
		public int getWebPageId(){
			return id;
		}
		
	}

	@Override
	public void onWebPageRetrieved(WebPageRetrievedEvent event) {
		refresh();
	}

	@Override
	public void onWebPageDelete(WebPageDeleteEvent event) {
		AppService.get().getCurrentUser().removeWebPage(event.getWebPage().getId());
		refresh();
	}

}
