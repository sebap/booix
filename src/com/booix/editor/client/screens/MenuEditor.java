package com.booix.editor.client.screens;

import java.util.ArrayList;
import java.util.TreeSet;

import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.ACCOUNT_TYPE;
import com.booix.editor.client.screens.ScreenButtonsPanel.IScreenButtonsPanel;
import com.booix.editor.client.widgets.DnDWidgetContainer;
import com.booix.editor.client.widgets.InnerPageWidget;
import com.booix.editor.client.widgets.TitleWidget;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MenuEditor extends DialogBox implements IScreenButtonsPanel {
	private VerticalPanel mainPanel = new VerticalPanel();
	private VerticalPanel pagesPanel = new VerticalPanel(); 
	private Label msgL = new Label();

	public MenuEditor() {
		setHTML("Menu Screen");
		setAnimationEnabled(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(false) ;
		add(mainPanel);
		createEditor();
	}
	
	
	/**
	 * Used to create and recreate the edit is opened.
	 */
	private void createEditor(){
		
		
		updatePagesPanel();
		mainPanel.add(pagesPanel);
		mainPanel.add(new MenuItemCreator());
		mainPanel.add(msgL);
		mainPanel.add(new ScreenButtonsPanel(this));
		
		msgL.getElement().getStyle().setProperty("color", "red");
	}
	
	private void updatePagesPanel(){
		pagesPanel.clear();
		if(AppService.get().getCurrentWebPage() != null){
			int index = 0;
			for (InnerPageWidget pageWidget : AppService.get().getCurrentWebPage().getInnerPageList()) {
				MenuItemEditor item = new MenuItemEditor(index);
				item.getPageNameTxt().setText(pageWidget.getName());
				pagesPanel.add(item);
				index++;
			}
		}
	}
	
	@Override
	public void show() {
		updatePagesPanel();
		msgL.setText("");
		super.show();
	}
	
	@Override
	public void save(){
		ArrayList<InnerPageWidget> pagesList = new ArrayList<InnerPageWidget>();

		TreeSet<String> uniqueNames = new TreeSet<String>();
		
		for (int i = 0; i < pagesPanel.getWidgetCount(); i++) {
			MenuItemEditor item = (MenuItemEditor)pagesPanel.getWidget(i);

			if(item.getPageNameTxt().getText().trim().equals("")){
				msgL.setText("Cant create empty page name");
				return;
			}
			
			if(!uniqueNames.contains(item.getPageNameTxt().getText().trim().toLowerCase())){
				uniqueNames.add(item.getPageNameTxt().getText().trim().toLowerCase());
			}else{
				msgL.setText("Please avoid duplicated name:" + item.getPageNameTxt().getText() );
				return;
			}
			
			
			
			InnerPageWidget pageWidget = null;
			if(item.getId() == -1){ //if it is new it will have -1
				pageWidget = new InnerPageWidget();
				TitleWidget child = new TitleWidget();
				child.setTitle(item.getPageNameTxt().getText());
				pageWidget.getMainContentWidgets().add(new DnDWidgetContainer(child, null));
			}else{
				pageWidget = AppService.get().getCurrentWebPage().getInnerPageList().get(item.getId());
			}
			pageWidget.setName(item.getPageNameTxt().getText());
			pagesList.add(pageWidget);
		}
		
		AppService.get().getCurrentWebPage().setInnerPageList(pagesList);
		AppController.get().getAppScreen().getWebPageEditor().update();
		super.hide();
	}
	
	@Override
	public void refresh(){
		super.hide();
	}
	
	
	public class MenuItemEditor extends Composite {
		private TextBox pageNameTxt = new TextBox();
		private PushButton upBtn = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.upIcon());
		private PushButton downBtn = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.downIcon());
		private PushButton deleteBtn = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.pageDeleteIcon());
		
		private int id = 0;

		public MenuItemEditor(int id) {
			this.id = id;
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			initWidget(horizontalPanel);
			horizontalPanel.add(pageNameTxt);
			horizontalPanel.add(upBtn);
			horizontalPanel.add(downBtn);
			horizontalPanel.add(deleteBtn);
			pageNameTxt.setWidth("200px");
			
			upBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					try{
						pagesPanel.insert(MenuItemEditor.this, pagesPanel.getWidgetIndex(MenuItemEditor.this) - 1);
						msgL.setText("");
					}catch(Exception e){
						Log.error(e.toString());
					}
					
				}
			});
			
			downBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					try{
						pagesPanel.insert(MenuItemEditor.this, pagesPanel.getWidgetIndex(MenuItemEditor.this) + 2);
						msgL.setText("");
					}catch(Exception e){
						Log.error(e.toString());
					}
					
				}
			});
			
			deleteBtn.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					if(pagesPanel.getWidgetCount() == 1){
						msgL.setText("You can't delete your last Page");
						return;
					}
					
					if(Window.confirm("Are you sure you want to delete this page?")){
						pagesPanel.remove(MenuItemEditor.this);
						msgL.setText("");
					}
				}
			});
		}
		
		public TextBox getPageNameTxt(){
			return pageNameTxt;
		}
		
		public int getId(){
			return id;
		}
	}
	
	public class MenuItemCreator extends Composite {
		private TextBox pageNameTxt = new TextBox();
		private PushButton addBtn = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.pageAddIcon());

		public MenuItemCreator() {
			
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			initWidget(horizontalPanel);
			horizontalPanel.add(pageNameTxt);
			horizontalPanel.add(addBtn);
			pageNameTxt.setWidth("200px");
			
			addBtn.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					
					if(AppService.get().getCurrentUser().getAccountType() == ACCOUNT_TYPE.FREE){
						if(pagesPanel.getWidgetCount() == ACCOUNT_TYPE.FREE.getMaxPages()){
							msgL.setText("You can't create more pages with a free account");
							return;
						}
					}
					
					if(MenuItemCreator.this.getPageNameTxt().getText().trim().equals("")){
						msgL.setText("Please add you page name");
						return;
					}
					MenuItemEditor item = new MenuItemEditor( -1 );
					item.getPageNameTxt().setText(MenuItemCreator.this.getPageNameTxt().getText());
					pagesPanel.add(item);
					MenuItemCreator.this.getPageNameTxt().setText("");
					msgL.setText("");
				}
			});
		}
		
		public TextBox getPageNameTxt(){
			return pageNameTxt;
		}
	}

}
