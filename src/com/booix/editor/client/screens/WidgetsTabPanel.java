package com.booix.editor.client.screens;

import java.util.ArrayList;
import java.util.List;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WidgetType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class WidgetsTabPanel extends Grid implements ResizeHandler {
	
	private VerticalPanel widgetButtonsContainersPanel = new VerticalPanel();
	private ScrollPanel widgetsScrollPanel = new ScrollPanel(widgetButtonsContainersPanel);
	private List<PushButton> widgetButtonContainers = new ArrayList<PushButton>();
	private int SCROLL_RIGHT_MARGIN = 22;
	
	public WidgetsTabPanel() {

		resize(1, 1);
		
		widgetButtonsContainersPanel.addStyleName(AppUtils.CSS_PREFIX + "WidgetButtonsPanel");
		//widgetsScrollPanel.setWidth(Window.getClientWidth() - SCROLL_RIGHT_MARGIN + "px");
		widgetsScrollPanel.setHeight("140px");
		
		//setWidget(0, 0, widgetsScrollPanel);
		setWidget(0, 0, widgetButtonsContainersPanel);
		
		reloadTab();
		
		bind();
	}
	
	private void bind(){
		
		Window.addResizeHandler(this);
		
		widgetButtonContainers.get(0).addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				AppController.get().getAppScreen().createTitleWidget();
			}
		});
		
		widgetButtonContainers.get(1).addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				AppController.get().getAppScreen().createTextWidget();
			}
		});
		
		widgetButtonContainers.get(2).addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				AppController.get().getAppScreen().createImageTextWidget();
			}
		});
		
		widgetButtonContainers.get(3).addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				AppController.get().getAppScreen().createVideoTextWidget();
			}
		});

	}
	
	public List<PushButton> getAllButtonsContainers(){
		return widgetButtonContainers;
	}

	@Override
	public void onResize(ResizeEvent event) {
		//widgetsScrollPanel.setWidth(Window.getClientWidth() - SCROLL_RIGHT_MARGIN + "px");
	}
	
	public void loadTab(){
		reloadTab();
	}
	
	public void reloadTab(){
		widgetButtonContainers.clear();
		widgetButtonsContainersPanel.clear();
		
		//title
		WidgetButton temp1 = new WidgetButton(WidgetType.TITLE, new Image(AppUtils.APP_IMAGE_BUNDLE.titleWidgetIcon()));
		widgetButtonContainers.add( temp1 );
		widgetButtonsContainersPanel.add(temp1);
		AppController.get().getDragController().makeDraggable(temp1);
		
		//text
		WidgetButton temp2 = new WidgetButton(WidgetType.TEXT, new Image(AppUtils.APP_IMAGE_BUNDLE.textWidgetIcon()));
		widgetButtonContainers.add( temp2 );
		widgetButtonsContainersPanel.add(temp2);
		AppController.get().getDragController().makeDraggable(temp2);
		
		//imagetext
		WidgetButton temp3 = new WidgetButton( WidgetType.IMAGE_TEXT, new Image(AppUtils.APP_IMAGE_BUNDLE.imageTextWidgetIcon()));
		widgetButtonContainers.add( temp3 );
		widgetButtonsContainersPanel.add(temp3);
		AppController.get().getDragController().makeDraggable(temp3);
		
		//videotext
		WidgetButton temp4 = new WidgetButton( WidgetType.VIDEO_TEXT, new Image(AppUtils.APP_IMAGE_BUNDLE.videoWidgetIcon()));
		widgetButtonContainers.add( temp4 );
		widgetButtonsContainersPanel.add(temp4);
		AppController.get().getDragController().makeDraggable(temp4);
		
		
	}
	
	public class WidgetButton extends PushButton{
		private WidgetType type = null; 
		public WidgetButton(WidgetType type, Image image) {
			super(image);
			this.type = type;
		}
		
		public WidgetType getType(){
			return type;
		}
	}

}
