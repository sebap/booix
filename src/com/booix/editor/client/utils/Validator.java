package com.booix.editor.client.utils;

 
public class Validator{
 
	private final static String EMAIL_VALIDATION_REGEX = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?"; 
	/**
	 * 
	 * @param email
	 * @return true if the email is valid
	 */
	public static boolean isEmailValid(String email) {
		return email.matches(EMAIL_VALIDATION_REGEX)  ;
	} 
	
	
	private final static String SUBDOMAIN_VALIDATION_REGEX = "[a-zA-Z0-9]+";
	public static boolean isSubdomainValid(String subdomain) {

		if( subdomain.equals("")  || subdomain.length() < 4 ){
			return false;
		}
		boolean flag =  subdomain.matches(SUBDOMAIN_VALIDATION_REGEX)  ;
		
		return flag;
	} 
}