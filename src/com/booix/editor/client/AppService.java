package com.booix.editor.client;

import com.booix.editor.client.model.Template;
import com.booix.editor.client.model.User;
import com.booix.editor.client.widgets.InnerPageWidget;
import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;


/**
 * This class contains all the values accessible from everywhere
 * for this session.
 * 
 * Values in this class can change over time, based on user choices.
 * @author Owner
 *
 */
public class AppService {
	
	public static String ABSOLUTE_PATH_TO_TEMPLATES_ICONS = GWT.getModuleBaseURL() + "server/images/icons/templates";
	public static String ABSOLUTE_PATH_TO_TEMPLATES = GWT.getModuleBaseURL() + "server/templates";
	public static Template DEFAULT_TEMPLATE = new Template();
	public static int NUMBER_OF_TEMPLATES = 5;
	public static int NUMBER_OF_WIDGETS = 4;
	
	private static AppService instance = null;
	private AppMessages messages = null;
	private SimpleEventBus eventBus = null;
	private AppProxy proxy = null;
	private WebPageWidget currentWebPage = null;
	private int currentInnerPage = 0;
	private User currentUser = null;
	
	public enum DropControllerEnum{
		MAIN,
		RIGHT,
		LEFT;
	}
	
	public static AppService get(){
		if (instance == null){
			instance = new AppService();
		}
		return instance;
			 
	}
	
	public void setCurrentWebPage(WebPageWidget currentWebPage1){
		currentWebPage = currentWebPage1;
	}
	
	public WebPageWidget getCurrentWebPage(){
		return currentWebPage;
	}
	
	public int getCurrentInnerPageId() {
		return currentInnerPage;
	}
	
	public InnerPageWidget getCurrentInnerPageWidget(){
		return currentWebPage.getInnerPageList().get(currentInnerPage);
		
	}
	
	public void setCurrentInnerPageId(int currentInnerPage) {
		this.currentInnerPage = currentInnerPage;
	}
	
	public void setMessages(AppMessages messages){
		this.messages = messages;
	}
	
	public AppMessages getMessages(){
		return messages;
	}
	
	public void setEventBus(SimpleEventBus eventBus){
		this.eventBus = eventBus;
	}
	
	public SimpleEventBus getEventBus(){
		return this.eventBus;
	}
	
	public void setProxy(AppProxy proxy){
		this.proxy = proxy;
	}
	
	public void loadTemplate(int id){
		proxy.loadTemplate(id);
	}
	
	public void loadWebPage(int pageId){
		proxy.loadWebPage(currentUser, pageId);
	}
	
	public void checkDomain(String subdomain){
		proxy.checkDomain(subdomain);
	}
	
	public void storeWebPage(boolean displayPopup){
		proxy.storeWebPage(displayPopup);
	}
	
	public void createWebPage(String subdomain){
		proxy.createWebPage(subdomain);
	}
	
	public void deleteWebPage(int webPageId){
		proxy.deleteWebPage(webPageId);
	}
	
	public void deleteCurrentUser(){
		proxy.deleteCurrentUser();
	}
	
	public void signup(User user){
		proxy.registerUser(user);
	}
	
	public void login(User user){
		proxy.login(user);
	}
	
	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
	
	
	public void setNewCSSStyleFile(int newId){
		String oldStyleURL = "template"+currentWebPage.getTemplate().getId()+"/style.css";
		removeCSSStyleFiles(oldStyleURL);
		String newCSSStyleFileURL = GWT.getModuleBaseURL() + "server/templates/template"+newId+"/style.css";
		addCSSStyleFile(newCSSStyleFileURL);
	}
	
	/**
	 * Utility method to add a css style file 
	 */
	public static native void addCSSStyleFile(String url) /*-{
	  	var style = $wnd.document.createElement("link");
	  	style.setAttribute("rel", "stylesheet")
	  	style.setAttribute("type", "text/css")
	  	style.setAttribute("href", url)
		var head = $wnd.document.getElementsByTagName("head")[0];
		if(head){
			head.appendChild(style);
		}
	}-*/;
	
	/**
	 * Utility method to add a css style file 
	 */
	public static native void removeCSSStyleFiles(String oldfilename) /*-{
		oldfilename="style.css";
	  	//determine element type to create nodelist using
		//var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none"; 
		var targetelement = "link" ;
		//determine corresponding attribute to test for
		//var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none";
		var targetattr = "href" ;
		var allsuspects=$wnd.document.getElementsByTagName(targetelement);
		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename)!=-1){
				allsuspects[i].parentNode.removeChild(allsuspects[i]);
			}
		}
	}-*/;
	
	public native void replaceCSSStyleFile (String oldfilename, String newfilename)/*-{
		//determine element type to create nodelist using
		var targetelement="link"; 
		//determine corresponding attribute to test for
		var targetattr="href";
		var allsuspects=$wnd.document.getElementsByTagName(targetelement);
		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename)!=-1){
				var newelement = $wnd.document.createElement("link");
				newelement.setAttribute("rel", "stylesheet");
				newelement.setAttribute("type", "text/css");
				newelement.setAttribute("href", newfilename);
				allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i]);
			}
		}
	}-*/;

}

/**
 * Resources:
 * 
 * TinyMCE Rich text editor
 * URL http://development.lombardi.com/?p=43
 * 
 * */
