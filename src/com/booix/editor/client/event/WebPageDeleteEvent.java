package com.booix.editor.client.event;

import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event gets triggered when a new webpage is Deleted
 * 
 * @author Owner
 * 
 */

public class WebPageDeleteEvent extends
		GwtEvent<WebPageDeleteEventHandler> {
	public static Type<WebPageDeleteEventHandler> TYPE = new Type<WebPageDeleteEventHandler>();
	private final WebPageWidget webpage;

	public WebPageDeleteEvent(WebPageWidget webpage) {
		this.webpage = webpage;
	}

	public WebPageWidget getWebPage() {
		return webpage;
	}

	@Override
	public Type<WebPageDeleteEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(WebPageDeleteEventHandler handler) {
		handler.onWebPageDelete(this);
	}

}