package com.booix.editor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface WebPageCreateEventHandler extends EventHandler {
	  void onWebPageCreate(WebPageCreateEvent event);
	}