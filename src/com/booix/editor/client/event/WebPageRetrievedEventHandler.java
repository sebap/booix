package com.booix.editor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface WebPageRetrievedEventHandler extends EventHandler {
	  void onWebPageRetrieved(WebPageRetrievedEvent event);
	}