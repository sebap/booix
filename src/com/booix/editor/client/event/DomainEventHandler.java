package com.booix.editor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DomainEventHandler extends EventHandler {
	  void onDomainCheckEvent(DomainEvent event);
}