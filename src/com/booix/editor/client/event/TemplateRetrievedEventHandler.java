package com.booix.editor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TemplateRetrievedEventHandler extends EventHandler {
	  void onTemplateRetrieved(TemplateRetrievedEvent event);
	}