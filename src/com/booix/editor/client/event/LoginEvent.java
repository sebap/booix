package com.booix.editor.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * 
 * @author Sebastian Pereyro.
 * 
 */

public class LoginEvent extends GwtEvent<LoginEventHandler> {
	public static Type<LoginEventHandler> TYPE = new Type<LoginEventHandler>();
	
	@Override
	public Type<LoginEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoginEventHandler handler) {
		handler.onLoginEvent(this);
	}
	
	

	 public LoginEvent(LoginEventType loginEventType) {
	  this.loginEventType = loginEventType;
	 }

	 public static Type<LoginEventHandler> getType() {
		 return TYPE;
	 }
	 
	// Code specific to this custom event

	 public enum LoginEventType {
	   SUCCESS, FAIL 
	 }
	 
	 private final LoginEventType loginEventType;
	 
	 public LoginEventType getLoginEventType(){
	  return loginEventType;  
	 }
	
	
	
}