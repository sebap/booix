package com.booix.editor.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface WebPageDeleteEventHandler extends EventHandler {
	  void onWebPageDelete(WebPageDeleteEvent event);
	}