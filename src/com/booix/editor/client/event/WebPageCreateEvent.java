package com.booix.editor.client.event;

import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event gets triggered when a new webpage is created
 * 
 * @author Owner
 * 
 */

public class WebPageCreateEvent extends
		GwtEvent<WebPageCreateEventHandler> {
	public static Type<WebPageCreateEventHandler> TYPE = new Type<WebPageCreateEventHandler>();
	private final WebPageWidget webpage;
	private final WebPageCreateType webPageCreateType;

	public WebPageCreateEvent(WebPageWidget webpage, WebPageCreateType webPageCreateType) {
		this.webpage = webpage;
		 this.webPageCreateType = webPageCreateType;
	}
	
	// Code specific to this custom event
	public enum WebPageCreateType {
		SUCCESS, 
		DOMAIN_NOT_AVAILABLE,
		GENERIC_FAILURE;
	}

	public WebPageWidget getWebPage() {
		return webpage;
	}

	@Override
	public Type<WebPageCreateEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(WebPageCreateEventHandler handler) {
		handler.onWebPageCreate(this);
	}
	
	public WebPageCreateType getWebPageCreateType(){
		return webPageCreateType;
	}
	

}