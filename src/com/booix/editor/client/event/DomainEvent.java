package com.booix.editor.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * 
 * @author Sebastian Pereyro.
 * 
 */

public class DomainEvent extends GwtEvent<DomainEventHandler> {
	
	public static Type<DomainEventHandler> TYPE = new Type<DomainEventHandler>();
	
	@Override
	public Type<DomainEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DomainEventHandler handler) {
		handler.onDomainCheckEvent(this);
	}
	
	public enum DomainEventType {
		VALID, INVALID
	}
	
	/**
	 * This is the type of the DomainEvent
	 */
	private final DomainEventType domainEventType;
	
	public DomainEvent(DomainEventType domainCheckEventType) {
		this.domainEventType = domainCheckEventType;
	}
	
	public static Type<DomainEventHandler> getType() {
		return TYPE;
	}
	
	public DomainEventType getDomainEventType(){
		return domainEventType;
	}
	
	
	
}