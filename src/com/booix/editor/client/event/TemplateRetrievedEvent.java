package com.booix.editor.client.event;

import com.booix.editor.client.model.Template;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event gets triggered when a new template is loaded from the server It is
 * responsability of the event bus to update its listeners.
 * 
 * @author Owner
 * 
 */

public class TemplateRetrievedEvent extends
		GwtEvent<TemplateRetrievedEventHandler> {
	public static Type<TemplateRetrievedEventHandler> TYPE = new Type<TemplateRetrievedEventHandler>();
	private final Template template;

	public TemplateRetrievedEvent(Template template) {
		this.template = template;
	}

	public Template getTemplate() {
		return template;
	}

	@Override
	public Type<TemplateRetrievedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TemplateRetrievedEventHandler handler) {
		handler.onTemplateRetrieved(this);

	}

}