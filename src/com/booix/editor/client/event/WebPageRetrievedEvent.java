package com.booix.editor.client.event;

import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event gets triggered when a new template is loaded from the server It is
 * responsability of the event bus to update its listeners.
 * 
 * @author Owner
 * 
 */

public class WebPageRetrievedEvent extends
		GwtEvent<WebPageRetrievedEventHandler> {
	public static Type<WebPageRetrievedEventHandler> TYPE = new Type<WebPageRetrievedEventHandler>();
	private final WebPageWidget webpage;

	public WebPageRetrievedEvent(WebPageWidget webpage) {
		this.webpage = webpage;
	}

	public WebPageWidget getWebPage() {
		return webpage;
	}

	@Override
	public Type<WebPageRetrievedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(WebPageRetrievedEventHandler handler) {
		handler.onWebPageRetrieved(this);
	}

}