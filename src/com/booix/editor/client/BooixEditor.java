package com.booix.editor.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BooixEditor implements EntryPoint {
	
	public void onModuleLoad(){
		
		//Setting up messages
		AppMessages messages = (AppMessages)GWT.create(AppMessages.class);
		AppService.get().setMessages(messages);
		
		AppService.get().setEventBus(new SimpleEventBus());
		
		AppProxy proxy = new AppProxy();
		AppService.get().setProxy(proxy);

		//We are not using Facebook API for logging for now.
		//AppFacebookAPI.get().init();
		
		AppController.get().initApp();
		
	}
	
	

}
