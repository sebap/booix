package com.booix.editor.client;

import java.util.ArrayList;
import java.util.List;

import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppUtils.RESPONSE_TYPE;
import com.booix.editor.client.AppUtils.SERVICE_ACTION_TYPE;
import com.booix.editor.client.AppUtils.USER_TYPE;
import com.booix.editor.client.AppUtils.WEBPAGE_TYPE;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.event.DomainEvent;
import com.booix.editor.client.event.LoginEvent;
import com.booix.editor.client.event.TemplateRetrievedEvent;
import com.booix.editor.client.event.WebPageCreateEvent;
import com.booix.editor.client.event.WebPageDeleteEvent;
import com.booix.editor.client.event.WebPageRetrievedEvent;
import com.booix.editor.client.event.DomainEvent.DomainEventType;
import com.booix.editor.client.event.LoginEvent.LoginEventType;
import com.booix.editor.client.event.WebPageCreateEvent.WebPageCreateType;
import com.booix.editor.client.model.Background;
import com.booix.editor.client.model.Template;
import com.booix.editor.client.model.User;
import com.booix.editor.client.model.response.DomainServiceResponse;
import com.booix.editor.client.model.response.UserServiceResponse;
import com.booix.editor.client.model.response.WebPageServiceResponse;
import com.booix.editor.client.widgets.BannerWidget;
import com.booix.editor.client.widgets.DnDWidgetContainer;
import com.booix.editor.client.widgets.IWidget;
import com.booix.editor.client.widgets.ImageTextWidget;
import com.booix.editor.client.widgets.InnerPageWidget;
import com.booix.editor.client.widgets.TextWidget;
import com.booix.editor.client.widgets.TitleWidget;
import com.booix.editor.client.widgets.VideoTextWidget;
import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;

/**
 * This class is in charge of doing all the requests to the DB.
 * 
 * @author Owner
 * 
 */
public class AppProxy {

	/****************************************************************
	 * User Section
	 ****************************************************************/
	
	public void deleteCurrentUser(){
		/**
		 * We will delete teh current user here
		 * Once it has been deleted, we will sign him out.
		 */
	}
	
	/**
	 * @param user
	 */
	public void registerUser(User user) {

		AppController.get().getLoadingDialogBox().show("Registering user.." + user.getEmail());
		String htmlContextFileName = "server/userService.php";
		String url = GWT.getModuleBaseURL() + htmlContextFileName;
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);
		builder.setHeader("Content-Type","application/x-www-form-urlencoded");
	    try {
	    	builder.sendRequest("action="+SERVICE_ACTION_TYPE.USER_STORE.toString()+"&" + user.generatePOSTdata(),new RequestCallback() {
	    		
	    		@Override
				public void onError(Request request, Throwable exception) {
					Window.alert("Error While storing user");
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					
					Log.debug(response.getText());
					UserServiceResponse userResponse = parseUserResponse(response.getText());
					
					AppController.get().getLoadingDialogBox().hide();
					
					if(userResponse.getType().equals(AppUtils.RESPONSE_TYPE.USER_STORE_SUCCESS)){
						User user = userResponse.getUser();
						login(user);
					}else if(userResponse.getType().equals(RESPONSE_TYPE.USER_STORE_FAIL)){
						Window.alert("Error registering user, please try later or contact support");
					}else if(userResponse.getType().equals(RESPONSE_TYPE.USER_STORE_ALREADY_EXIST)){
						Window.alert("Error registering user, User Already Exist");
					}else if(userResponse.getType().equals(AppUtils.RESPONSE_TYPE.GENERIC_FAILURE)) {
						Window.alert("Genereic failure");
					}
				}
			});
		} catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
	    	Window.alert("Failed to send the request: " + e.getMessage());
		}
	}
	
	/**
	 * This method is used to try to login the current user
	 * The current user is only a facebook user for now, but we might want to end up extending this to support more user
	 * Sice facebook is the stronges social media we think we need to aim in that direction.
	 */
	public void login(final User user){
		AppController.get().getLoadingDialogBox().show("Please wait while we log you in...");
		String htmlContextFileName = "server/userService.php";
		String url = GWT.getModuleBaseURL() + htmlContextFileName;
		String action = "";
		if(user.getUserType() == USER_TYPE.FACEBOOK){
			action = SERVICE_ACTION_TYPE.FACEBOOK_USER_LOGIN.toString();
		}else{
			action = SERVICE_ACTION_TYPE.BOOIX_USER_LOGIN.toString();
		}
		
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);
		builder.setHeader("Content-Type","application/x-www-form-urlencoded");
		
		try {
	    	builder.sendRequest("action=" + action + "&" + user.generatePOSTdata(),new RequestCallback() {
				
				@Override
				public void onResponseReceived(Request request, Response response) {
					Log.debug(response.getText());
					UserServiceResponse userResponse = parseUserResponse(response.getText());

					AppController.get().getLoadingDialogBox().hide();
					
					if (userResponse.getType().equals(AppUtils.RESPONSE_TYPE.USER_LOGIN_SUCCESSED)){
						user.setId(userResponse.getUser().getId());
						user.setWebPageList(userResponse.getUser().getWebPageList());
						AppService.get().setCurrentUser(user);
						AppService.get().getEventBus().fireEvent(new LoginEvent(LoginEventType.SUCCESS));
						//AppController.get().getLoadingDialogBox().hide();  DO not hide the message, because we want to show we are loading the WebPage
					}else if(userResponse.getType().equals(AppUtils.RESPONSE_TYPE.USER_LOGIN_FAILED)){
						AppService.get().getEventBus().fireEvent(new LoginEvent(LoginEventType.FAIL));
						
					} 
				}
				
				@Override
				public void onError(Request request, Throwable exception) {
					AppService.get().setCurrentUser(null);
					AppService.get().getEventBus().fireEvent(new LoginEvent(LoginEventType.FAIL));
					AppController.get().getLoadingDialogBox().hide();
				}
	    	});
	    } catch (RequestException e) {
	    	AppController.get().getLoadingDialogBox().hide();
	    	Window.alert("Failed to send the request: " + e.getMessage());
	    }
	}
	
	
	
	private UserServiceResponse parseUserResponse(String xml){
		try{
			Document customerDom = XMLParser.parse(xml);
			Element customerElement = customerDom.getDocumentElement();
			XMLParser.removeWhitespace(customerElement);
			
			Element userNode = (Element)customerElement.getElementsByTagName("object").item(0).getFirstChild();
			UserServiceResponse userResponse = new UserServiceResponse();
			userResponse.setType(RESPONSE_TYPE.valueOf(getElementTextValue(customerElement, "type")));
			userResponse.setMessage(getElementTextValue(customerElement, "message"));
			userResponse.setUser(parseUser(userNode));
			return userResponse;
		}catch (Exception e){
			Log.error(xml);
			Log.error(e.toString());
			e.printStackTrace();
		}
		UserServiceResponse userResponse = new UserServiceResponse();
		userResponse.setType(RESPONSE_TYPE.GENERIC_FAILURE);
		return userResponse;
	}
	
	private User parseUser(Element userElement){
		try{
			if(userElement != null){
				User user = new User(USER_TYPE.valueOf(getElementTextValue(userElement, "userType")));
				user.setId(getElementIntValue(userElement, "id"));
				user.setEmail(getElementTextValue(userElement, "email"));
				user.setPassword(getElementTextValue(userElement, "password"), false);
				//Element webPagesNode = (Element)userElement.getElementsByTagName("webPages").item(0).getFirstChild();
				NodeList webpages = userElement.getElementsByTagName("webpage");
				List<WebPageWidget> webPageList = new ArrayList<WebPageWidget>();
				for (int i=0; i < webpages.getLength(); i++){
					Node webPageNode = webpages.item(i);
					if(webPageNode instanceof Element){
						WebPageWidget webPage = new WebPageWidget();
						webPage.setId(getElementIntValue((Element)webPageNode, "id"));
						webPage.setSubDomain(getElementTextValue((Element)webPageNode, "subdomain"));
						webPageList.add(webPage);
					}
				}
				user.setWebPageList(webPageList);
				return user;
			}else{
				return null;
			}
		}catch(Exception e){
			Log.error(e.toString());
			return null;
		}
		
		
	}
	
	/********************************************************************
	 * Template Loader Section
	 ********************************************************************/
	/**
	 * @param id
	 */
	public void loadTemplate(int id){
		
		AppController.get().getLoadingDialogBox().show("Loading Template...");
		Log.debug("Loading Template...");
		
		String htmlContextFileName = "server/templates/template"+id+"/template.xml";
		
		String url = GWT.getModuleBaseURL() + htmlContextFileName;

		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);

		try {
			Request response = builder.sendRequest(Long.toString(System
					.currentTimeMillis()), new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.getMessage());
				}

				public void onResponseReceived(Request request, Response response) {
					
					String xml = response.getText();
					Template template = parseTemplate(xml);
					if(template != null){
						AppService.get().getCurrentWebPage().setTemplate(template);
						AppService.get().getEventBus().fireEvent(new TemplateRetrievedEvent(template));
					}else{
						AppController.get().getLoadingDialogBox().hide();
						Window.alert("Error occurred. Please try later or contact your administrator.");
					}
				}
			});
		} catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.getMessage());
		} catch(Exception e){
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.getMessage());
		}
		
	}
	
	private Template parseTemplate(String xml){
		try{
			Document customerDom = XMLParser.parse(xml);
			Element customerElement = customerDom.getDocumentElement();
			XMLParser.removeWhitespace(customerElement);
			Template template = new Template();
			template.setId(getElementIntValue(customerElement, "id"));
			template.setHtml(getElementTextValue(customerElement, "html").trim());
			return template;
		}catch(Exception e){
			Log.error(e.toString());
			return null;
		}
		
	}
	
	
	public void checkDomain(String subdomain){
		
		AppController.get().getLoadingDialogBox().show("Validating subdomain:" + subdomain);
		
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.DOMAIN_CHECK.toString());
		params.append("&subdomain="+subdomain);
		params.append("&webPageId="+AppService.get().getCurrentWebPage().getId());
		
		String htmlContextFileName = "server/domainService.php";
		String url = GWT.getModuleBaseURL() + htmlContextFileName;
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);
		builder.setHeader("Content-Type","application/x-www-form-urlencoded");
	    try {
	    	builder.sendRequest(params.toString(),new RequestCallback() {
	    		
	    		@Override
				public void onError(Request request, Throwable exception) {
					Window.alert("Error while checking domain");
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					
					Log.debug(response.getText());
					DomainServiceResponse domainServiceResponse = parseDomainServiceResponse(response.getText());
					
					AppController.get().getLoadingDialogBox().hide();
					
					if(domainServiceResponse.getType().equals(RESPONSE_TYPE.DOMAIN_CHECK_VALID)){
						
						AppService.get().getEventBus().fireEvent(new DomainEvent(DomainEventType.VALID));

					}else if(domainServiceResponse.getType().equals(RESPONSE_TYPE.DOMAIN_CHECK_INVALID)){
						
						AppService.get().getEventBus().fireEvent(new DomainEvent(DomainEventType.INVALID));
						
					}else if(domainServiceResponse.getType().equals(RESPONSE_TYPE.GENERIC_FAILURE)){
						Window.alert("Something wrong happened");
					}
					
					
				}
			});
		} catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
	    	Window.alert("Failed to send the request: " + e.getMessage());
		}

	}
	
	private DomainServiceResponse parseDomainServiceResponse(String xml){
		try{
			Document customerDom = XMLParser.parse(xml);
			Element customerElement = customerDom.getDocumentElement();
			XMLParser.removeWhitespace(customerElement);
			Element domainElement = (Element)customerElement.getElementsByTagName("object").item(0).getFirstChild();

			DomainServiceResponse webPageServiceresponse = new DomainServiceResponse();
			webPageServiceresponse.setType(RESPONSE_TYPE.valueOf(getElementTextValue(customerElement, "type")));
			webPageServiceresponse.setMessage(getElementTextValue(customerElement, "message"));
			return webPageServiceresponse;
			
		}catch (Exception e){
			Log.error(xml);
			e.printStackTrace();
		}
		DomainServiceResponse domainServiceResponse = new DomainServiceResponse();
		domainServiceResponse.setType(RESPONSE_TYPE.GENERIC_FAILURE);
		return domainServiceResponse;
	}
	
	private WebPageServiceResponse parseWebPageServiceResponse(String xml){
		try{
			Document customerDom = XMLParser.parse(xml);
			Element customerElement = customerDom.getDocumentElement();
			XMLParser.removeWhitespace(customerElement);
			Element webPage = (Element)customerElement.getElementsByTagName("object").item(0).getFirstChild();

			WebPageServiceResponse webPageServiceresponse = new WebPageServiceResponse();
			webPageServiceresponse.setType(RESPONSE_TYPE.valueOf(getElementTextValue(customerElement, "type")));
			webPageServiceresponse.setMessage(getElementTextValue(customerElement, "message"));
			webPageServiceresponse.setWebPage(parseLightWeightWebPageWidget(webPage));
			return webPageServiceresponse;
			
		}catch (Exception e){
			Log.error(xml);
			Log.error(e.toString());
			e.printStackTrace();
		}
		WebPageServiceResponse domainServiceResponse = new WebPageServiceResponse();
		domainServiceResponse.setType(RESPONSE_TYPE.GENERIC_FAILURE);
		return domainServiceResponse;
	}
	
	
	
	private WebPageWidget parseLightWeightWebPageWidget(Element webPageElement) throws Exception{
		WebPageWidget webPageWidget = new WebPageWidget();
		webPageWidget.setId(getElementIntValue(webPageElement, "id"));
		webPageWidget.setSubDomain(getElementTextValue(webPageElement, "subdomain"));
		return webPageWidget;
	}
	
	/*************************************************************************
	 * Web Page Loader Section
	 *************************************************************************/
	
	public void loadWebPage(User user, final int pageId ){
		
		AppController.get().getLoadingDialogBox().show( "Loading please wait..." );
		Log.info(AppProxy.class.getName()+ ".loadWebPage WebPage Id:" + pageId + " User:" + user.toString() );
		
		StringBuffer params = new StringBuffer();
		params.append("action="+ SERVICE_ACTION_TYPE.WEBPAGE_LOAD.toString());
		params.append("&userId="+user.getId());
		params.append("&webPageId="+pageId);

		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(params.toString(), new RequestCallback() {

				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Window.alert("Request Error. Please try again later.");
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					AppController.get().getLoadingDialogBox().hide();
					Log.debug(response.getText());
					WebPageWidget webPage = parseWebPageWidget(response.getText());
					if(webPage != null){
						AppService.get().setCurrentWebPage(webPage);
						AppService.get().setCurrentInnerPageId(0);
						Log.debug("Firing WebPageRetrieved..");
						AppService.get().getEventBus().fireEvent(new WebPageRetrievedEvent(webPage));
					}else{
						Window.alert("Failed to load your Web Page. Please try again.");
						AppController.get().getAppScreen().showAccountSettingsScreen();
					}
					
				}
			});
		} catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			e.printStackTrace();
		}
	}

	private WebPageWidget parseWebPageWidget(String xml){
		
		try{
			Document customerDom = XMLParser.parse(xml);
			Element webPageElement = customerDom.getDocumentElement();
			XMLParser.removeWhitespace(webPageElement);
			
			Template template = new Template();
			template.setId(getElementIntValue(webPageElement, AppUtils.WEBPAGE_XML_TAGS.TEMPLATE_ID.getTag()));
			
			Background background = new Background();
			background.setId(getElementIntValue(webPageElement, AppUtils.WEBPAGE_XML_TAGS.BACKGROUND_ID.getTag() ));
			
			Node logoWidgetNode = webPageElement.getElementsByTagName(AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET.getTag()).item(0);
			
			BannerWidget bannerWidget = new BannerWidget();
			bannerWidget.setBannerText(getElementTextValue((Element)logoWidgetNode, AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET_TEXT.getTag()));
			bannerWidget.setSlogan(getElementTextValue((Element)logoWidgetNode, AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET_SLOGAN.getTag()));
			
			
			NodeList innerpages = webPageElement.getElementsByTagName(WEBPAGE_XML_TAGS.INNER_PAGE.getTag());
			List<InnerPageWidget> innerPagesList = new ArrayList<InnerPageWidget>();
			for (int i=0; i < innerpages.getLength(); i++){
				Node innerPageNode = innerpages.item(i);
				if(innerPageNode instanceof Element){
					InnerPageWidget innerPage = new InnerPageWidget();
					innerPage.setName( getElementTextValue((Element)innerPageNode, WEBPAGE_XML_TAGS.INNER_PAGE_NAME.getTag()));
					innerPage.setMainContentWidgets(getWidgetList(((Element)innerPageNode).getElementsByTagName(WEBPAGE_XML_TAGS.MAIN_CONTENT.getTag()).item(0)));
					innerPage.setSideContentWidgets(getWidgetList(((Element)innerPageNode).getElementsByTagName(WEBPAGE_XML_TAGS.SIDE_CONTENT.getTag()).item(0)));
					innerPagesList.add(innerPage);
				}
			}

			WebPageWidget webpage = new WebPageWidget();
			webpage.setId(getElementIntValue(webPageElement, WEBPAGE_XML_TAGS.WEB_PAGE_ID.getTag()));
			webpage.setSubDomain(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.SUBDOMAIN.getTag()));
			webpage.setTitle(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.TITLE.getTag()));
			webpage.setType(WEBPAGE_TYPE.valueOf(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.TYPE.getTag())));
			webpage.setTypeDescription(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.TYPE_DESCRIPTION.getTag()));
			webpage.setDescription(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.DESCRIPTION.getTag()));
			webpage.setKeywords(getElementTextValue(webPageElement, WEBPAGE_XML_TAGS.KEYWORDS.getTag()));
			webpage.setTemplate(template);
			webpage.setBackground(background);
			webpage.setBannerWidget(bannerWidget);
			webpage.setInnerPageList(innerPagesList);

			return webpage;
		}catch(Exception e){
			Log.error(e.toString());
			return null;
		}
		
	}

	private List<DnDWidgetContainer> getWidgetList(Node containerNode) throws Exception{
		
		NodeList widgetNodes = ((Element)containerNode).getElementsByTagName(WEBPAGE_XML_TAGS.WIDGET.getTag());
		List<DnDWidgetContainer> widgetList = new ArrayList<DnDWidgetContainer>();
		for (int i=0; i < widgetNodes.getLength(); i++){
			Node widgetNode = widgetNodes.item(i);
			if(widgetNode instanceof Element){
				IWidget widget = null;
				String type = ((Element) widgetNode).getAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag());
				
				if(type.equals(WidgetType.TITLE.getName())){
					widget = new TitleWidget();
					((TitleWidget)widget).setTitle(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.TITLE_WIDGET_TITLE.getTag()));
				}else if(type.equals(WidgetType.TEXT.getName())){
					widget = new TextWidget();
					((TextWidget)widget).setText(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.TEXT_WIDGET_TEXT.getTag()));
				}else if(type.equals(WidgetType.IMAGE_TEXT.getName())){
					widget = new ImageTextWidget();
					((ImageTextWidget)widget).setImageURL(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_URL.getTag()));
					((ImageTextWidget)widget).setText(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_TEXT.getTag()));
					((ImageTextWidget)widget).setImageFloatingStyle(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_FLOAT.getTag()));
					((ImageTextWidget)widget).setImageWidth(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_WIDTH.getTag()));
				}else if(type.equals(WidgetType.VIDEO_TEXT.getName())){
					widget = new VideoTextWidget();
					((VideoTextWidget)widget).setVideo(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO.getTag()));
					((VideoTextWidget)widget).setText(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_TEXT.getTag()));
					((VideoTextWidget)widget).setFloatingStyle(getElementTextValue((Element)widgetNode, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO_FLOAT.getTag()));
					((VideoTextWidget)widget).setVideoWidth(getElementIntValue((Element)widgetNode, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO_WIDTH.getTag()));
				}
				widgetList.add(new DnDWidgetContainer(widget, null));
			}
		}
		
		return widgetList;
	}
	
	/***************************************************************
	 * Web Page create section
	 * This method will delete a webpage
	 **************************************************************/
	public void deleteWebPage(int webPageId){

		AppController.get().getLoadingDialogBox().show("Deleting your selected web page");
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.WEBPAGE_DELETE.toString());
		params.append("&userId="+AppService.get().getCurrentUser().getId());
		params.append("&webPageId="+webPageId);
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try{
			builder.sendRequest(params.toString(), new RequestCallback() {
				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.toString());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					AppController.get().getLoadingDialogBox().hide();
					Log.debug(response.getText());
					WebPageServiceResponse webPageServiceResponse = parseWebPageServiceResponse(response.getText());
					if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.WEBPAGE_DELETE_DELETED)){
						//AppService.get().getEventBus().fireEvent(new WebPageDeleteEvent(webPageServiceResponse.getWebPage()));
						deleteSubdomain(webPageServiceResponse.getWebPage());
					/*}else if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.GENERIC_FAILURE)){
						callOnGenericWebPageError();*/
					}else{
						callOnGenericWebPageError();
					}
				}
			});
			
		}catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.toString());
		}
	}
	
	private void callOnGenericWebPageError(){
		Window.alert("Error executing the request. Please try again later.");
		AppController.get().getAppScreen().showAccountSettingsScreen();
	}
	
	
	/**
	 * This will delete a subdomain, and it should only be called after we know for sure the webpage has been deleted.
	 * This one works, but it returns something really ugly.
	 */
	private void deleteSubdomain(final WebPageWidget webPage){

		AppController.get().getLoadingDialogBox().show("Deleting subdomain: " + webPage.getSubDomain() + ".booix.com");
		
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.WEBPAGE_DELETE_SUBDOMAIN.toString());
		params.append("&userId="+AppService.get().getCurrentUser().getId());
		params.append("&webPageId="+webPage.getId());
		params.append("&subdomain="+webPage.getSubDomain());
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try{
			builder.sendRequest(params.toString(), new RequestCallback() {
				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.toString());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					AppController.get().getLoadingDialogBox().hide();
					AppService.get().getEventBus().fireEvent(new WebPageDeleteEvent(webPage));
					//Initially we will ignore the response
					Log.debug(response.getText().trim());
				}
			});
		}catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.toString());
		}
	}
	
	
	/***************************************************************
	 * Web Page create section
	 **************************************************************/
	
	/**
	 * This will create a WebPage, for the current user.
	 * After several issues trying to have the creation of the subdomain withing the same process,
	 * we neede to come up with a workaround, which is calling this creation of the webpage first,
	 * once we know we were able to create it, create the subdomain i a separated process.
	 * 
	 * This separated process is createSubdomain()
	 * 
	 */
	public void createWebPage(String subdomain){

		AppController.get().getLoadingDialogBox().show("Creating Your Web Page..");
		
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.WEBPAGE_CREATE.toString());
		params.append("&userId="+AppService.get().getCurrentUser().getId());
		params.append("&subdomain="+subdomain);
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try{
			builder.sendRequest(params.toString(), new RequestCallback() {
				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.toString());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					AppController.get().getLoadingDialogBox().hide();
					Log.debug(response.getText());
					WebPageServiceResponse webPageServiceResponse = parseWebPageServiceResponse(response.getText());
					
					if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.WEBPAGE_STORE_STORED)){

						//AppService.get().getEventBus().fireEvent(new WebPageCreateEvent(webPageServiceResponse.getWebPage(),WebPageCreateType.SUCCESS));
						createSubdomain(webPageServiceResponse.getWebPage());

					}else if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE)){
						
						AppService.get().getEventBus().fireEvent(new WebPageCreateEvent(webPageServiceResponse.getWebPage(),WebPageCreateType.DOMAIN_NOT_AVAILABLE));

					}else if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.GENERIC_FAILURE)){
						Window.alert("Something wrong happened: " + response.getText());
					}
				}
			});
			
		}catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.toString());
		}
	}
	
	/**
	 * This will create a subdomain, and it should only be called after we know for sure the webpage has been created.
	 * This is a complement for the createwebpage(), we will send it in two different requests
	 * This one works, but it returns something really ugly, empty carriage return.
	 */
	private void createSubdomain(final WebPageWidget webPage){

		AppController.get().getLoadingDialogBox().show("Creating subdomain: " + webPage.getSubDomain() + ".booix.com");
		
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.WEBPAGE_CREATE_SUBDOMAIN.toString());
		params.append("&userId="+AppService.get().getCurrentUser().getId());
		params.append("&webPageId="+webPage.getId());
		params.append("&subdomain="+webPage.getSubDomain());
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try{
			builder.sendRequest(params.toString(), new RequestCallback() {
				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.toString());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					
					AppController.get().getLoadingDialogBox().hide();
					AppService.get().getEventBus().fireEvent(new WebPageCreateEvent(webPage,WebPageCreateType.SUCCESS));
					//Initially we will ignore the response
					Log.debug(response.getText().trim());
				}
			});
		}catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.toString());
		}
	}
	
	
	/***************************************************************
	 * Web Page publish/store section
	 **************************************************************/
	
	public void storeWebPage(final boolean displayPopup){

		AppController.get().getLoadingDialogBox().show("Storing "+ AppService.get().getCurrentWebPage().getSubDomain() +".booix.com web page");
		StringBuffer params = new StringBuffer();
		params.append("action="+SERVICE_ACTION_TYPE.WEBPAGE_STORE.toString());
		params.append("&userId="+AppService.get().getCurrentUser().getId());
		params.append("&webPageId="+AppService.get().getCurrentWebPage().getId());
		params.append("&subdomain="+AppService.get().getCurrentWebPage().getSubDomain());
		params.append("&webPageXML="+AppUtils.encodeURL(AppService.get().getCurrentWebPage().getXMLwithWebPageSource()));
		params.append("&webPageHTML="+AppUtils.encodeURL(AppService.get().getCurrentWebPage().getXMLWithInnerPagesHTML()));
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "server/webPageService.php");
		
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try{
			builder.sendRequest(params.toString(), new RequestCallback() {
				@Override
				public void onError(Request request, Throwable exception) {
					AppController.get().getLoadingDialogBox().hide();
					Log.error(exception.toString());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					
					Log.debug(response.getText());
					WebPageServiceResponse webPageServiceResponse = parseWebPageServiceResponse(response.getText());
					
					if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.WEBPAGE_STORE_STORED)){

						AppService.get().getCurrentUser().getWebPage(AppService.get().getCurrentWebPage().getId()).setSubDomain(AppService.get().getCurrentWebPage().getSubDomain());
						
					}else if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE)){
						
						AppService.get().getEventBus().fireEvent(new DomainEvent(DomainEventType.INVALID));

					}else if(webPageServiceResponse.getType().equals(RESPONSE_TYPE.GENERIC_FAILURE)){
						Window.alert("Failed to Store WebPage." + response.getText());
					}
					
					AppController.get().getLoadingDialogBox().hide();
					
					if(displayPopup){
						if(Window.confirm("Do you want to see")){
							Window.Location.replace(AppService.get().getCurrentWebPage().getWebPageURL());
						}
					}
					
				}
			});
			
		}catch (RequestException e) {
			AppController.get().getLoadingDialogBox().hide();
			Log.error(e.toString());
		}
	}

	/****************************************************************
	 * Utility methods												
	 ****************************************************************/
	
	/***
	 * @param parent
	 * @param elementTag
	 * @return String value for specified parent and elementTag
	 */
	private static String getElementTextValue(Element parent, String elementTag) throws Exception {
		
		try{
			return parent.getElementsByTagName(elementTag).item(0).getFirstChild().getNodeValue();
		}catch(Exception e){
			return "";
		}
		
	}

	/**
	 * 
	 * @param parent
	 * @param elementTag
	 * @return integer value for specified parent and elementTag
	 */
	private static int getElementIntValue(Element parent, String elementTag) {
		try{
			return Integer.parseInt(parent.getElementsByTagName(elementTag).item(0).getFirstChild().getNodeValue());
		}catch(Exception e){
			return 0 ;
		}
		
	}

}
