package com.booix.editor.client;

import com.booix.editor.client.AppUtils.USER_TYPE;
import com.booix.editor.client.model.User;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtfb.client.Callback;
import com.gwtfb.client.JSOModel;
import com.gwtfb.sdk.FBCore;
import com.gwtfb.sdk.FBEvent;
import com.gwtfb.sdk.FBXfbml;

public class AppFacebookAPI {
	
	private FBCore fbCore = GWT.create(FBCore.class);
	private FBEvent fbEvent = GWT.create(FBEvent.class);
	private FBXfbml fbXfbml = GWT.create(FBXfbml.class);
	
	private boolean status = true;
	private boolean xfbml = true;
	private boolean cookie = true;
	
	private static AppFacebookAPI singleton = null;
	
	public static AppFacebookAPI get(){
		if(singleton == null){
			singleton = new AppFacebookAPI();
		}
		return singleton;
	}
	
	//it seems that this is needed everytime we reload a screen
	public void init(){
		fbCore.init(AppUtils.FACEBOOK_API_ID, status, cookie, xfbml);
	}
	
	public FBXfbml getXFBML(){
		return fbXfbml;
	}

	private AppFacebookAPI() {
		// This is to check the initial state
		SessionChangeCallback sessionChangeCallback = new SessionChangeCallback ();
		fbEvent.subscribe("auth.sessionChange",sessionChangeCallback);
	}
	
	//This is a very important code.
	//Sine we are using faecbook as a login emchanism, this class is the one that will notify us when there is a change on the
	// session status
	class SessionChangeCallback extends Callback<JavaScriptObject> {
		public void onSuccess ( JavaScriptObject response ) {
		   // Make sure cookie is set so we can use the non async method
		   //Window.alert("SessionChange.onSuccess()");
		   //displayResponse(response);
		   handleSessionChange ();
		}
	}
	
	private void displayResponse(JavaScriptObject response){
		//User here is logged in and we are already getting his information
		JSOModel jso = response.cast();
		Window.alert( new JSONObject( response ).toString() );
	}
	
	
	// Callback used for checking login status checking login status
	class LoginStatusCallback extends Callback<JavaScriptObject> {
		public void onSuccess ( JavaScriptObject response ) {
			Window.alert("LoginStatus.onSuccess()");
			displayResponse(response);
		}
	}
	
	
	
	/**
	 * Asynchronously check the login status, that simple no magic here.
	 */
	public void checkLoginStatus(){
		// Get login status, after the user logs in or out
		LoginStatusCallback loginStatusCallback = new LoginStatusCallback ();
		fbCore.getLoginStatus( loginStatusCallback );
	}
	
	/**
	 * Asynchronously check the login status, that simple no magic here.
	 */
	public JavaScriptObject getSession(){
		return fbCore.getSession();
	}
	
	
	/**
	 * Use this method to call the login core method.
	 * 
	 * This will popup a login screen, and it is teh same as using the <fb:loginbutton />
	 */
	public void login(){
		
		fbCore.login(new AsyncCallback<JavaScriptObject>() {
			
			@Override
			public void onSuccess(JavaScriptObject result) {
				displayResponse(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	/**
	 * Use this method to call the login core method.
	 * 
	 * This will popup a login screen, and it is teh same as using the <fb:loginbutton />
	 */
	public void signout(){
		
		fbCore.logout(new AsyncCallback<JavaScriptObject>() {
			
			@Override
			public void onSuccess(JavaScriptObject result) {
				AppController.get().signOut();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	/**
	 * Render home view. If user is logged in display welcome message, otherwise
	 * display login dialog.
	 */
	private void handleSessionChange () {
		if ( fbCore.getSession() == null ) {
			AppController.get().signOut();
        } else {
        	//The user already has a session, so we will go ahead get the info and then login
        	getUserDataAndShowAppScreen();
        }
		fbXfbml.parse();
	}
	
	public void getUserDataAndShowAppScreen(){
		//We will call the user info api, and once we get the answer we will continue loading teh app
		fbCore.api ( "/me" , new MeAndShowAppScreenCallback () ); //get User info
	}
	
	/*
	 * asyncallback for requesting login data
	 * 
	 * {"id":"719932581", "name":"Sebastian Pereyro", "first_name":"Sebastian", 
	 * "last_name":"Pereyro", "link":"http://www.facebook.com/profile.php?id=719932581", 
	 * "birthday":"07/12/1980", "email":"sebapereyro@gmail.com", "timezone":-7, "locale":"en_US", 
	 * "verified":true, "updated_time":"2010-09-08T23:56:22+0000"}
	 */
	class MeAndShowAppScreenCallback extends Callback<JavaScriptObject> {
		public void onSuccess ( JavaScriptObject response ) {
			User user = new User(USER_TYPE.FACEBOOK);
			user.setFacebookData((JSOModel)response.cast());
			if(user.getEmail().equals("undefined")){
				AppFacebookAPI.get().signout();
				return;
			}
			AppService.get().login(user);
			/*if(AppService.get().getCurrentUser() == null){
				AppService.get().setCurrentUser(user);
				AppController.get().showAppScreen();
			}*/
		}
	}
	
	public FBCore getFbCore(){
		return fbCore;
	}
	

}
