package com.booix.editor.client.model.response;

import com.booix.editor.client.widgets.WebPageWidget;

public class WebPageServiceResponse extends AbstractServiceResponse{
	/**
	 * Light version only contains id, userid, and subdomain
	 */
	private WebPageWidget webPage = null;

	public WebPageWidget getWebPage() {
		return webPage;
	}

	public void setWebPage(WebPageWidget webPage) {
		this.webPage = webPage;
	}

}
