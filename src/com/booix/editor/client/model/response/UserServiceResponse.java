package com.booix.editor.client.model.response;

import com.booix.editor.client.model.User;

public class UserServiceResponse extends AbstractServiceResponse{
	private User user = null;
	
	public void setUser(User user){
		this.user = user;
	}
	
	public User getUser(){
		return user;
	}
}
