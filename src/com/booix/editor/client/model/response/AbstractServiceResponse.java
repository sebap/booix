package com.booix.editor.client.model.response;

import com.booix.editor.client.AppUtils.RESPONSE_TYPE;

public class AbstractServiceResponse {
	private RESPONSE_TYPE type = null;
	private String msg = null;
	
	public void setType(RESPONSE_TYPE type){
		this.type = type;
	}
	public RESPONSE_TYPE getType(){
		return type;
	}
	
	public void setMessage(String msg){
		this.msg = msg;
	}
	public String getMessage(){
		return msg;
	}

}
