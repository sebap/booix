package com.booix.editor.client.model.response;



public class DomainServiceResponse extends AbstractServiceResponse{
	
	private String subdomain = null;

	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

}
