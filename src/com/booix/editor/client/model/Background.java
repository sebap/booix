package com.booix.editor.client.model;

public class Background {
	
	private int id = 0;
	private String filename = null; //usually backgroundN where N is 1 to N.
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	

}
