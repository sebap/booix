package com.booix.editor.client.model;

public class Template {

	private int id = 0;
	private String name = null;
	private String description = null;
	private String html = null;

	private boolean supportBanner = false;
	private int bannerWidth = 0;
	private int bannerHeigth = 0;
	
	private boolean hasLeftContainer = false;
	private boolean hasMainContainer = false;
	private boolean hasRightContainer = false;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

}
