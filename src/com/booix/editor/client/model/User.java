package com.booix.editor.client.model;

import java.util.List;

import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppUtils.ACCOUNT_TYPE;
import com.booix.editor.client.AppUtils.USER_TYPE;
import com.booix.editor.client.utils.MD5;
import com.booix.editor.client.widgets.WebPageWidget;
import com.google.gwt.http.client.URL;
import com.gwtfb.client.JSOModel;


public class User {
	//{"id":"719932581", "name":"Sebastian Pereyro", "first_name":"Sebastian", "last_name":"Pereyro", "link":"http://www.facebook.com/profile.php?id=719932581", "birthday":"07/12/1980", "email":"sebapereyro@gmail.com", "timezone":-7, "locale":"en_US", "verified":true, "updated_time":"2010-09-08T23:56:22+0000"}
	
	private int id = 0;
	private String facebookUID = null;
	private String firstName = null;
	private String lastName = null;
	private String name = null;
	private String facebookProfileLink = null;
	private String birthday = null;
	private String email = null;
	private String password = null;
	private USER_TYPE user_type = USER_TYPE.FACEBOOK;
	private ACCOUNT_TYPE account_type = ACCOUNT_TYPE.FREE;
	private List<WebPageWidget> webPageList = null;
	
	public User(USER_TYPE userType){
		user_type = userType;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFacebookUID() {
		return facebookUID;
	}

	public void setFacebookUID(String facebookUID) {
		this.facebookUID = facebookUID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFacebookProfileLink() {
		return facebookProfileLink;
	}

	public void setFacebookProfileLink(String facebookProfileLink) {
		this.facebookProfileLink = facebookProfileLink;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<WebPageWidget> getWebPageList() {
		return webPageList;
	}

	public void setWebPageList(List<WebPageWidget> list) {
		this.webPageList = list;
	}

	public USER_TYPE getUserType() {
		return user_type;
	}

	public void setUserType(USER_TYPE userType) {
		user_type = userType;
	}

	public ACCOUNT_TYPE getAccountType() {
		return account_type;
	}

	public void setAccountType(ACCOUNT_TYPE accountType) {
		account_type = accountType;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password, boolean encode) {
		if(encode){
			this.password = new MD5().calcMD5(password);
		}else{
			this.password = password;
		}
	}
	
	public void setFacebookData(JSOModel facebookData){
		//id = 0; // we do not know the booix id yet
		facebookUID = facebookData.get("id");
		firstName = facebookData.get("first_name");
		lastName = facebookData.get("last_name");
		name = facebookData.get("name");
		facebookProfileLink = facebookData.get("link");
		birthday = facebookData.get("birthday");
		email = facebookData.get("email");
	}

	public String toString(){
		StringBuffer string = new StringBuffer();
		//string.append("<?xml version='1.0' encoding='UTF-8' ?>\n"); 
		string.append("<user>\n");
		string.append("<id>"+id+"</id>\n");
		string.append("<firstName>"+firstName+"</firstName>\n");
		string.append("<lastName>"+lastName+"</lastName>\n");
		string.append("<email>"+email+"</email>\n");
		string.append("</user>");
		return string.toString();
	}
	
	public String generatePOSTdata(){
		StringBuffer sb = new StringBuffer();
		String encodedName = URL.encodeQueryString("userType"); sb.append(encodedName); sb.append("=");
		String encodedValue = URL.encodeQueryString(getUserType().toString()); sb.append(encodedValue);
		sb.append("&");
		encodedName = URL.encodeQueryString("email"); sb.append(encodedName); sb.append("=");
		encodedValue = URL.encodeQueryString(getEmail()); sb.append(encodedValue);
		
		if(getUserType() == USER_TYPE.BOOIX){
			sb.append("&");
			encodedName = URL.encodeQueryString("password");	sb.append(encodedName); sb.append("=");
			encodedValue = URL.encodeQueryString(getPassword()); sb.append(encodedValue);
		}

		Log.info("User POST Data: " + sb.toString());
		return sb.toString();
	}
	
	
	public WebPageWidget getWebPage(int webPageId){
		for (WebPageWidget currentWebPage: webPageList ){
			if(currentWebPage.getId() == webPageId){
				return currentWebPage;
			}
		}
		return null;
	}
	
	public void removeWebPage(int webPageId){
		for (WebPageWidget currentWebPage: webPageList ){
			if(currentWebPage.getId() == webPageId){
				webPageList.remove(currentWebPage);
				break;
			}
		}
	}
}
