package com.booix.editor.client;

import com.google.gwt.i18n.client.Messages;

public interface AppMessages extends Messages {
	String getTemplates();
	String getComponents();
	String getBackgrounds();
	String getPreview();
	String getPublish();
	String getSettings();
	String getLogin();
	String getLogout();

	//Widgets
	String getMenuWidgetDisplayText();
	String getBannerWidgetDisplayText();
	String getTitleWidgetDisplayText();
	String getTextWidgetDisplayText();
	String getImageTextWidgetDisplayText();
	String getVideoTextWidgetDisplayText();
	
	//TitleWidget
	String getTitleWidgetTitlePlaceholder();
	
	//TextWidget
	String getTextWidgetTextPlaceholder();
	
	//VideoTextWidget
	String getVideoTextWidgetTextPlaceholder();
	
	//ImageTextWidget
	String getImageTextWidgetTextPlaceholder();
	
	String getLoading();
	
	String getSave();
	String getRefresh();
	String getCancel();
	String getEdit();
	String getDoneEditing();
	String getRemoveConfirmation();
	
	String getDropWidgetCSSName();

	
}
