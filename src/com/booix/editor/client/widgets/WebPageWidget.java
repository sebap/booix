package com.booix.editor.client.widgets;

import java.util.List;

import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WEBPAGE_TYPE;
import com.booix.editor.client.model.Background;
import com.booix.editor.client.model.Template;
import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.XMLParser;


/**
 * This is the page that is being modified.
 * @author Owner
 *
 */
public class WebPageWidget {
	
	private int id = 0;
	private String title = "";
	private String subdomain = ""; //without the booix.com
	private WEBPAGE_TYPE type = WEBPAGE_TYPE.PERSONAL;
	private String typeDescription = "";
	private String description = "";
	private String keywords = "";
	private Template template = null;
	private Background background = null;
	private BannerWidget bannerWidget = new BannerWidget();
	private List<InnerPageWidget> innerPageWidgetList = null;
	
	public WebPageWidget() { }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubDomain() {
		return subdomain;
	}
	public void setSubDomain(String domain) {
		this.subdomain = domain;
	}
	
	public WEBPAGE_TYPE getType() {
		return type;
	}

	public void setType(WEBPAGE_TYPE type) {
		this.type = type;
	}

	public String getTypeDescription() {
		return typeDescription;
	}

	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	public Background getBackground() {
		return background;
	}
	public void setBackground(Background background) {
		this.background = background;
	}

	public List<InnerPageWidget> getInnerPageList(){
		return innerPageWidgetList;
	}
	
	public void setInnerPageList(List<InnerPageWidget> innerPages) {
		this.innerPageWidgetList = innerPages;
	}
	
	public BannerWidget getBannerWidget() {
		return bannerWidget;
	}

	public void setBannerWidget(BannerWidget bannerWidget) {
		this.bannerWidget = bannerWidget;
	}
	
	
	
	/**
	 * Ths method will return an xml string containing the html for each inner page in the following format
	 * <webpage>
	 * 		<innerPage>
	 * 			<name>home</name>
	 * 			<html>CDATA[HTML content here]</html>
	 * 		</innerPage>
	 * 	 	<innerPage>
	 * 			<name>about_us</name>
	 * 			<html>CDATA[HTML content here]</html>
	 * 		</innerPage>
	 * </webpage>
	 * @return
	 */
	public String getXMLWithInnerPagesHTML(){
		Document doc= XMLParser.createDocument();
		Element webPage = doc.createElement(AppUtils.WEBPAGE_XML_TAGS.WEB_PAGE.getTag());
		doc.appendChild(webPage);
		
		for (InnerPageWidget current : innerPageWidgetList) {
			webPage.appendChild(current.getInnerPageHTML(doc));
		}
		
		String xmlStr = AppUtils.XML_FILE_DOC_TYPE + doc.toString(); 
		//Window.alert(xmlStr) ;
		return xmlStr;
	}
	
	/**
	 * This method will generate an XML with all the information to recreate the webpage for the app.
	 * This is mainly the source of each webpage.
	 * @return
	 */
	
	public String getXMLwithWebPageSource(){
		Document xml= XMLParser.createDocument();
		xml = XMLParser.createDocument();
		Element webPage= xml.createElement(AppUtils.WEBPAGE_XML_TAGS.WEB_PAGE.getTag());
		xml.appendChild(webPage);
		
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.WEB_PAGE_ID.getTag(), Integer.toString(getId()));
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.TITLE.getTag(), getTitle());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.SUBDOMAIN.getTag(), getSubDomain());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.TYPE.getTag(), getType().toString());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.TYPE_DESCRIPTION.getTag(), getTypeDescription());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.DESCRIPTION.getTag(), getDescription());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.KEYWORDS.getTag(), getKeywords());
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.TEMPLATE_ID.getTag(), Integer.toString(getTemplate().getId()));
		AppUtils.appendNewChildAndValue(xml, webPage, AppUtils.WEBPAGE_XML_TAGS.BACKGROUND_ID.getTag(), Integer.toString(getBackground().getId()));

		Element headerWidget = xml.createElement(AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET.getTag());
		webPage.appendChild(headerWidget);
		AppUtils.appendNewChildAndValue(xml, headerWidget, AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET_TEXT.getTag(), getBannerWidget().getLogoText());
		AppUtils.appendNewChildAndValue(xml, headerWidget, AppUtils.WEBPAGE_XML_TAGS.BANNER_WIDGET_SLOGAN.getTag(), getBannerWidget().getSlogan());
		
		
		for (InnerPageWidget current : innerPageWidgetList) {
			webPage.appendChild(current.getInnerPageXML(xml));
		}
		
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + xml.toString();
		
		return xmlString;
	}
	
	public String getWebPageURL(){
		return GWT.getModuleBaseURL() + "../../webpages/user" + AppService.get().getCurrentUser().getId() + "/page" + AppService.get().getCurrentWebPage().getId() + "/" + AppService.get().getCurrentWebPage().getInnerPageList().get(0).getNameForLink(); 
	}
	
	
	
}
