package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.booix.editor.client.screens.toolbar.RichTextToolbar;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

public class TextWidget extends Composite implements IWidget{

	private VerticalPanel mainPanel = new VerticalPanel();
	private RichTextArea area = new RichTextArea();
	private RichTextToolbar toolbar = new RichTextToolbar(area);
	private String text = AppUtils.TEXT_WIDGET_DEFAULT_TEXT;
	private HTML widgetHTMLContent = new HTML( text );
	private WidgetType type = WidgetType.TEXT;

	public TextWidget() {
		
		initWidget(mainPanel);
		addStyleName(type.getCSSName());
		this.setWidth("100%");
		
		widgetHTMLContent.addStyleName(type.getCSSName());
		widgetHTMLContent.getElement().setId(type.getName() + AppUtils.TEXT_WIDGET_NEXT_ID);
		
		area.setSize("100%", "100%");
		area.setHTML(widgetHTMLContent.getHTML());
		area.getElement().getStyle().setBackgroundColor("white");
		
		toolbar.setWidth("100%");
		
		setToolBarButtonsVisibility();

		mainPanel.add(toolbar);
		mainPanel.add(area);
		mainPanel.add(widgetHTMLContent);
		mainPanel.setWidth("100%");

		setText(text);
		
		endEditing();
		
		AppUtils.TEXT_WIDGET_NEXT_ID++;
	}
	
	private void setToolBarButtonsVisibility(){
		toolbar.getBold().setVisible(true);
		toolbar.getItalic().setVisible(true);
		toolbar.getUnderline().setVisible(true);
		toolbar.getSubscript().setVisible(false);
		toolbar.getSuperscript().setVisible(false);
		toolbar.getStrikethrough().setVisible(true);
		toolbar.getOutdent().setVisible(true);
		toolbar.getJustifyLeft().setVisible(true);
		toolbar.getJustifyCenter().setVisible(true);
		toolbar.getJustifyRight().setVisible(true);
		toolbar.getHr().setVisible(true);
		toolbar.getOl().setVisible(true);
		toolbar.getUl().setVisible(true);
		toolbar.getInsertImage().setVisible(false);
		toolbar.getCreateLink().setVisible(true);
		toolbar.getRemoveLink().setVisible(true);
		toolbar.getRemoveFormat().setVisible(true);
		toolbar.getBackColors().setVisible(true);
		toolbar.getForeColors().setVisible(true);
		toolbar.getFonts().setVisible(true);
		toolbar.getFontSizes().setVisible(true);
	}

	public void setText(String text) {
		this.text = text;
		widgetHTMLContent.setHTML(text);
	}
	
	@Override
	public void endEditing() {
		widgetHTMLContent.setVisible(true);
		toolbar.setVisible(false);
		area.setVisible(false);
		setText(area.getHTML());
	}

	@Override
	public void startEditing() {
		widgetHTMLContent.setVisible(false);
		toolbar.setVisible(true);
		area.setVisible(true);
		area.setHTML(widgetHTMLContent.getHTML());
	}

	@Override
	public HasClickHandlers getWidgetToInitEdition() {
		// TODO Auto-generated method stub
		return widgetHTMLContent;
	}

	@Override
	public WidgetType getWidgetType() {
		// TODO Auto-generated method stub
		return type;
	}

	@Override
	public IWidget getCopy() {
		TextWidget w = new TextWidget();
		w.setText(text);
		return w;
	}

	@Override
	public String getWidgetHTML() {
		return widgetHTMLContent.toString();
	}

	@Override
	public Element getWidgetXML(Document xml) {
		com.google.gwt.xml.client.Element widget = xml.createElement(WEBPAGE_XML_TAGS.WIDGET.getTag());
		widget.setAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag(), type.getName());
		AppUtils.appendNewChildAndCDATAValue(xml, widget, WEBPAGE_XML_TAGS.TEXT_WIDGET_TEXT.getTag(), widgetHTMLContent.getHTML());
		return widget;
	}

}
