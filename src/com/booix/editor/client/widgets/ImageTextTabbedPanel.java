package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils;
import com.booix.editor.client.screens.toolbar.RichTextToolbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

@SuppressWarnings("deprecation")
public class ImageTextTabbedPanel extends Composite {

	private TabPanel tabbedPanel = new TabPanel();
	
	private VerticalPanel textTabPanel = new VerticalPanel();
	private RichTextArea textArea = new RichTextArea();;
	private RichTextToolbar textToolbar = new RichTextToolbar(textArea);
	
	
	//Image Tab Components
	private VerticalPanel imageTabPanel = new VerticalPanel();
	private PushButton increaseButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.increaseIcon());
	private PushButton decreaseButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.decreaseIcon());
	private PushButton leftButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.imageMoveLeftIcon());
	private PushButton centerButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.imageCenterIcon());
	private PushButton rightButton =AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.imageMoveRightIcon());
	private TextBox imgUrlTxt = new TextBox();
	private PushButton uploadButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.imageUploadIcon());

	//reference to the widget
	private ImageTextWidget widget = null;
	
	@UiConstructor
	public ImageTextTabbedPanel(ImageTextWidget widget) {
		initWidget(tabbedPanel);
		this.widget = widget;
		this.setWidth("100%");
		
		configureTextTabPanel();
		configureImageTabPanel();
		buildListeners();
		
		tabbedPanel.add(textTabPanel, "Text");
		tabbedPanel.add(imageTabPanel, "Image");
		
		tabbedPanel.getElement().getStyle().setBackgroundColor("white");
	}
	
	
	
	
	private void configureTextTabPanel(){

		textArea.setWidth("100%");
		textArea.getElement().getStyle().setBackgroundColor("white");
		textArea.setHTML(widget.getText().getHTML());
		
		textToolbar.setWidth("100%");
		setToolBarButtonsVisibility();
		textTabPanel.add(textToolbar);
		textTabPanel.add(textArea);
		textTabPanel.setWidth("100%");
	}
	
	private void configureImageTabPanel(){
		imageTabPanel.setWidth("100%");
		HorizontalFlowPanel hpanel = new HorizontalFlowPanel();
		hpanel.setWidth("100%");
		hpanel.add(increaseButton);
		hpanel.add(decreaseButton);
		hpanel.add(leftButton);
		hpanel.add(centerButton);
		hpanel.add(rightButton);
		hpanel.add(imgUrlTxt);
		hpanel.add(uploadButton);
		
		imgUrlTxt.setStyleName("floatLeft");
		
		imageTabPanel.add(hpanel);

	}
	
	private void setToolBarButtonsVisibility(){
		textToolbar.getBold().setVisible(true);
		textToolbar.getItalic().setVisible(true);
		textToolbar.getUnderline().setVisible(true);
		textToolbar.getSubscript().setVisible(false);
		textToolbar.getSuperscript().setVisible(false);
		textToolbar.getStrikethrough().setVisible(true);
		textToolbar.getOutdent().setVisible(true);
		textToolbar.getJustifyLeft().setVisible(true);
		textToolbar.getJustifyCenter().setVisible(true);
		textToolbar.getJustifyRight().setVisible(true);
		textToolbar.getHr().setVisible(true);
		textToolbar.getOl().setVisible(true);
		textToolbar.getUl().setVisible(true);
		textToolbar.getInsertImage().setVisible(false);
		textToolbar.getCreateLink().setVisible(true);
		textToolbar.getRemoveLink().setVisible(true);
		textToolbar.getRemoveFormat().setVisible(true);
		textToolbar.getBackColors().setVisible(true);
		textToolbar.getForeColors().setVisible(false);
		textToolbar.getFonts().setVisible(false);
		textToolbar.getFontSizes().setVisible(true);
	}
	
	private void buildListeners(){
		increaseButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				widget.setImageWidth( (widget.getImage().getWidth() + AppUtils.INCREASE_DECREASE_FACTOR) +"px");
			}
		});
		
		decreaseButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int width = widget.getImage().getWidth();
				width = width - AppUtils.INCREASE_DECREASE_FACTOR;
				if(width > 0){
					widget.setImageWidth(width+"px");
				}
			}
		});
		
		leftButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				widget.setImageFloatingStyle("floatLeft");
			}
		});
		
		centerButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				widget.setImageFloatingStyle("floatCenter");
			}
		});
		
		rightButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				widget.setImageFloatingStyle("floatRight");
			}
		});
		
		
		uploadButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(!imgUrlTxt.getText().equals("")){
					widget.setImageURL(imgUrlTxt.getText());
				}
			}
		});
		
		textArea.addKeyUpHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				widget.setText(textArea.getHTML());
			}
		});
	}
	
	public void startEditing(){
		tabbedPanel.selectTab(0);
		textArea.setHTML(widget.getText().getHTML());
	}
	
	public void endEditing(){
		widget.setText(textArea.getHTML());
	}

}
