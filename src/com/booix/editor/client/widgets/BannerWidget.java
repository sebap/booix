package com.booix.editor.client.widgets;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.HTML_SUPPORTED_DIV_ID;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;

public class BannerWidget extends Composite implements IWidget {
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private HTML widgetHTMLContent = new HTML();
	private String bannerText = "Logo";
	private String slogan = "Slogan";
	private WidgetType type = WidgetType.BANNER;
	private PushButton editB = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.editIcon());
	
	public BannerWidget() {
		
		initWidget(mainPanel);
		addStyleName( type.getCSSName() );
		setBannerText(bannerText);
		setSlogan(slogan);
		
		mainPanel.add(widgetHTMLContent);
		mainPanel.add(editB) ;
		mainPanel.setWidth("100%");
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		
		widgetHTMLContent.addStyleName( type.getCSSName() );
		widgetHTMLContent.getElement().setId(HTML_SUPPORTED_DIV_ID.BANNER.getId());

		editB.setVisible(false);
		//editB.getElement().getStyle().setMarginTop(-20, Unit.PX);
		editB.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				showEditor();
			}
		});
		
    	sinkEvents(Event.ONCLICK);
		sinkEvents(Event.ONMOUSEOVER);
		sinkEvents(Event.ONMOUSEOUT);
		
		endEditing(); //call this to handle the visibility;
	}
	
	public void showEditor(){
		AppController.get().getAppScreen().getWebPageEditor().showBannerEditor();
		removeStyleName( type.getCSSName() + "-MouseOver");
    	editB.setVisible(false);
	}
	
	public void setBannerText(String bannerText){
		this.bannerText = bannerText;
		widgetHTMLContent.setHTML("<h1><a href=\"INDEX_HERE\">"+this.bannerText+"</a></h1><h2><a href=\"INDEX_HERE\">"+this.slogan+"</a></h2>");
	}
	
	public String getLogoText(){
		return bannerText;
	}
	
	public void setSlogan(String slogan){
		this.slogan = slogan;
		widgetHTMLContent.setHTML("<h1><a href=\"INDEX_HERE\">"+this.bannerText+"</a></h1><h2><a href=\"INDEX_HERE\">"+this.slogan+"</a></h2>");
	}
	
	public String getSlogan(){
		return slogan;
	}
	
	@Override
	public void endEditing() {
		bannerText = bannerText.replaceAll("\\<.*?\\>", "");
		setTitle(bannerText);
		widgetHTMLContent.setVisible(true);
	}

	@Override
	public void startEditing() {
		widgetHTMLContent.setVisible(false);
		bannerText = widgetHTMLContent.getHTML().replaceAll("\\<.*?\\>", "");
	}

	@Override
	public HasClickHandlers getWidgetToInitEdition() {
		return widgetHTMLContent;
	}

	@Override
	public WidgetType getWidgetType() {
		return type;
	}

	@Override
	public IWidget getCopy() {
		BannerWidget w = new BannerWidget();
		w.setBannerText(bannerText);
		w.setSlogan(slogan);
		return w;
	}
	
	@Override
	public void onBrowserEvent(Event event) {
		Element el = DOM.eventGetTarget(event);
        switch(DOM.eventGetType(event)) {
                case Event.ONCLICK:
                	showEditor();
                	DOM.eventPreventDefault(event) ;
                	break;
                case Event.ONMOUSEOVER:
                	setStyleName( type.getCSSName() + "-MouseOver" );
                	editB.setVisible(true);
                	break;
                case Event.ONMOUSEOUT:
                	removeStyleName(type.getCSSName() + "-MouseOver");
                	editB.setVisible(false);
                	break;
                default:
                	break;
        }
    }

	@Override
	public String getWidgetHTML() {
		return widgetHTMLContent.getElement().getInnerHTML().replaceAll("INDEX_HERE", AppService.get().getCurrentWebPage().getInnerPageList().get(0).getNameForLink());
	}

	@Override
	public com.google.gwt.xml.client.Element getWidgetXML(Document xml) {
		com.google.gwt.xml.client.Element widget = xml.createElement(WEBPAGE_XML_TAGS.WIDGET.getTag());
		widget.setAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag(), type.getName());
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.BANNER_WIDGET_TEXT.getTag(), bannerText);
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.BANNER_WIDGET_SLOGAN.getTag(), slogan);
		return widget;
	}

}
