package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.booix.editor.client.screens.toolbar.RichTextToolbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

@SuppressWarnings("deprecation")
public class VideoTextWidget extends Composite implements IWidget {
	
	private WidgetType type 				= WidgetType.VIDEO_TEXT;
	
	//Widget Data
	private String currentVideo 				= AppUtils.VIDEO_TEXT_WIDGET_DEFAULT_VIDEO;
	private int currentVideoWidth 				= AppUtils.VIDEO_TEXT_WIDGET_DEFAULT_VIDEO_WIDTH_INT;
	private String currentVideoFloatingStyle 	= AppUtils.VIDEO_TEXT_WIDGET_DEFAULT_FLOATING_STYLE;
	
	//Widget Layout components
	private VerticalPanel mainPanel 		= new VerticalPanel();
	private HTMLPanel widgetHTMLContent 	= new HTMLPanel("<div id=\""+type.getName()+"Video"+AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID+"\"></div><div id=\""+type.getName()+"Text"+AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID+"\"></div>");
	private HTML videoContainer 			= new HTML();
	private HTML textContainer 				= new HTML(AppUtils.VIDEO_TEXT_WIDGET_DEFAULT_TEXT );
	private VideoTextEditPanel tabbedPanel 	= new VideoTextEditPanel();

	public VideoTextWidget() {
		initWidget(mainPanel);
		addStyleName(type.getCSSName()); //this is the style for the edition mode /it will adjust the edition panels
		
		widgetHTMLContent.getElement().setId( type.getName() + AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID);
		widgetHTMLContent.addStyleName(type.getCSSName()); //this is the style i need to use when the webpage is created.
		widgetHTMLContent.addAndReplaceElement(videoContainer, type.getName() + "Video" + AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID);
		widgetHTMLContent.addAndReplaceElement(textContainer, type.getName() + "Text" + AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID);
		
		videoContainer.addStyleName(currentVideoFloatingStyle);
		videoContainer.addStyleName( type.getCSSName() + "Video");
		videoContainer.getElement().getStyle().setProperty("display", "block");
		videoContainer.getElement().setId( type.getName() + "Video" + AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID);
		setVideo(currentVideo);

		textContainer.getElement().setId( type.getName() + "Text" + AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID);

		mainPanel.add(tabbedPanel);
		mainPanel.add(widgetHTMLContent);

		tabbedPanel.getElement().getStyle().setBackgroundColor("white");
		
		tabbedPanel.setVisible(false);
		
		AppUtils.VIDEO_TEXT_WIDGET_NEXT_ID++; //increment for next video
	}
	
	public void setText(String text){
		this.textContainer.setHTML(text);
	}
	
	public void setVideo(String video){
		currentVideo = video;
		this.videoContainer.setHTML( "<object width=\""+currentVideoWidth+"\" height=\""+currentVideoWidth+"\" ><param name=\"wmode\" value=\"transparent\" /><param name=\"movie\" value=\"http://www.youtube.com/v/"+ currentVideo +"\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/"+currentVideo+"\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\""+ currentVideoWidth + "\" height=\""+currentVideoWidth+"\" wmode=\"transparent\"></embed></object>" );
	}
	
	public void setVideoWidth(int width){
		currentVideoWidth = width;
		this.videoContainer.setHTML( "<object width=\""+currentVideoWidth+"\" height=\""+currentVideoWidth+"\" ><param name=\"wmode\" value=\"transparent\" /><param name=\"movie\" value=\"http://www.youtube.com/v/"+ currentVideo +"\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/"+currentVideo+"\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\""+ currentVideoWidth + "\" height=\""+currentVideoWidth+"\" wmode=\"transparent\"></embed></object>" );
	}
	
	public void setFloatingStyle(String floatingStyle){
		
		if(!floatingStyle.equals("")){
			currentVideoFloatingStyle = floatingStyle;
		}
		
		videoContainer.removeStyleName("floatRight");
		videoContainer.removeStyleName("floatCenter");
		videoContainer.removeStyleName("floatLeft");
		videoContainer.addStyleName(currentVideoFloatingStyle);
	}

	@Override
	public void endEditing() {
		tabbedPanel.setVisible(false);
		tabbedPanel.endEditing();
	}

	@Override
	public void startEditing() {
		tabbedPanel.setVisible(true);
		tabbedPanel.startEditing();
	}

	@Override
	public HasClickHandlers getWidgetToInitEdition() {
		// TODO Auto-generated method stub
		return textContainer;
	}

	@Override
	public WidgetType getWidgetType() {
		return type;
	}
	
	
	/**
	 * This class contains the edition panel
	 * @author Owner
	 *
	 */
	private class VideoTextEditPanel extends Composite{
		
		private TabPanel mainPanel = new TabPanel();
		
		private VerticalPanel textTabPanel = new VerticalPanel();
		private RichTextArea area = new RichTextArea();
		private RichTextToolbar toolbar = new RichTextToolbar(area);
		
		private VerticalPanel videoTabPanel = new VerticalPanel();
		private HorizontalFlowPanel videoTabHorizontalPanel = new HorizontalFlowPanel();
		private PushButton increaseButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.increaseIcon());
		private PushButton decreaseButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.decreaseIcon());
		private PushButton leftButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.videoMoveLeftIcon());
		private PushButton centerButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.videoCenterIcon());
		private PushButton rightButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.videoMoveRightIcon());
		private TextBox videoURLTxt = new TextBox();
		private PushButton uploadButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.videoUploadIcon());
		
		
		public VideoTextEditPanel(){
			initWidget(mainPanel);
			
			createTextTabPanel();
			createVideoTabPanel();
			
			mainPanel.add(textTabPanel, "Text");
			mainPanel.add(videoTabPanel, "Video");
			mainPanel.getElement().getStyle().setBackgroundColor("white");
			mainPanel.selectTab(0);
			mainPanel.setWidth("100%");

			buildListeners();
		}
		
		private void createTextTabPanel(){
			toolbar.setWidth("100%");
			setToolBarButtonsVisibility();
			
			area.setWidth("100%");

			textTabPanel.add(toolbar);
			textTabPanel.add(area);
			textTabPanel.setWidth("100%");
			textTabPanel.getElement().getStyle().setBackgroundColor("white");
		}
		
		private void createVideoTabPanel(){
			videoTabPanel.getElement().getStyle().setBackgroundColor("white");

			HorizontalPanel hpanel = new HorizontalPanel();
			hpanel.add(videoURLTxt);
			hpanel.add(uploadButton);
			hpanel.addStyleName("floatLeft");
			videoURLTxt.setWidth("100px");
			
			videoTabHorizontalPanel.add(increaseButton);
			videoTabHorizontalPanel.add(decreaseButton);
			videoTabHorizontalPanel.add(leftButton);
			videoTabHorizontalPanel.add(centerButton);
			videoTabHorizontalPanel.add(rightButton);
			videoTabHorizontalPanel.add(hpanel);
			videoTabHorizontalPanel.setWidth("100%");
			
			videoTabPanel.add(videoTabHorizontalPanel);
			videoTabPanel.setWidth("100%");
		}
		
		private void setToolBarButtonsVisibility(){
			toolbar.getBold().setVisible(true);
			toolbar.getItalic().setVisible(true);
			toolbar.getUnderline().setVisible(true);
			toolbar.getSubscript().setVisible(false);
			toolbar.getSuperscript().setVisible(false);
			toolbar.getStrikethrough().setVisible(true);
			toolbar.getOutdent().setVisible(true);
			toolbar.getJustifyLeft().setVisible(true);
			toolbar.getJustifyCenter().setVisible(true);
			toolbar.getJustifyRight().setVisible(true);
			toolbar.getHr().setVisible(true);
			toolbar.getOl().setVisible(true);
			toolbar.getUl().setVisible(true);
			toolbar.getInsertImage().setVisible(false);
			toolbar.getCreateLink().setVisible(true);
			toolbar.getRemoveLink().setVisible(true);
			toolbar.getRemoveFormat().setVisible(true);
			toolbar.getBackColors().setVisible(true);
			toolbar.getForeColors().setVisible(false);
			toolbar.getFonts().setVisible(false);
			toolbar.getFontSizes().setVisible(true);
		}
		
		private void buildListeners(){
			increaseButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setVideoWidth(currentVideoWidth + AppUtils.INCREASE_DECREASE_FACTOR);
				}
			});
			
			decreaseButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setVideoWidth(currentVideoWidth - AppUtils.INCREASE_DECREASE_FACTOR);
				}
			});
			
			leftButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setFloatingStyle("floatLeft");
				}
			});
			
			centerButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setFloatingStyle("floatCenter");
				}
			});
			
			rightButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setFloatingStyle("floatRight");
				}
			});

			uploadButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if(!videoURLTxt.getText().equals("")){

						try{
							String [] pair = videoURLTxt.getText().substring(1, videoURLTxt.getText().length()).split("\\?")[1].split("&");
							for (String string : pair) {
								String param = string.split("=")[0];
								String value = string.split("=")[1];
								if(param.equals("v")){
									setVideo(value);
									break;
								}
							}	
						}catch (Exception e) {
							//try text 
							setVideo(videoURLTxt.getText());
						}
						
					}
				}
			});

			area.addKeyUpHandler(new KeyUpHandler() {
				@Override
				public void onKeyUp(KeyUpEvent event) {
					textContainer.setHTML(area.getHTML());
					
				}
			});
		}
		
		public void startEditing(){
			area.setHTML(textContainer.getHTML());
		}
		
		public void endEditing(){
			textContainer.setHTML(area.getHTML());
		}

	}
	
	@Override
	public IWidget getCopy() {
		VideoTextWidget w = new VideoTextWidget();
		w.setVideo(currentVideo);
		w.setVideoWidth(currentVideoWidth);
		w.setText(textContainer.getHTML());
		w.setFloatingStyle(currentVideoFloatingStyle);

		return w;
	}

	@Override
	public String getWidgetHTML() {
		return widgetHTMLContent.toString();
	}

	@Override
	public Element getWidgetXML(Document xml) {
		com.google.gwt.xml.client.Element widget = xml.createElement(WEBPAGE_XML_TAGS.WIDGET.getTag());
		widget.setAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag(), type.getName());
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO_WIDTH.getTag(), Integer.toString(currentVideoWidth));
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO.getTag(), currentVideo);
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_VIDEO_FLOAT.getTag(), currentVideoFloatingStyle);
		AppUtils.appendNewChildAndCDATAValue(xml, widget, WEBPAGE_XML_TAGS.VIDEO_TEXT_WIDGET_TEXT.getTag(), textContainer.getHTML());
		return widget;
	}
	

}
