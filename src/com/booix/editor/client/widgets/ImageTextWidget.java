package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

public class ImageTextWidget extends Composite implements IWidget {
	
	private WidgetType type = WidgetType.IMAGE_TEXT;
	
	//Widget Data
	private String currentImageWidth 	= AppUtils.IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_WIDTH;
	private String currentImageFloatingStyle = AppUtils.IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_FLOATING_STYLE;
	private String currentImageUrl = AppUtils.IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_URL;
	// text can be accessed directly
	
	//Widget Data	
	private VerticalPanel mainPanel = new VerticalPanel();
	private HTMLPanel widgetHTMLContent= new HTMLPanel("<div id=\""+type.getName() + "Image" + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID+"\"></div><div id=\""+type.getName() + "Text" + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID+"\"></div>");
	private Image image = new Image();
	private HTML text = new HTML(AppUtils.IMAGE_TEXT_WIDGET_DEFAULT_TEXT);
	private ImageTextTabbedPanel tabbedPanel = new ImageTextTabbedPanel(this);

	public ImageTextWidget() {
		initWidget(mainPanel);
		addStyleName(type.getCSSName());
		
		widgetHTMLContent.getElement().setId(type.getName() + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID);
		widgetHTMLContent.addStyleName(type.getCSSName());
		widgetHTMLContent.addAndReplaceElement(image, type.getName() + "Image" + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID);
		widgetHTMLContent.addAndReplaceElement(text, type.getName() +"Text"+AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID);
		
		text.getElement().setId( type.getName() + "Text" + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID);
		
		image.getElement().setId( type.getName() + "Image" + AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID);
		image.addStyleName(currentImageFloatingStyle);
		image.addStyleName( type.getCSSName() + "Image");
		image.setWidth(currentImageWidth);
		setImageURL(currentImageUrl);

		mainPanel.add(tabbedPanel);
		mainPanel.add(widgetHTMLContent);
		
		endEditing();
		
		AppUtils.IMAGE_TEXT_WIDGET_NEXT_ID++;// bump id for next
	}

	@Override
	public void endEditing() {
		tabbedPanel.setVisible(false);
		tabbedPanel.endEditing();
	}

	@Override
	public void startEditing() {
		tabbedPanel.setVisible(true);
		tabbedPanel.startEditing();
	}

	@Override
	public HasClickHandlers getWidgetToInitEdition() {
		return text;
	}

	@Override
	public WidgetType getWidgetType() {
		return type;
	}

	@Override
	public IWidget getCopy() {
		ImageTextWidget w = new ImageTextWidget();
		w.setImageFloatingStyle(currentImageFloatingStyle);
		w.setImageURL(currentImageUrl);
		w.setImageWidth(currentImageWidth);
		w.setText(text.getHTML());
		return w;
	}
	
	public Image getImage() {
		return image;
	}
	
	public HTML getText(){
		return text;
	}
	
	public void setText(String text){
		this.text.setHTML( text );
	}
	
	public void setImageURL(String imageUrl){
		currentImageUrl = imageUrl;
		image.setUrl(imageUrl);
	}
	
	public void setImageFloatingStyle(String floatingStyle){
		if(!floatingStyle.equals("")){
			currentImageFloatingStyle = floatingStyle;
		}
		image.removeStyleName("floatRight");
		image.removeStyleName("floatCenter");
		image.removeStyleName("floatLeft");
		image.addStyleName(currentImageFloatingStyle);
	}
	
	public void setImageWidth( String width){
		currentImageWidth = width;
		image.setWidth(width);
	}

	@Override
	public String getWidgetHTML() {
		return widgetHTMLContent.toString();
	}

	@Override
	public Element getWidgetXML(Document xml) {
		com.google.gwt.xml.client.Element widget = xml.createElement(WEBPAGE_XML_TAGS.WIDGET.getTag());
		widget.setAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag(), type.getName());
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_WIDTH.getTag(), currentImageWidth);
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_URL.getTag(), currentImageUrl);
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_IMAGE_FLOAT.getTag(), currentImageFloatingStyle);
		AppUtils.appendNewChildAndCDATAValue(xml, widget, WEBPAGE_XML_TAGS.IMAGE_TEXT_WIDGET_TEXT.getTag(), text.getHTML());
		return widget;
	}
	
}
