package com.booix.editor.client.widgets;

import java.util.ArrayList;
import java.util.List;

import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.HTML_SUPPORTED_DIV_ID;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.hp.gagawa.client.elements.Body;
import com.hp.gagawa.client.elements.CustomTag;
import com.hp.gagawa.client.elements.Div;
import com.hp.gagawa.client.elements.Head;
import com.hp.gagawa.client.elements.Html;
import com.hp.gagawa.client.elements.Link;
import com.hp.gagawa.client.elements.Meta;
import com.hp.gagawa.client.elements.Title;


public class InnerPageWidget{ 
	private String name = "";
	private List<DnDWidgetContainer> mainContentWidgets = null;
	private List<DnDWidgetContainer> sideContentWidgets = null;
	
	public InnerPageWidget(){
		mainContentWidgets = new ArrayList<DnDWidgetContainer>();
		sideContentWidgets = new ArrayList<DnDWidgetContainer>();
	}
	
	public String getName() {
		return name;
	}
	
	public String getNameForLink(){
		return name.replaceAll(" ", "_").toLowerCase() + ".html";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public List<DnDWidgetContainer> getMainContentWidgets() {
		return mainContentWidgets;
	}
	public void setMainContentWidgets(List<DnDWidgetContainer> mainContentWidgets) {
		this.mainContentWidgets = mainContentWidgets;
	}
	public List<DnDWidgetContainer> getSideContentWidgets() {
		return sideContentWidgets;
	}
	public void setSideContentWidgets(List<DnDWidgetContainer> rightContainerWidgets) {
		this.sideContentWidgets = rightContainerWidgets;
	}
	
	public Element getInnerPageXML(Document xml){
		Element innerPage = xml.createElement(AppUtils.WEBPAGE_XML_TAGS.INNER_PAGE.getTag());
		
		AppUtils.appendNewChildAndValue(xml, innerPage, AppUtils.WEBPAGE_XML_TAGS.INNER_PAGE_NAME.getTag(), getName());
		
		Element mainContainerE = xml.createElement(AppUtils.WEBPAGE_XML_TAGS.MAIN_CONTENT.getTag());
		Element sideContainerE = xml.createElement(AppUtils.WEBPAGE_XML_TAGS.SIDE_CONTENT.getTag());
		
		for (DnDWidgetContainer widget : mainContentWidgets) {
			mainContainerE.appendChild(widget.getChildWidget().getWidgetXML(xml));
		}
		
		for (DnDWidgetContainer widget : sideContentWidgets) {
			sideContainerE.appendChild(widget.getChildWidget().getWidgetXML(xml));
		}
		
		innerPage.appendChild(mainContainerE) ;
		innerPage.appendChild(sideContainerE) ;
		
		return innerPage;
	}
	
	public Element getInnerPageHTML(Document doc){
		
		Element innerPage = doc.createElement(AppUtils.WEBPAGE_XML_TAGS.INNER_PAGE.getTag());
		
		//This is cool
		Html html = new Html();
		Head head = new Head();
		Body body = new Body();
		
		Title title = new Title();
		title.appendText(AppService.get().getCurrentWebPage().getTitle());
		
		Meta metaDescription = new Meta(AppService.get().getCurrentWebPage().getDescription());
		metaDescription.setName(WEBPAGE_XML_TAGS.DESCRIPTION.getTag());
		
		Meta metaKeywords = new Meta(AppService.get().getCurrentWebPage().getKeywords());
		metaKeywords.setName(WEBPAGE_XML_TAGS.KEYWORDS.getTag());
		
		Meta metaHttpEquiv = new Meta("text/html; charset=iso-8859-1");
		metaHttpEquiv.setHttpEquiv("Content-Type");
		
		Link link = new Link();
		link.setRel("stylesheet") ;
		link.setType("text/css");
		int templateId = AppService.get().getCurrentWebPage().getTemplate().getId();
		link.setHref(GWT.getModuleBaseURL() + "server/templates/template"+templateId+"/style.css");
		
		Link link2 = new Link();
		link2.setRel("stylesheet") ;
		link2.setType("text/css");
		link2.setHref(GWT.getHostPageBaseURL()+"booixeditor.css");
		
		head.appendChild(title);
		head.appendChild(metaDescription);
		head.appendChild(metaKeywords);
		head.appendChild(metaHttpEquiv);
		head.appendChild(link2);
		head.appendChild(link);
		
		html.appendChild(head);
		html.appendChild(body);

		Div banner = new Div(); banner.setId( HTML_SUPPORTED_DIV_ID.BANNER.getId());
		Div menu = new Div(); menu.setId(HTML_SUPPORTED_DIV_ID.MENU.getId());
		Div mainContainer = new Div(); mainContainer.setId(HTML_SUPPORTED_DIV_ID.MAIN_CONTENT.getId());
		Div rightContainer = new Div(); rightContainer.setId(HTML_SUPPORTED_DIV_ID.SIDE_CONTENT.getId());
		Div footer = new Div(); footer.setId(HTML_SUPPORTED_DIV_ID.FOOTER.getId());
		
		banner.appendText(AppService.get().getCurrentWebPage().getBannerWidget().getWidgetHTML());
		
		menu.appendText(AppController.get().getAppScreen().getWebPageEditor().getMenu().getHTMLContent()) ;

		/*for (DnDWidgetContainer widget : leftContainerWidgets) {
			content.appendText(widget.getChildWidget().getInnerHTML());
		}*/
		
		for (DnDWidgetContainer widget : mainContentWidgets) {
			mainContainer.appendText(widget.getChildWidget().getWidgetHTML());
		}
		
		//Addding adsense to the side bar
		rightContainer.appendText("<div id=\""+HTML_SUPPORTED_DIV_ID.ADSENSE.getId()+"\" style=\"width:100%; height:50px; background:red;\" >"+AppUtils.ADDSENSE_CODE+"</div>");
		for (DnDWidgetContainer widget : sideContentWidgets) {
			rightContainer.appendText(widget.getChildWidget().getWidgetHTML());
		}
		
		footer.appendText("<p> Web Page created By Booix.com </p>");
		
		String template = AppService.get().getCurrentWebPage().getTemplate().getHtml();
		template = template.replaceAll("<div id=\""+HTML_SUPPORTED_DIV_ID.BANNER.getId()+"\"></div>", AppUtils.forRegEx(banner.write())) ;
		template = template.replaceAll("<div id=\""+HTML_SUPPORTED_DIV_ID.MENU.getId()+"\"></div>", AppUtils.forRegEx(menu.write())) ;
		template = template.replaceAll("<div id=\""+HTML_SUPPORTED_DIV_ID.MAIN_CONTENT.getId()+"\"></div>", AppUtils.forRegEx(mainContainer.write())) ;
		template = template.replaceAll("<div id=\""+HTML_SUPPORTED_DIV_ID.SIDE_CONTENT.getId()+"\"></div>", AppUtils.forRegEx(rightContainer.write())) ;
		template = template.replaceAll("<div id=\""+HTML_SUPPORTED_DIV_ID.FOOTER.getId()+"\"></div>", AppUtils.forRegEx(footer.write())) ;
		
		body.appendText(template) ;
		
		Log.debug(html.toString());
		
		AppUtils.appendNewChildAndValue(doc, innerPage, "name", getNameForLink());
		Element htmlE = doc.createElement("html");
		htmlE.appendChild(doc.createCDATASection(AppUtils.HTML_FILE_DOC_TYPE + html.toString()));
		innerPage.appendChild(htmlE);
		return innerPage;
	}

}
