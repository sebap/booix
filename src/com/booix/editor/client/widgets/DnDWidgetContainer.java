package com.booix.editor.client.widgets;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.HTML_SUPPORTED_DIV_ID;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Drag and Drop Widget Container
 * This container is used to display a border a title and 3 common buttons
 * @author Owner
 *
 */

public class DnDWidgetContainer extends Composite{
	
	private IWidget child = null;
	private VerticalPanel container = null; //this is the container where this widgets is added to
	
	private VerticalPanel mainLayout = new VerticalPanel();
	private RoundedPanel roundTitle = null;
	private Label widgetName = new Label();
	private PushButton duplicateButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.duplicateIcon());
	private PushButton editButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.editIcon());
	private PushButton doneButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.saveIcon());
	private PushButton deleteButton = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.deleteIcon());
	private SimplePanel childContainer = new SimplePanel();
	
	private static boolean editing = false;
	private static boolean deleting = false;
	
	/**
	 * 
	 * @param child
	 * @param container //just pass null when we do not know what is the container yet
	 */
	public DnDWidgetContainer( IWidget child, VerticalPanel container ){
		this.child = child;
		this.container = container;
		buildTitleBar();		
		buildMainLayout();
		mainLayout.setWidth("100%");
		initWidget(mainLayout);
		//set style names
		setStyleName("gwtapps-GadgetContainer");
		//editPanel.setStyleName("gwtapps-GadgetContainerEditPanel");
		mainLayout.setStyleName("gwtapps-GadgetContainerPanel");
		registerListeners();
		setContainerVisible(false);
	}
	
	public void setContainer(VerticalPanel container){
		this.container = container;
	}
	
	
	private void setContainerVisible(boolean val){
		roundTitle.setVisible(val);
		if(val){
			childContainer.setStyleName("gwtapps-GadgetContainerBody");
		}else{
			 childContainer.removeStyleName("gwtapps-GadgetContainerBody");
			
		}
		
	}
	
	protected void buildMainLayout(){
		roundTitle = new RoundedPanel("#bdd3f3",buildTitleBar(),RoundedPanel.ROUND_TOP);
		//roundTitle = new RoundedPanel("green",titleBar,RoundedPanel.ROUND_BOTH);
		roundTitle.setWidth("100%");
		mainLayout.add( roundTitle );
		/*if( child.getGadgetClass().getUserPrefsCount() > 0 ){
			mainLayout.add( editPanel );
			editPanel.setWidth("100%");
			buildEditPanel();
		}*/
		childContainer.setWidget( (Widget)child );		
		mainLayout.add( childContainer );
	}
	
	protected HorizontalPanel buildTitleBar(){
		//titleBar.add( minimizeButton );
		HorizontalPanel titleBar = new HorizontalPanel();
		titleBar.add( widgetName );
		
		titleBar.add( duplicateButton );
		titleBar.add( editButton );
		titleBar.add( doneButton );
		titleBar.add( deleteButton );
		titleBar.setCellWidth( widgetName, "100%" );
		titleBar.setWidth("100%");
		widgetName.setWidth("100%");
		widgetName.setText(((Widget)child).getTitle());
		titleBar.setStyleName("gwtapps-GadgetContainerTitleBar");

		widgetName.setStyleName("gwtapps-GadgetContainerTitle");
		widgetName.setText(child.getWidgetType().getDisplayName());
		
		String buttonHeight = "15px";
		editButton.setHeight(buttonHeight);
		doneButton.setHeight(buttonHeight);
		deleteButton.setHeight(buttonHeight);
		duplicateButton.setHeight(buttonHeight);
		
		doneButton.setVisible(false);
		
		return titleBar;
	}
	
	protected void registerListeners(){
		
		deleteButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				startDeleting();
				if( Window.confirm( AppService.get().getMessages().getRemoveConfirmation()) ){
					stopEditing();
					AppController.get().getAppScreen().getWebPageEditor().removeFromWebPage(DnDWidgetContainer.this);
					DnDWidgetContainer.this.removeFromParent();
				}
				stopDeleting();
			}
		});
		
		editButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(editing == false){
					startEditing();
				}
			}
		});
		
		doneButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(editing == true){
					stopEditing();
				}
			}
		});
		
		duplicateButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				if(DnDWidgetContainer.this.container.getElement().getId().equals(HTML_SUPPORTED_DIV_ID.MAIN_CONTENT.getId())){
					AppController.get().getAppScreen().getWebPageEditor().insertWidgetInMainContainer(new DnDWidgetContainer(child.getCopy(),DnDWidgetContainer.this.container ), DnDWidgetContainer.this.container.getWidgetIndex(DnDWidgetContainer.this));
				}else if(DnDWidgetContainer.this.container.getElement().getId().equals(HTML_SUPPORTED_DIV_ID.SIDE_CONTENT.getId())){
					AppController.get().getAppScreen().getWebPageEditor().insertWidgetInRightContainer(new DnDWidgetContainer(child.getCopy(),DnDWidgetContainer.this.container ), DnDWidgetContainer.this.container.getWidgetIndex(DnDWidgetContainer.this));
				}
			}
		});
		
		child.getWidgetToInitEdition().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				startEditing();
			}
		});
		
		this.sinkEvents(Event.ONMOUSEOVER);
		this.sinkEvents(Event.ONMOUSEOUT);

	}
	
	public IWidget getChildWidget(){
		return child;
	}

	public Label getTitleLabel(){
		return widgetName;
	}
	
	public void onBrowserEvent(Event event) {
		
		switch(DOM.eventGetType(event)) {
        	case Event.ONMOUSEOVER:
        		if((editing == false) && (deleting == false)){
        			setContainerVisible(true);
        		}
        		break;
        	case Event.ONMOUSEOUT:
        		if((editing == false) && (deleting == false)){
        			setContainerVisible(false);
        		}
        		break;
        	default:
        		break;
        }
    }
	
	private void startEditing(){
		if (editing == false){
			editButton.setVisible(false);
			doneButton.setVisible(true);
			child.startEditing();
			editing = true;
		}
		
	}
	
	public void stopEditing(){
		if(editing == true){
			
			editButton.setVisible(true);
			doneButton.setVisible(false);
			child.endEditing();
			editing = false;
		}
	}
	
	private void startDeleting(){
		if (deleting == false){
			deleting = true;
		}
	}
	
	private void stopDeleting(){
		if (deleting == true){
			deleting = false;
		}
	}
	
	public static boolean isEditing(){
		return editing;
	}

}
