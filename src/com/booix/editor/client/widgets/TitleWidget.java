package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.WidgetType;
import com.booix.editor.client.AppUtils.WEBPAGE_XML_TAGS;
import com.booix.editor.client.screens.toolbar.RichTextToolbar;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

public class TitleWidget extends Composite implements IWidget {
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private RichTextArea area = new RichTextArea();
	private RichTextToolbar toolbar = new RichTextToolbar(area);
	private HTML widgetHTMLContent = new HTML();
	private String title = AppUtils.TITLE_WIDGET_DEFAULT_TEXT;
	private WidgetType type = WidgetType.TITLE;
	
	public TitleWidget() {
		initWidget(mainPanel);
		addStyleName(type.getCSSName());
		
		widgetHTMLContent.addStyleName(type.getCSSName());
		widgetHTMLContent.getElement().setId( type.getName() + AppUtils.TITLE_WIDGET_NEXT_ID);
		
		area.getElement().getStyle().setBackgroundColor("white");
		area.setWidth("100%");
		area.setHTML(title);

		//panel.add(toolbar);
		mainPanel.add(area);
		mainPanel.setWidth("100%");
		mainPanel.add(widgetHTMLContent);
		
		endEditing(); //call this to handle the visibility;
		
		AppUtils.TITLE_WIDGET_NEXT_ID++;
	}
	
	public void setTitle(String title){
		//this.title = AppUtils.forHTML(title);
		this.title = title;
		widgetHTMLContent.setHTML("<h2>"+this.title+"</h2>");
	}

	@Override
	public void endEditing() {
		toolbar.setVisible(false);
		area.setVisible(false);
		title = area.getHTML();
		title = title.replaceAll("\\<.*?\\>", "");
		setTitle(title);
		widgetHTMLContent.setVisible(true);
	}

	@Override
	public void startEditing() {
		widgetHTMLContent.setVisible(false);
		toolbar.setVisible(true);
		area.setVisible(true);
		String title = widgetHTMLContent.getHTML().replaceAll("\\<.*?\\>", "");
		area.setHTML("<h2>"+title+"</h2>");
	}

	@Override
	public HasClickHandlers getWidgetToInitEdition() {
		return widgetHTMLContent;
	}

	@Override
	public WidgetType getWidgetType() {
		return type;
	}

	@Override
	public IWidget getCopy() {
		TitleWidget w = new TitleWidget();
		w.setTitle(title);
		return w;
	}

	@Override
	public String getWidgetHTML() {
		return widgetHTMLContent.toString();
	}

	@Override
	public Element getWidgetXML(Document xml) {
		com.google.gwt.xml.client.Element widget = xml.createElement(WEBPAGE_XML_TAGS.WIDGET.getTag());
		widget.setAttribute(WEBPAGE_XML_TAGS.WIDGET_TYPE.getTag(), type.getName());
		AppUtils.appendNewChildAndValue(xml, widget, WEBPAGE_XML_TAGS.TITLE_WIDGET_TITLE.getTag(), title);
		return widget;
	}

}
