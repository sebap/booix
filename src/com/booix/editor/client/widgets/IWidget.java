package com.booix.editor.client.widgets;

import com.booix.editor.client.AppUtils.WidgetType;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

public interface IWidget {
	
	public void startEditing();
	
	public void endEditing();
	
	public HasClickHandlers getWidgetToInitEdition();
	
	public WidgetType getWidgetType();
	
	public IWidget getCopy();
	
	/**
	 * This method returns widget HTML. It will be used to generate the Webpage.
	 * @return
	 */
	public String getWidgetHTML();
	
	/**
	 * This method the widget in XML format. used to store teh widget settings, for later retrieving.
	 * @return
	 */
	public Element getWidgetXML(Document xml);

}
