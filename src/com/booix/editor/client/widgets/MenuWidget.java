package com.booix.editor.client.widgets;

import com.booix.editor.client.AppController;
import com.booix.editor.client.AppService;
import com.booix.editor.client.AppUtils;
import com.booix.editor.client.AppUtils.HTML_SUPPORTED_DIV_ID;
import com.booix.editor.client.AppUtils.WidgetType;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * This class is used to manage the Navigation menu in the webpage.
 * For now we will support only One level.
 * 
 * We need to investigate further to support multilevel menus
 * @author Owner
 *
 */
public class MenuWidget extends Composite {
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private HTML widgetHTMLContent = new HTML();
	private WidgetType type = WidgetType.MENU;
	private PushButton editB = AppUtils.getStandardSmallPushButton(AppUtils.APP_IMAGE_BUNDLE.editIcon());
	
	
	public MenuWidget() {
		initWidget(mainPanel);
		sinkEvents(Event.ONCLICK);
		sinkEvents(Event.ONMOUSEOVER);
		sinkEvents(Event.ONMOUSEOUT);
		getElement().setId(HTML_SUPPORTED_DIV_ID.MENU.getId());
		create();
		
	
		
	}
	
	private void create(){
		mainPanel.add(widgetHTMLContent);
		mainPanel.add(editB);
		editB.setVisible(false);
		widgetHTMLContent.getElement().setId(HTML_SUPPORTED_DIV_ID.MENU.getId());
		
		editB.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				showEditor();
			}
		});
	}
	
	private void showEditor(){
		AppController.get().getAppScreen().getWebPageEditor().showMenuEditor();
		removeStyleName( type.getCSSName() + "-MouseOver");
		editB.setVisible(false);
	}
	
	
	

	public void updateContent(){
		StringBuffer html = new StringBuffer();
		html.append("<ul>");
		for (InnerPageWidget innerPageWidget : AppService.get().getCurrentWebPage().getInnerPageList()) {
			if(AppService.get().getCurrentInnerPageWidget().getName().equals(innerPageWidget.getName())){
				// we can add some active link css here
				html.append("<li><a href=\""+ innerPageWidget.getNameForLink()+"\">"+innerPageWidget.getName()+"</a></li>");
			}else{
				html.append("<li><a href=\""+ innerPageWidget.getNameForLink()+"\">"+innerPageWidget.getName()+"</a></li>");
			}
		}
		html.append("</ul>");
		widgetHTMLContent.getElement().setInnerHTML(html.toString());
	}
	
	@Override
	public void onBrowserEvent(Event event) {
		Element el = DOM.eventGetTarget(event);
        switch(DOM.eventGetType(event)) {
                case Event.ONCLICK:
                	if(DnDWidgetContainer.isEditing()){
                		Window.alert("Your are editing something please close");
                		return;
                	}
                	int i = 0;
                	for (InnerPageWidget innerPageWidget : AppService.get().getCurrentWebPage().getInnerPageList()) {
                		if(innerPageWidget.getName().equals(el.getInnerHTML().toString())){
                			AppService.get().setCurrentInnerPageId(i);
                        	AppController.get().getAppScreen().getWebPageEditor().update();
                        	DOM.eventPreventDefault(event) ;
                        	return;
                		}
                		i++;
                	}
                	
                	//If the user is not clicking on the links, user might be clicking somewhere else.
                	//show the editor
                	showEditor();
                	DOM.eventPreventDefault(event) ;
                	break;
                case Event.ONMOUSEOVER:
                	setStyleName( type.getCSSName() + "-MouseOver" );
                	editB.setVisible(true);
                	break;
                case Event.ONMOUSEOUT:
                	removeStyleName(type.getCSSName() + "-MouseOver");
                	editB.setVisible(false);
                	break;
                default:
                	break;
        }
    }
	
	public String getHTMLContent(){
		return widgetHTMLContent.getHTML();
	}
}
