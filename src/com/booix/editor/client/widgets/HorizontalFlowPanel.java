package com.booix.editor.client.widgets;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class HorizontalFlowPanel extends FlowPanel {
	public void add(Widget w) {
		w.getElement().getStyle().setProperty("display", "block");
		super.add(w);
	}
}
