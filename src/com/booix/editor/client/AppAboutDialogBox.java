package com.booix.editor.client;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AppAboutDialogBox extends DialogBox {

	private VerticalPanel mainPanel = new VerticalPanel();
	private HTML msg = new HTML();
	
	
	public AppAboutDialogBox() {
		getElement().setId( AppUtils.CSS_PREFIX + "AppAboutDialogBox");
		setHTML("About");
		setModal(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(true);
		create();
	}
	
	private void create(){
		final Grid grid = new Grid(2, 2);
		grid.setWidget(0, 0, msg);
		mainPanel.add(grid);
		add(mainPanel);
		msg.setWidth("300px");
		msg.setAutoHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		msg.setHTML("<br><div><b>www.booix.com</b>" +
				"<br>Editor Version: 0.3" +
				"<br>Build Date: 11/06/2010" +
				"<br><br>Copyrights 2010. All Rights Reserved.</div>");
	}
	

	@Override
	public void show(){
		super.show();
		center();
	}
	
	@Override
	public void hide(){
		super.hide();
	}
	
}
