package com.booix.editor.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoadingDialogBox extends DialogBox {

	private VerticalPanel mainPanel = new VerticalPanel();
	private Label msg = new Label();
	private Image image = new Image(AppUtils.APP_IMAGE_BUNDLE.loaderIcon());
	
	public LoadingDialogBox() {
		getElement().setId( AppUtils.CSS_PREFIX + "LoadingDialogBox");
		setHTML("Please wait...");
		setModal(true) ;
		setGlassEnabled(true) ;
		setAutoHideEnabled(false);
		create();
	}
	
	private void create(){
		
		final Grid grid = new Grid(2, 2);
		grid.setWidget(0, 0, image);
		grid.setWidget(0, 1, msg);
		mainPanel.add(grid);
		add(mainPanel);
		msg.setWidth("300px");
		msg.setAutoHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	}
	

	
	public void show(String message){
		Log.info(LoadingDialogBox.class.getName() + ".show() - message:" + message );
		msg.setText(message);
		super.show();
		center();
	}
	
	@Override
	public void hide(){
		Log.info(LoadingDialogBox.class.getName() + ".hide()");
		super.hide();
	}
	
}
