package com.booix.editor.client;

import com.allen_sauer.gwt.log.client.DivLogger;
import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;

public class AppUtils {
	
	public static String CSS_PREFIX = "gwtapps-";
	
	public static String FACEBOOK_API_KEY = "key";
	public static String FACEBOOK_API_ID = "id";
	
	public static String ADDSENSE_CODE = 	"add_sense_code_here";
	
	public static final String HTML_FILE_DOC_TYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
	public static final String XML_FILE_DOC_TYPE = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";

	public interface AppImageBundle extends ClientBundle {
		
		@Source("./images/loader.gif") 				ImageResource loaderIcon();
		@Source("./images/title_widget.png") 		ImageResource titleWidgetIcon();
		@Source("./images/text_widget.png")			ImageResource textWidgetIcon();
		@Source("./images/video_widget.png") 		ImageResource videoWidgetIcon();
		@Source("./images/image_text_widget.png")	ImageResource imageTextWidgetIcon();
		
		@Source("./images/up.png")	 				ImageResource upIcon();
		@Source("./images/down.png") 				ImageResource downIcon();
		@Source("./images/next.png") 				ImageResource nextIcon();
		@Source("./images/previous.png")			ImageResource previousIcon();
		@Source("./images/page_add.png")			ImageResource pageAddIcon();
		@Source("./images/page_delete.png")			ImageResource pageDeleteIcon();
		
		@Source("./images/image_placeholder.jpg")	ImageResource imagePlaceHolderImage();
		@Source("./images/magnifier_zoom_in.png")	ImageResource increaseIcon();
		@Source("./images/magifier_zoom_out.png") 	ImageResource decreaseIcon();
		@Source("./images/video_right.png")			ImageResource videoMoveRightIcon();
		@Source("./images/video_center.png")		ImageResource videoCenterIcon();
		@Source("./images/video_left.png")			ImageResource videoMoveLeftIcon();
		@Source("./images/video_add.png")			ImageResource videoUploadIcon();
		@Source("./images/picture_right.png") 		ImageResource imageMoveRightIcon();	
		@Source("./images/picture_center.png") 		ImageResource imageCenterIcon();
		@Source("./images/picture_left.png")		ImageResource imageMoveLeftIcon();
		@Source("./images/picture_add.png")			ImageResource imageUploadIcon();
		@Source("./images/accept.png")				ImageResource saveIcon();
		@Source("./images/refresh.gif")				ImageResource refreshIcon();
		@Source("./images/cancel.png")				ImageResource cancelIcon();
		@Source("./images/delete.png")				ImageResource deleteIcon();
		@Source("./images/application_edit.png")	ImageResource editIcon();
		@Source("./images/application_double.png")	ImageResource duplicateIcon();
	}

	public static AppImageBundle APP_IMAGE_BUNDLE = (AppImageBundle) GWT.create( AppImageBundle.class );
	
	public static final int INCREASE_DECREASE_FACTOR = 30;
	
	public static int TITLE_WIDGET_NEXT_ID = 0;
	public static final String TITLE_WIDGET_DEFAULT_TEXT = AppService.get().getMessages().getTitleWidgetTitlePlaceholder();
	
	public static int TEXT_WIDGET_NEXT_ID = 0;
	public static final String TEXT_WIDGET_DEFAULT_TEXT = AppService.get().getMessages().getTextWidgetTextPlaceholder();
	
	public static int IMAGE_TEXT_WIDGET_NEXT_ID = 0;
	public static final String IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_WIDTH 	  = "200px";
	public static final String IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_FLOATING_STYLE = "floatLeft";
	public static final String IMAGE_TEXT_WIDGET_DEFAULT_IMAGE_URL = new Image(APP_IMAGE_BUNDLE.imagePlaceHolderImage()).getUrl();
	public static final String IMAGE_TEXT_WIDGET_DEFAULT_TEXT = AppService.get().getMessages().getImageTextWidgetTextPlaceholder();
	
	//Defaults used in VideoTextWidget
	public static int VIDEO_TEXT_WIDGET_NEXT_ID = 0;
	public static final String VIDEO_TEXT_WIDGET_DEFAULT_VIDEO 		= "OUmbvvzKWLw"; //Support for Youtube videos for now
	public static final int VIDEO_TEXT_WIDGET_DEFAULT_VIDEO_WIDTH_INT 		= 200;
	public static final String VIDEO_TEXT_WIDGET_DEFAULT_FLOATING_STYLE 	= "floatLeft";
	public static final String VIDEO_TEXT_WIDGET_DEFAULT_TEXT 				= AppService.get().getMessages().getVideoTextWidgetTextPlaceholder();

	

	

	public static PushButton getStandardSmallPushButton(ImageResource imgResource){ 
		PushButton button = new PushButton(new Image(imgResource));
		formatSmallButton(button);
		return button;
	}
	
	public static void formatSmallButton(ButtonBase button){ 
		button.addStyleName("floatLeft");
		button.addStyleName( CSS_PREFIX + "AppSmallButtonSize");
		button.getElement().getStyle().setMarginLeft(2, Unit.PX);
		button.getElement().getStyle().setMarginRight(2, Unit.PX);
	}
	
	public static enum WidgetType {
		
		BANNER("BannerWidget", AppService.get().getMessages().getBannerWidgetDisplayText()),
		MENU("MenuWidget", AppService.get().getMessages().getMenuWidgetDisplayText()),
		TITLE("TitleWidget", AppService.get().getMessages().getTitleWidgetDisplayText()),
		TEXT("TextWidget", AppService.get().getMessages().getTextWidgetDisplayText()),
		IMAGE_TEXT("ImageTextWidget" , AppService.get().getMessages().getImageTextWidgetDisplayText()),
		VIDEO_TEXT("VideoTextWidget" , AppService.get().getMessages().getVideoTextWidgetDisplayText());
		
		private String name = null;
		private String displayName = null;
		private WidgetType(String name, String displayName) {
			this.name = name;
			this.displayName = displayName;
		}
		
		public String getName(){
			return name;
		}
		
		public String getDisplayName(){
			return displayName;
		}
		
		public String getCSSName(){
			return  CSS_PREFIX + name;
		}
	}
	
	public static enum WEBPAGE_TYPE{
		PERSONAL,
		BUSINESS;
	}
	
	public static enum ACCOUNT_TYPE{
		FREE(0, 5, 2),
		PRO(1, 10, 10);
		
		private int id = 0;
		private int maxInnerPages = 5;
		private int maxWebPages = 5;
		
		ACCOUNT_TYPE(int id, int maxInnerPages, int maxWebPages){
			this.id =id;
			this.maxInnerPages = maxInnerPages;
			this.maxWebPages = maxWebPages;
		}
		
		public int getId() {
			return id;
		}
		
		public int getMaxPages(){
			return maxInnerPages;
		}
		
		public int getMaxWebPages(){
			return maxWebPages;
		}
	}
	
	public static enum USER_TYPE{
		BOOIX,
		FACEBOOK;
	}
	
	
	/**
	 * This is the same as the RESPONSE_TYPE in the php code
	 * @author Owner
	 *
	 */
	public static enum RESPONSE_TYPE{
		GENERIC_FAILURE, // this is a generic failure response type, i this we could create a event for this type of response and do all the cleanup as needed.
		USER_STORE_SUCCESS,
		USER_STORE_FAIL,
		USER_STORE_ALREADY_EXIST,
		USER_LOGIN_SUCCESSED,
		USER_LOGIN_FAILED,
		WEBPAGE_STORE_STORED,
		WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE,
		WEBPAGE_DELETE_DELETED,
		WEBPAGE_LOAD_SUCCESSFUL,
		DOMAIN_CHECK_VALID,
		DOMAIN_CHECK_INVALID;
	}
	
	public static enum SERVICE_ACTION_TYPE{
		FACEBOOK_USER_LOGIN, // this action means the user has already signed in with facebook accoutn and we need to get the data from booix
		BOOIX_USER_LOGIN,
		USER_STORE,
		DOMAIN_CHECK,
		WEBPAGE_LOAD,
		WEBPAGE_CREATE,
		WEBPAGE_STORE,
		WEBPAGE_DELETE,
		WEBPAGE_CREATE_SUBDOMAIN,
		WEBPAGE_DELETE_SUBDOMAIN;
	}
	
	
	public static enum WEBPAGE_XML_TAGS {
		WEB_PAGE("webPage"),
		WEB_PAGE_ID ("webPageId"),
		TITLE ("title"),
		SUBDOMAIN ("subdomain"), //without the .booix.com
		TYPE ("type"), // webpage type see WEBPAGE_TYPE
		TYPE_DESCRIPTION ("typeDescription"), 
		KEYWORDS ("keywords"),
		DESCRIPTION ("description"),
		TEMPLATE_ID ("templateId"),
		BACKGROUND_ID ("backgroundId"),
		BANNER_WIDGET ("bannerWidget"),
		BANNER_WIDGET_IMAGE ("image"), //tipically a image on the background
		BANNER_WIDGET_LOGO ("logo"), //logo isotype
		BANNER_WIDGET_TEXT ("text"), // text
		BANNER_WIDGET_SLOGAN ("slogan"), // slogan
		INNER_PAGE ("innerPage"),
		INNER_PAGE_NAME ("name"),
		MAIN_CONTENT ("mainContent"), 
		SIDE_CONTENT ("sideContent"),
		WIDGET ("widget"),
		WIDGET_TYPE ("type"),
		TITLE_WIDGET_TITLE("title"), 
		TEXT_WIDGET_TEXT ("text"),
		IMAGE_TEXT_WIDGET_IMAGE_URL ("imageURL"),
		IMAGE_TEXT_WIDGET_IMAGE_WIDTH ("imageWidth"), 
		IMAGE_TEXT_WIDGET_IMAGE_FLOAT ("imageFloat"), 
		IMAGE_TEXT_WIDGET_TEXT ("text"),
		VIDEO_TEXT_WIDGET_VIDEO ("video"),
		VIDEO_TEXT_WIDGET_VIDEO_WIDTH ("videoWidth"), 
		VIDEO_TEXT_WIDGET_VIDEO_FLOAT ("videoFloat"), 
		VIDEO_TEXT_WIDGET_TEXT ("text");
		
		
		
		private String tag = null;
		
		private WEBPAGE_XML_TAGS(String tag){
			this.tag = tag;
		}
		
		public String getTag(){
			return tag;
		}
	}
	
	/**
	 * This enum contains all the supported DIV ID for the templates.
	 * @author Owner
	 *
	 */
	public static enum HTML_SUPPORTED_DIV_ID {
		BANNER ("banner"), //contains logo image, logo text, slogan, and background image.
		MENU ("menu"), //navigation menu
		MAIN_CONTENT ("mainContent"), //main content of the page 
		SIDE_CONTENT ("sideContent"),
		FOOTER ("footer"),
		ADSENSE ("adsense");
		
		private String id = null;
		
		private HTML_SUPPORTED_DIV_ID(String tag){
			this.id = tag;
		}
		
		public String getId(){
			return id;
		}
	}
	
	public static void appendNewChildAndValue(Document doc, Element parentNode, String tag , String value){
		Element node = doc.createElement(tag);
		node.appendChild(doc.createTextNode(value));
		parentNode.appendChild(node);
	}
	
	public static void appendNewChildAndCDATAValue(Document doc, Element parentNode, String tag , String value){
		Element node = doc.createElement(tag);
		node.appendChild(doc.createCDATASection(value));
		parentNode.appendChild(node);
	}
	
	/**
	 * Uses UTF8 encode + replacing teh & by %26, so it wont break the encoding
	 * @param decoded
	 * @return
	 */
	public static String encodeURL(String decoded){
		return decoded.replaceAll("&", "%26");
	}
	
	public static String forHTML(String aText) {
		final StringBuilder result = new StringBuilder();

		for (char character : aText.toCharArray()) {
			if (character == '<') {
				result.append("&lt;");
			} else if (character == '>') {
				result.append("&gt;");
			} else if (character == '&') {
				result.append("&amp;");
			} else if (character == '\"') {
				result.append("&quot;");
			} else if (character == '\t') {
				addCharEntity(9, result);
			} else if (character == '!') {
				addCharEntity(33, result);
			} else if (character == '#') {
				addCharEntity(35, result);
			} else if (character == '$') {
				addCharEntity(36, result);
			} else if (character == '%') {
				addCharEntity(37, result);
			} else if (character == '\'') {
				addCharEntity(39, result);
			} else if (character == '(') {
				addCharEntity(40, result);
			} else if (character == ')') {
				addCharEntity(41, result);
			} else if (character == '*') {
				addCharEntity(42, result);
			} else if (character == '+') {
				addCharEntity(43, result);
			} else if (character == ',') {
				addCharEntity(44, result);
			} else if (character == '-') {
				addCharEntity(45, result);
			} else if (character == '.') {
				addCharEntity(46, result);
			} else if (character == '/') {
				addCharEntity(47, result);
			} else if (character == ':') {
				addCharEntity(58, result);
			} else if (character == ';') {
				addCharEntity(59, result);
			} else if (character == '=') {
				addCharEntity(61, result);
			} else if (character == '?') {
				addCharEntity(63, result);
			} else if (character == '@') {
				addCharEntity(64, result);
			} else if (character == '[') {
				addCharEntity(91, result);
			} else if (character == '\\') {
				addCharEntity(92, result);
			} else if (character == ']') {
				addCharEntity(93, result);
			} else if (character == '^') {
				addCharEntity(94, result);
			} else if (character == '_') {
				addCharEntity(95, result);
			} else if (character == '`') {
				addCharEntity(96, result);
			} else if (character == '{') {
				addCharEntity(123, result);
			} else if (character == '|') {
				addCharEntity(124, result);
			} else if (character == '}') {
				addCharEntity(125, result);
			} else if (character == '~') {
				addCharEntity(126, result);
			} else {
				// the char is not a special one
				// add it to the result as is
				result.append(character);
			}
		}

		return result.toString();

	}

	public static String forRegEx(String aText) {
		final StringBuilder result = new StringBuilder();
		for (char character : aText.toCharArray()) {
			/*
			 * All literals need to have backslashes doubled.
			 */
			if (character == '.') {
				result.append("\\.");
			} else if (character == '\\') {
				result.append("\\\\");
			} else if (character == '?') {
				result.append("\\?");
			} else if (character == '*') {
				result.append("\\*");
			} else if (character == '+') {
				result.append("\\+");
			} else if (character == '&') {
				result.append("\\&");
			} else if (character == ':') {
				result.append("\\:");
			} else if (character == '{') {
				result.append("\\{");
			} else if (character == '}') {
				result.append("\\}");
			} else if (character == '[') {
				result.append("\\[");
			} else if (character == ']') {
				result.append("\\]");
			} else if (character == '(') {
				result.append("\\(");
			} else if (character == ')') {
				result.append("\\)");
			} else if (character == '^') {
				result.append("\\^");
			} else if (character == '$') {
				result.append("\\$");
			} else {
				// the char is not a special one
				// add it to the result as is
				result.append(character);
			}
		}

		return result.toString();

	}

	private static void addCharEntity(Integer aIdx, StringBuilder aBuilder) {
		String padding = "";
		if (aIdx <= 9) {
			padding = "00";
		} else if (aIdx <= 99) {
			padding = "0";
		} else {
			// no prefix
		}
		String number = padding + aIdx.toString();
		aBuilder.append("&#" + number + ";");
	}
	
	public static void setLoggerVisible(boolean val){
		Widget divLogger = Log.getLogger(DivLogger.class).getWidget();
    	if(divLogger != null ){
    		Log.debug("Setting divLogger visible: " + !divLogger.isVisible());
    		divLogger.setVisible(false);
    	}
	}
	
	public static void showHideLogger(){
		Widget divLogger = Log.getLogger(DivLogger.class).getWidget();
    	if(divLogger != null ){
    		Log.debug("Setting divLogger visible: " + !divLogger.isVisible());
    		divLogger.setVisible(!divLogger.isVisible());
    	}
	}

}