package com.booix.editor.client;


import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.log.client.Log;
import com.booix.editor.client.screens.AppScreen;
import com.booix.editor.client.screens.init.InitialScreen;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * This is the Booix Edit App Controller. Based on several samples over internet,
 * based on the GWT suggestion of using: MVP pattern. Command Pattern for events EventBus
 * 
 * @author Sebastian Pereyro
 * 
 */
public class AppController implements ValueChangeHandler<String> {
	
	public static enum SCREENS{
		LOGIN,
		APPSCREEN;
	}
	
	private LoadingDialogBox loadingDialogBox = new LoadingDialogBox();
	private AppAboutDialogBox aboutDialogBox = new AppAboutDialogBox();
	private InitialScreen initialScreen = new InitialScreen();
	private AppScreen appScreen = null;
	private PickupDragController widgetsDragController = new PickupDragController(RootPanel.get(), true);
	
	private static AppController singleton = null;
	
	public static AppController get(){
		if(singleton == null){
			singleton = new AppController();
		}
		return singleton;
	}

	private AppController() {
		bind();
	}
	
	public LoadingDialogBox getLoadingDialogBox(){
		return loadingDialogBox;
	}
	
	public AppAboutDialogBox getAboutDialogBox(){
		return aboutDialogBox;
	}
	
	public AppScreen getAppScreen(){
		return appScreen;
	}
	
	public PickupDragController getDragController(){
		return widgetsDragController; 
	}

	private void bind() {
		// For History managment.
		History.addValueChangeHandler(this);
	}
	
	public void initApp(){
		restartApp();
	}
	
	public void signOut(){
		Window.Location.reload();
	}
	
	public void showAppScreen(){
		if(History.getToken().equals(SCREENS.APPSCREEN.toString())){
			History.fireCurrentHistoryState();
		}else{
			History.newItem(SCREENS.APPSCREEN.toString());
		}
	}
	
	//Restart App can be called at beginning or when signing out
	private void restartApp(){
		loadingDialogBox.hide();
		AppService.get().setCurrentUser( null );
		RootLayoutPanel.get().clear();
		RootLayoutPanel.get().getElement().setId("RootLayoutPanel");
		RootLayoutPanel.get().add(initialScreen);
	}
	
	/**
	 * Use this method to support History, We will implement this later when we
	 * know what we are doing. :D
	 * 
	 * This is also the one who decides which screen to display.
	 * 
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();
		Log.info("History value: " + token);
		if (token != null ) {
			if(token.equals(SCREENS.APPSCREEN.toString())){
				if(AppService.get().getCurrentUser() != null){ // user is logged in
					RootLayoutPanel.get().clear();
					if(appScreen == null){
						AppController.get().getLoadingDialogBox().show("Creating App Screen..");
						appScreen = new AppScreen();
						AppController.get().getLoadingDialogBox().hide();
					}
					RootLayoutPanel.get().add(appScreen);
					//AppFacebookAPI.get().getXFBML().parse(); //need to reparse after adding a new screen;
					
					/*if(AppService.get().getCurrentUser().getWebPageList() != null ){
						if(AppService.get().getCurrentUser().getWebPageList().size() == 0){
							appScreen.showAccountSettingsScreen();
						}else if(AppService.get().getCurrentUser().getWebPageList().size() == 1){
							AppService.get().loadWebPage(AppService.get().getCurrentUser().getWebPageList().get(0).getId());
						}else if (AppService.get().getCurrentUser().getWebPageList().size() > 1) {
							appScreen.showAccountSettingsScreen();
						}
					}else{
						appScreen.showAccountSettingsScreen();
					}*/

				}else{
					Log.info("Entered History with value:" + token + " But current user is not logged in. Reload Page...");
					Window.Location.reload();	
				}
			}else if(token.equals("")){
				boolean val = Window.confirm("Are you sure you want to go back, this will log you out.");
				if(val){
					Log.info("Entered History with empty token. Reload Page..");
					Window.Location.reload();	
				}else{
					Log.info("Dont do anything. The user wants to stay in this amazing app.");
				}
			}
		}else{
			Log.info("Entered History with token null. Reload Page..");
			Window.Location.reload();
		}
	}
}
