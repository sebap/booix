<?php

System::import("database/MySQL.php");
System::import("service/Response.php");
System::import("facebook/Facebook.php");
System::import("webpage/WebPageManager.php");
System::import("enums/RESPONSE_TYPE.php");
/**
 * This class is used to manage the users
 */
class UserManager{
	
	/**
	 * This method is used for two purposes.
	 * If the user is new, it will create a new account for him, and log him in.
	 * If the user is a customer already will log him in
	 * @param unknown_type $user
	 */
	public static function facebookUserLogin($user){
		$response = new Response();
		list ($isRegistered,$user) = UserManager::isUserRegistered($user);

		if ($isRegistered){
			$response->setType(RESPONSE_TYPE::$USER_LOGIN_SUCCESSED);
			$response->setObject($user);
			return $response;
		}else{
			UserManager::store($user);
			$response->setType(RESPONSE_TYPE::$USER_LOGIN_SUCCESSED);
			$response->setObject($user);
			return $response;
		}
	}

	public static function booixUserlogin($user){
		$response = new Response();
		list ($validUser,$user) = UserManager::isValidUser($user);
		
		if ($validUser){
			UserManager::setSessionValues($user);
			$response->setType(RESPONSE_TYPE::$USER_LOGIN_SUCCESSED);
			$response->setObject($user);
			return $response;
		}else{
			$response->setType(RESPONSE_TYPE::$USER_LOGIN_FAILED);
			return $response;
		}
	}
	
	/**
	 * It is not being used now since we are using only Facebook account
	 * @param unknown_type $user
	 */
	private static function setSessionValues($user){
		$_SESSION['email'] = $user->getEmail();
		$_SESSION['password'] = $user->getPassword();
		$_SESSION['userType'] = $user->getUserType();
	}
	
/**
	 * This method will check the user and if it is valid it will update 
	 * the user with the id and load the webpages and return the updated object
	 * @param array (boolean , User) true or false
	 */
	public static function isUserRegistered($user){
		$email = $user->getEmail();
		
		if (empty($email)){
			return array (FALSE, $user); //we can't continue if the email is empty
		}else{
			$db = new MySQL();
			$sql = "select * from users where email='$email'";
			$result = $db->querySelect($sql);
			$cant = $db->getLastQueryCount();
			$db->disconnect();
			if($cant > 0){
				if (($user->getEmail()==$result[0]["email"]) && ($user->getPassword()==$result[0]["password"])){
					$user->setId($result[0]["id"]);
					$user->setWebPageList(WebPageManager::getLightWebPageList($user));
					return array (TRUE, $user);
				}
			}
			return array (FALSE, $user);
		}
	}

	/**
	 * This method will check the user and if it is valid it will update 
	 * the user id and return the updated user object
	 * @param array (boolean , User) true or false
	 */
	public static function isValidUser($user){
		$email = $user->getEmail();
		$password = $user->getPassword();
		if (empty($email) or empty($password)){
			return array(FALSE, $user);
		}else{
			$db = new MySQL();
			$sql="select * from users where email='$email'";
			$result=$db->querySelect($sql);
			$cant = $db->getLastQueryCount();
			$db->disconnect(); 
			if( $cant > 0){
				if (($user->getEmail()==$result[0]["email"]) && ($user->getPassword()==$result[0]["password"])){
					$user->setId($result[0]["id"]);
					$user->setWebPageList(WebPageManager::getLightWebPageList($user));
					return array (TRUE, $user);
				}
			}
			return array (FALSE, $user);
		}
	}
	
	public static function store($user){
		$response = new Response();
		$db = new MySQL();
		$sql="select * from users where email='".$user->getEmail()."'";
		$result=$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		if( $cant > 0){
			$response->setType(RESPONSE_TYPE::$USER_STORE_ALREADY_EXIST);
			return $response;
		}

		$db->connect();
		$sql="Insert into users (email, password) values ('".$user->getEmail()."','".$user->getPassword()."') ";
		$db->query($sql);
		$error = $db->ErrorSql();
		$inserId = $db->insert_id();
		$db->disconnect(); 
		if ( $error == 0){
			$user->setId($inserId);
			$response->setType(RESPONSE_TYPE::$USER_STORE_SUCCESS);
			$response->setObject($user);
			UserManager::createUserHome($user);
			return $response;
		}else{
			
			$response->setType(RESPONSE_TYPE::$USER_STORE_FAIL);
			$response->setMessage("Failed to store the user");
			return $response;
		}
	}
	
	private static function createUserHome($user){
		//First create user home
		@mkdir(PATH_TO_BOOIX_PAGES."user".$user->getId());
		
		//We will not automatically create one webpage
		//instead we will ask the user to create one.
		//UserManager::createWebPage($user->getId());
	}

	/*
	 * This method is used to validate all the time if the user is still logged in
	 */
	public static function isUserLoggedIn(){
		
		if($_SESSION["userType"] == USER_TYPE::$BOOIX){
			if(isset($_SESSION["email"]) && isset($_SESSION["password"]) ){
				return true;
			}else{
				return false;
			}
		}else{
			// Create our Application instance (replace this with your appId and secret).
			$facebook = new Facebook(array( 'appId'  => FACEBOOK_APP_ID, 'secret' => FACEBOOK_APP_SECRET,'cookie' => true,));
			$session = $facebook->getSession();
			if($session != null){
				return true;
			}else{
				return  false;
			}
		}
	}
}
?>