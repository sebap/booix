<?php
System::import("enums/ACCOUNT_TYPE.php");
System::import("enums/USER_TYPE.php");

class User{

	private $id = 0;
	private $firstName = null;
	private $lastName = null;
	private $dateOfBirth = null;
	private $email = null;
	private $password = null;
	private $userType = null;
	private $accountType = null;
	private $webPageList = null;
	
	function __construct(){
		//By Default set the Account_Type to FREE
		$this->accountType = ACCOUNT_TYPE::$FREE;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}
	
	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}
	
	public function setUserType($userType){
		$this->userType = $userType;
	}
	
	public function getUserType(){
		return $this->userType;
	}
	
	public function setWebPageList($webPageList) {
		$this->webPageList = $webPageList;
	}
	
	public function getWebPageList(){
		return $this->webPageList;
	}

	/*
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	

	public void setAccountType(ACCOUNT_TYPE type){
		this.type = type; 
	}
	public ACCOUNT_TYPE getAccountType(){
		return type;
	}*/
	
	public function toXML(){
		$xml = "<user>";
		$xml .= "<id>".$this->id."</id>";
		$xml .= "<userType>".$this->getUserType()."</userType>";
		$xml .= "<firstName>".$this->firstName."</firstName>";
		$xml .= "<lastName>".$this->lastName."</lastName>";
		$xml .= "<email>".$this->getEmail()."</email>";
		$xml .= "<password>".$this->getPassword()."</password>";
		$xml .= "<accountType>".$this->accountType."</accountType>";
		$xml .= "<webPages>";
		//we need this validation because if the user is new won't have any webpage
		if(isset($this->webPageList)){
			foreach ($this->webPageList as $currentWebPage){
				$xml .= $currentWebPage->toXML();
			}
		}
		$xml .= "</webPages>";
		$xml .= "</user>";
		return $xml;
	}

}
?>