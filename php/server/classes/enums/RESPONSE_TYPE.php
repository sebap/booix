<?php

/**
 * This class is like a enum for setting the type of response we are sending back to the client
 * @author Owner
 *
 */
class RESPONSE_TYPE{
	
	//The variables are of the type <service>_<action>_<message>
	public static $GENERIC_FAILURE = "GENERIC_FAILURE"; //We will respond with this one if something whent wrong on any service
	
	public static $USER_STORE_SUCCESS = "USER_STORE_SUCCESS";
	public static $USER_STORE_FAIL = "USER_STORE_FAIL";
	public static $USER_STORE_ALREADY_EXIST = "USER_STORE_ALREADY_EXIST";

	public static $USER_LOGIN_SUCCESSED = "USER_LOGIN_SUCCESSED";
	public static $USER_LOGIN_FAILED = "USER_LOGIN_FAILED";
	
	public static $WEBPAGE_STORE_STORED = "WEBPAGE_STORE_STORED";
	public static $WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE = "WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE";
	public static $WEBPAGE_DELETE_DELETED = "WEBPAGE_DELETE_DELETED";
	public static $WEBPAGE_LOAD_SUCCESSFUL = "WEBPAGE_LOAD_SUCCESSFUL";
	
	public static $DOMAIN_CHECK_VALID = "DOMAIN_CHECK_VALID";
	public static $DOMAIN_CHECK_INVALID = "DOMAIN_CHECK_INVALID";
	
	

}