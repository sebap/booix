<?php
/**
* Example of use <br>
* $sql = new Cmysql();<br>
* $sql->SetHostNamePassAndDB( "localhost", "user", "pass", "test_db" );<br>
* $sql->Connect();<br>
* $arr = $sql->QuerySelect( "SELECT * FROM table" );<br>
* print_r( $arr );<br>
*
* @package Database
* @author Sebastian Pereyro
* @version 2004-01-09
*/
class MySQL 
{
	var $link;
    var $host;
    var $name;
    var $password;
    var $database;

    var $lastQuery;
	var $lastQueryCount;
    var $isConnected;

    function MySQL() 
	{
       	$this->isConnected = false;
       	$lastQuery = "There is no sql query defined yet.";
       	$this->setHostNamePassAndDB(MYSQL_HOST, MYSQL_NAME, MYSQL_PASS, MYSQL_DBASE);
       	$this->connect();
    }
        
    /**
    * @param String The host name or ip address. 127.0.0.1 or localhost for the localhost
    * @param String The username to access the database
    * @param String The password to access the database
    * @param String The database name to be selected when the connection is set.
    */        
    function setHostNamePassAndDB($host, $name, $password, $database)
	{
    	$this->host = $host;
    	$this->name = $name;
        $this->password = $password;
    	$this->database = $database;
    	//echo "Initializing";
    }
        
    /**
   	* @param String Change the database to this new name.
   	*/            
	function setDB($database) 
	{
   		//if( $database == "medidores" ) $database = "medidores_web";
   		$this->database = $database;
       	if($this->isConnected)
       		mysql_select_db($this->database,$this->link) or die ("base_datos no");
   	}
        
    /**
	* ESTA FUNCION ES EXACTAMENTE IGUAL A LA ANTERIOR (SetDB)
    * @param String Change the database to this new name.
    */              
    function useDB($database) 
	{
    	$this->database = $database;
    	if($this->isConnected)
        	mysql_select_db($this->database,$this->link) or die ("base_datos no");
    }

	/**
    * 
    */
    function isConnected() 
	{
    	return $this->isConnected;
    }
	
	/**
    * 
    */
    function connect()
    {
    	if($this->link = mysql_connect($this->host,$this->name,$this->password))
      		$this->isConnected = true;
    	if($this->isConnected)
      		mysql_select_db($this->database,$this->link) or die ("Wrong database");
    }
	
	/**
    * Disconnects the object from the DB server 
    */
    function disconnect(){
		mysql_close($this->link);
    }
    
    
    /**
     * Caution NOT TESTED This is the new generation query. It is smart enough to know if it returned TRUE, FALSE or Resource
     * @param $sql
     * @return Array: This method will returns an array in the format (resource, TRUE | FALSE ) 
     * 
     */
    public function execute($sql){
    	$this->lastQuery = $query;
    	$result = mysql_query($query);
    	if( $result == TRUE || $result == FALSE  ){ 
    		return array( $result , null);
    	}else{ // it is a query that returns a resource
    		while ($row = mysql_fetch_array($result)){
            	$resultArray[] = $row;
            }
            mysql_free_result ($result);
    	}
    	
    	if(!isset($resultArray)){
    		$this->lastQueryCount = 0;
		}else{
			$this->lastQueryCount = count($resultArray);
		}

		return array( TRUE, $resultArray);
    }

    /**
    * @param String A one-line sql query. Eg. SELECT * FROM table
    * @return Array An associative and scalar array with all the fields of the table.
    */            
    function querySelect($query)
	{
		$this->lastQuery = $query;
        $result = mysql_query($query);
		if ($this->ErrorSql() == 0)
		{
        	while ($row = mysql_fetch_array($result)) 
			{
            	$arrayResult[]=$row;
            }
            mysql_free_result ($result);
        }
        if(!isset($arrayResult)){
			$this->lastQueryCount = 0;
		}else{
			$this->lastQueryCount = count($arrayResult);
		}
		return $arrayResult;
    }
	
	/**
	* return the count of rows of the lastQuery();
	*/
	function getLastQueryCount(){
		return $this->lastQueryCount;
	}
	
	/**
    * @param String with sql query. Eg. SELECT * FROM table
    * @return integer with amount of registries be foundeed
    */            
    function queryRowsNum($query)
	{
    	$this->lastQuery = $query;
        $result = mysql_query($query);
        if ($this->ErrorSql() == 0){
        	$cant = mysql_num_rows($result);
            mysql_free_result ($result);
        }       
		return $cant;
    }

    /**
    * @param String A one-line sql query. Eg. SELECT * FROM table
    * @return Array A scalar array with all the fields of the table.
    */              
    function querySelectRow($query) 
	{
		$this->lastQuery = $query;
    	$result = mysql_query($query);
    	if ($this->ErrorSql() == 0)
		{
       		$pos = strpos(strtolower($query), "select");
      		if (($pos > 0 && $pos < 2) || $pos === 0) //$a === $b (identidad). Cierto si $a es igual a $b y si son del mismo tipo (s�lo PHP4) (Marce)
			{  // Preguntar si es un comando tipo SELECT
       			while ($row = mysql_fetch_row($result)){
            		$arrayResult[]=$row;
       			}
      			mysql_free_result ($result);
      		}
    	}
    	if(!isset($arrayResult)){
			$arrayResult = "unset";
			$this->lastQueryCount = 0;
		}else{
			$this->lastQueryCount = count($arrayResult);
		}
			
    	return $arrayResult;
    }
        
    /**
	* Execute queries that do not return a result of data.<br/>
    * @param String A one-line sql query. Eg. SELECT * FROM table
    * @return int 0: if the query was executed without returning any errors.
    */      
    function query($query) 
	{
    	$this->lastQuery = $query;
        return mysql_query($query);
    }
        
    /**
    * @param String A one-line sql query. Eg. SELECT * FROM table
    * @return Array An associative and scalar array with all the fields of the first row
    */            
    function queryOneLine($query) 
	{
		$this->lastQuery = $query;
        $result = mysql_query($query);
        if ($this->ErrorSql() == 0)
		{
        	$row = mysql_fetch_array($result);
        	$arrayResult = $row;
        }
        mysql_free_result($result);
        
		return $arrayResult;
    }
        
    /**
    * @param String A one-line sql query. Eg. SELECT * FROM table
    * @return Array A scalar array with all the fields of the first row
    */              
	function queryOneLineRow($query) 
	{
    	$this->lastQuery = $query;
        $result = mysql_query ($query);
    	if ($this->ErrorSql() == 0)
		{
        	$row = mysql_fetch_row($result);
            $arrayResult = $row;
        }
        mysql_free_result ($result);
        
		return $arrayResult;
    }
        
    /**
    * @return int 0: There was no error.
    */      
	function errorSql() 
	{
        if (mysql_errno() != 0)
        	echo '<br><b><font color="#FF0000">'.mysql_errno().": ".mysql_error().'</font></b><br>';
        
		return mysql_errno();
    }
	
	/*
	*Devuelve el ultimo id (auto_increment) insertado 
	*en la base de datos (insert)
	*/ 
	function insert_id()
	{
		return mysql_insert_id($this->link); 
	}
}
?>
