<?php
/*header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");*/
ini_set("display_errors",True);
ini_set("error_reporting", E_ALL & ~E_NOTICE);

/*Remote
define( "MYSQL_HOST", host
define( "MYSQL_NAME", "name");
define( "MYSQL_PASS", "pass");
define( "MYSQL_DBASE", "db");/**/

/* Local */ 
define( "MYSQL_HOST", "localhost");
define( "MYSQL_NAME", "root" );
define( "MYSQL_PASS", "password" );
define( "MYSQL_DBASE", "booix" );/**/

//CPANEL CONFIG: ths is used to access some cpanel functionality from the code
define( "CPANEL_USER", "user");
define( "CPANEL_PASS", "pass" );
define( "CPANEL_SKIN", "skin" );
define( "CPANEL_DOMAIN", "domain" );



//Relative Paths
define("ROOT_CLASSES", 								dirname(__FILE__) . "/");//get /home/http/sitio.com/uzgals/classes/
$root=explode($_SERVER["DOCUMENT_ROOT"],			ROOT_CLASSES); 
$root=explode("classes/",							$root[0]); // obtengo /server/classes/
define("PATH_TO_BOOIX_ROOT",			 			$root[0]);// obtengo /server/
define("PATH_TO_BOOIX_PAGES",						PATH_TO_BOOIX_ROOT."../../../webpages/"); // /home/booix.com/webpages/


/**
 * Example of use: 
 * The following line sets a log with PRIORITY_HIGH
 * Csystem::log( "hola mundo", PRIORITY_HIGH);
 * 
 */

class System
{
    /**
     * 
     * @param String $ The name of the class, included the path from the Classes path.
     */
    public static function import($class)
    {
        $classPath = ROOT_CLASSES . $class;
        include_once $classPath;
    } 
}
?>