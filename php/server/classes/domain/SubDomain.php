<?php
System::import("user/UserManager.php");

class SubDomain{

	private $subdomain = "";
	
	public function getSubDomain(){
		return $this->subdomain;
	}

	public function setSubDomain($subdomain) {
		$this->subdomain = $subdomain;
	}

	public function toXML(){
		$xml = "<subdomain>".$this->getSubDomain()."</subdomain>";
		return $xml;
	}

}
?>