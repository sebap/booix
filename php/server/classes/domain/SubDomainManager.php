<?php
System::import("database/MySQL.php");
System::import("service/Response.php");
System::import("enums/RESPONSE_TYPE.php");

/**
 * This class is used to manage domain and subdomains
 */

class SubDomainManager{

	public static function check($subdomain){
		$response = new Response();
		$response->setObject($subdomain);
		
		$db = new MySQL();
		$sql="select * from webpages where subdomain='".$subdomain->getSubDomain()."'";
		$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		if( $cant > 0){
			$response->setType(RESPONSE_TYPE::$DOMAIN_CHECK_INVALID);
			return $response;
		}else{
			$response->setType(RESPONSE_TYPE::$DOMAIN_CHECK_VALID);
			return $response;
		}
	}
}
?>