<?php

/**
 * This class can be used to wrap a response with an object. Please look at tht toXML method to see
 * what this class can return.
 * @author Owner
 *
 */
class Response{
	private $type = null;
	private $object = null;
	private $msg = null;
	
	public function setType($type){
		$this->type = $type;
	}
	
	public function getType(){
		return $this->type;
	}

	public function setObject($object){
		$this->object = $object;
	}
	
	public function getObject(){
		return $this->object;
	}
	
	public function setMessage($message){
		$this->msg = $message;
	}
	
	public function getMessage(){
		return $this->msg;
	}
	
	public function toXML(){
		$xml .= "<response>";
		$xml .= "<type>".$this->getType()."</type>";
		$xml .= "<message>".$this->getMessage()."</message>";
		if(isset($this->object)){
			$xml .= "<object>".$this->getObject()->toXML()."</object>";	
		}else{
			$xml .= "<object></object>";
		}
		$xml .= "</response>";
		return $xml;
	}
}

?>