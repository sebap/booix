<?php
System::import("database/MySQL.php");
System::import("user/User.php");
System::import("webpage/WebPage.php");
System::import("enums/RESPONSE_TYPE.php");
System::import("cpanel/xmlapi.php");

/**
 * This class is used to manage the webpages
 */
class WebPageManager{

	/**
	 * This is mainly to have all the webpages related to the user
	 * we cannot use cdata within cdata because it will take the first end delimiter as the delimiter of the first one.
	 * @param User $user
	 */
	public static function getLightWebPageList($user){
		$webPageList;
		$db = new MySQL();
		$sql="select * from webpages where user_id=".$user->getId();
		$result=$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		for($i = 0;$i < $cant ;$i++){
			$webPage = new WebPage();
			$webPage->setId($result[$i]["id"]);
			$webPage->setUserId($result[$i]["user_id"]);
			$webPage->setSubdomain($result[$i]["subdomain"]);
			$webPageList[$i] = $webPage;
		}
		return $webPageList;
	}
	
	public static function store($webPage, $webPageXML, $webPageHTML){
		
		$response = new Response();
		
		$db = new MySQL();
		//validate subdomain
		//get the current subdomain
		$sql="select * from webpages where id=".$webPage->getId()." and user_id=".$webPage->getUserId();
		$result=$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		if( $cant > 0){
			$oldSubdomain = $result[0]["subdomain"];
		}else{
			$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
			$response->setMessage("There is no webpage");
			return $response;
		}
		//At this point we know the current subdomain
		if($oldSubdomain != $webPage->getSubdomain()){
			//validate if the new subdomain is available
			$db->connect();
			$sql = "select * from webpages where subdomain='".$webPage->getSubdomain()."'";
			$result=$db->querySelect($sql);
			$cant = $db->getLastQueryCount();
			$db->disconnect();
			if($cant > 0){
				//Here we know the subdomain is not available
				$response->setType(RESPONSE_TYPE::$WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE);
				return $response;
			}else{
				//store the new subdomain
				$db->connect();
				$sql = "update webpages set subdomain='".$webPage->getSubdomain()."' where id=".$webPage->getId()." and user_id=".$webPage->getUserId();
				$db->query($sql);
				$db->disconnect();
				//create subdomain in hostmonster
				WebPageManager::createSubdomain($webPage);
				if($oldSubdomain != ""){
					WebPageManager::deleteSubdomain($oldSubdomain);
				}
			}
		}

		//Clean XMLs files
		$webPageXML = str_replace("\\","",$webPageXML);
		$webPageHTML = str_replace("\\","",$webPageHTML);
	
		$webPageFolderURL = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId() ;
		$webPageXMLURL = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId()."/webpage.xml";
		$webPageHTMLfolderURL = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId()."/";

		//Clean previous files
		WebPageManager::deleteAllFiles($webPageFolderURL);
		
		//Now save it
		WebPageManager::writeFile($webPageXMLURL, $webPageXML );
		
		$obj = simplexml_load_string($webPageHTML);
		$i = 0;
		foreach($obj as $element){
			if($i == 0){
				$content = "<?php header( 'Location: ".$element->name[0]."' ) ; ?>";
				WebPageManager::writeFile($webPageHTMLfolderURL."index.php", $content);
			}
			WebPageManager::writeFile($webPageHTMLfolderURL.$element->name[0], $element->html[0]);
			$i++;
		}

		$response->setType(RESPONSE_TYPE::$WEBPAGE_STORE_STORED);
		return $response;
	}
	
	/**
	 * 
	 * @param $webPageId
	 * @return WebPage object or null if the id does not exists
	 */
	private static function getWebPageFromDB($webPageId){
		$db = new MySQL();
		$sql = "select * from webpages where id=".$webPageId."";
		$result=$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		if($cant > 0){
			$webPage = new WebPage();
			$webPage->setId($result[0]["id"]);
			$webPage->setUserId($result[0]["user_id"]);
			$webPage->setSubdomain($result[0]["subdomain"]);
			//$webPage->setSource(WebPageManager::getWebPageSource($webPage));
			return $webPage; 
		}else{
			return null;
		}
	}
	
	/**
	 * This method returns the web page xml.
	 * @param unknown_type $webPage
	 */
	private static function getWebPageSource($webPage){
		
		//A better implementation would be to store the webpage in the database.
		
		$url = PATH_TO_BOOIX_PAGES."/user".$webPage->getUserId()."/page".$webPage->getId()."/webpage.xml";
		$webpagexml = file_get_contents($url);
		
		
		//this is a safety check so we are sure the no process has the handle on this file
		for ($i = 0; $i <= 3; $i++) {
			if($webpagexml == false ){
				//we will sleep 4 seconds and try again
				sleep(5);
				$webpagexml = file_get_contents($url);
			}else{
				break;
			}
		}

		return stripslashes($webpagexml);
		
	}
	
	
	/**
	 * This method is used to create a new webpage for the user
	 * @param $webPage
	 */
	public static function delete($webPage){
		
		$response = new Response();
		//make sure we get the latest data from DB
		$webPage = WebPageManager::getWebPageFromDB($webPage->getId());
		
		if($webPage == null){
			$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
			$response->setMessage("Getting values from db");
			return $response;
		}
		
		//Delete it from DB
		$db = new MySQL();
		$sql = "delete from webpages where id=".$webPage->getId();
		$db->query($sql);
		$err = $db->errorSql();
		$db->disconnect();
		if($err != 0){
			$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
			$response->setMessage("Error deleting from database". $sql);
			return $response;
		}
		
		// We will clean up the folder firt
		$webPageFolderURL = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId() ;
		if(!WebPageManager::recursiveRemoveDirectory($webPageFolderURL)){
			$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
			$response->setMessage("Error deleting the folders");
			return $response;
		}else{
			//delete subdomain, so we can reuse it
			//we do not remove it here call it directly independently
			//WebPageManager::deleteSubdomain($webPage->getSubdomain());
		}

		$response->setType(RESPONSE_TYPE::$WEBPAGE_DELETE_DELETED);
		$response->setMessage("Success");
		$response->setObject($webPage);
		return $response;
	}
	
	/**
	 * This method will load a webpage.
	 * Th
	 * @param WebPage $webPage
	 * @return web page xml content. We cannot send a response object because the cdata section issue.
	 * 
	 */
	public static function load($webPage){
		//Get the latest webPage Data from DB
		$webPage = WebPageManager::getWebPageFromDB($webPage->getId());
		
		//get the webpage xml to return to the client.
		$webPageXML = WebPageManager::getWebPageSource($webPage);
		echo $webPageXML;
	}
	
	
	/**
	 * This method is used to create a new webpage for the user
	 * @param $webPage
	 */
	public static function create($webPage){
		
		$response = new Response();

		$db = new MySQL();
		//validate if the new subdomain is available
		$sql = "select * from webpages where subdomain='".$webPage->getSubdomain()."'";
		$result=$db->querySelect($sql);
		$cant = $db->getLastQueryCount();
		$db->disconnect();
		if( $cant > 0){
			//Here we know the subdomain is not available
			$response->setType(RESPONSE_TYPE::$WEBPAGE_STORE_DOMAIN_NOT_AVAILABLE);
			$response->setMessage("Domain is not available");
			return $response;
		}else{
			//this will update the id too
			$webPage = WebPageManager::createWebPage($webPage);
			//WebPageManager::createSubdomain($webPage);
		}
		
		$response->setType(RESPONSE_TYPE::$WEBPAGE_STORE_STORED);
		$response->setMessage("Page Stored Sucessfully");
		$response->setObject($webPage);
		return $response;
	}
	
	/**
	 * Used to create a new page
	 * @param $userId
	 * @param $subdomain
	 * @return newly created page id
	 */
	public static function createWebPage($webPage){
		$sql="insert into webpages (user_id, subdomain) values (".$webPage->getUserId().", '".$webPage->getSubdomain()."')";
		$db = new MySQL();
		$db->query($sql);
		$webPage->setId($db->insert_id());
		$db->disconnect();
		WebPageManager::recurseCopy(PATH_TO_BOOIX_PAGES."default_page/", PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page". $webPage->getId() );
		$webPageXML = WebPageManager::getWebPageSource($webPage);
		$webPageXML = str_replace("#webPageId#", $webPage->getId() , $webPageXML);
		$webPageXML = str_replace("#subdomain#", $webPage->getSubdomain() , $webPageXML); 
		$webPageXMLURL = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId()."/webpage.xml";
		WebPageManager::writeFile($webPageXMLURL, $webPageXML );
		return $webPage;
	}
	
	private static function recurseCopy($src,$dst) { 
	    $dir = opendir($src); 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) ) { 
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) { 
	                WebPageManager::recurseCopy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	            else { 
	                copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	        } 
	    } 
	    closedir($dir); 
	}
	
	/**
	 * This method will delete only the files find in a specified directory
	 * @param path to a dir $dir
	 */
	private static function deleteAllFiles($dir) {
	    $mydir = opendir($dir);
	    while(false !== ($file = readdir($mydir))) {
	        if($file != "." && $file != "..") {
	            chmod($dir."/".$file, 0777);
	            if(is_dir($dir."/".$file)) {
	            	//We are not deleting folders here sir.
	            	/*chdir('.');
	                destroy($dir.$file.'/');
	                rmdir($dir.$file) or DIE("couldn't delete $dir$file<br />");*/
	            }
	            else{
	            	unlink($dir."/".$file) or DIE("couldn't delete $dir$file<br />");
	            }
	                
	        }
	    }
	    closedir($mydir);
	}
	
	
	/*
	 * this method will delete the whole directory and its content
	 */
	public static function recursiveRemoveDirectory($directory, $empty=FALSE){
	     if(substr($directory,-1) == '/')
	     {
	         $directory = substr($directory,0,-1);
	     }
	     if(!file_exists($directory) || !is_dir($directory))
	     {
	         return FALSE;
	     }elseif(is_readable($directory))
	     {
	         $handle = opendir($directory);
	         while (FALSE !== ($item = readdir($handle)))
	         {
	             if($item != '.' && $item != '..')
	             {
	                 $path = $directory.'/'.$item;
	                 if(is_dir($path)) 
	                 {
	                     WebPageManager::recursiveRemoveDirectory($path);
	                 }else{
	                     unlink($path);
	                 }
	             }
	         }
	         closedir($handle);
	         if($empty == FALSE)
	         {
	             if(!rmdir($directory))
	             {
	                 return FALSE;
	             }
	         }
	     }
	     return TRUE;
	}

	private static function writeFile($filename , $content){
		$fh = fopen($filename, 'w') or die("can't open file");
		fwrite($fh, $content);
		fwrite($fh, $stringData);
		fclose($fh);
	}

	/**
	 * This method has given me nightmares. it seems that the process of creating a subdomain, creates
	 * many resources. This process can take long time, so if the user tries to access the webpage that is in the same
	 * fodler can lead to errors.
	 * This can be fixed by storing the web page in the database, not in the filesystem.
	 * 
	 * One of the problems we have with this method is that the execution of this creation of the subdomain, returns an error.
	 * I could not figure out what was the issue, but it actually creates the subdomain. I think Hostmonster is not quite ready for this
	 * but as a work around, we will trust it will work everytime, and we will ignore the response from this method.
	 * 
	 * @param WebPage $webPage
	 */
	public static function createSubdomain($webPage){
		/*$dir = PATH_TO_BOOIX_PAGES."user".$webPage->getUserId()."/page".$webPage->getId() ;
		$dir = "public_html/booix/webpages/user".$webPage->getUserId()."/page".$webPage->getId() ;
		$url = "http://".CPANEL_USER.":".CPANEL_PASS."@".CPANEL_DOMAIN.":2082/frontend/".CPANEL_SKIN."/subdomain/doadddomain.html?";
		$url = $url . "domain=".$webPage->getSubdomain()."&rootdomain=".CPANEL_DOMAIN."&dir=".$dir."&go=Create";*/

		/*$xmlapi = new xmlapi(CPANEL_DOMAIN);
		$xmlapi->set_port(2082);
		$xmlapi->password_auth(CPANEL_USER,CPANEL_PASS);

		//Check if fork exists
		if ( function_exists('pcntl_fork') ){
			$pid = pcntl_fork();
			if($pid) { //parent
				//we do not want the next line because it causes the process to fail
				//pcntl_wait($status);
			}else { //child
				$xmlapi->api1_query(CPANEL_USER,'SubDomain','addsubdomain',array($webPage->getSubdomain(),CPANEL_DOMAIN,0,0,"public_html/booix/webpages/user".$webPage->getUserId()."/page".$webPage->getId()));
				exit(0);
			}
		}else{
			$result = $xmlapi->api1_query(CPANEL_USER,'SubDomain','addsubdomain',array($webPage->getSubdomain(),CPANEL_DOMAIN,0,0,"public_html/booix/webpages/user".$webPage->getUserId()."/page".$webPage->getId()));
		}*/
		//$result = $xmlapi->api1_query(CPANEL_USER,'SubDomain','addsubdomain',array($webPage->getSubdomain(),CPANEL_DOMAIN,0,0,"public_html/booix/webpages/user".$webPage->getUserId()."/page".$webPage->getId()));
		
		
		/**
		 * @link http://docs.cpanel.net/twiki/bin/view/ApiDocs/WebHome
		 */
		//Creating a subdomain via xml api
		$xmlapi = new xmlapi(CPANEL_DOMAIN);
		$xmlapi->set_port(2082);
		$xmlapi->password_auth(CPANEL_USER,CPANEL_PASS);
		$result = $xmlapi->api1_query(CPANEL_USER,'SubDomain','addsubdomain',array($webPage->getSubdomain(),CPANEL_DOMAIN,0,0,"public_html/booix/webpages/user".$webPage->getUserId()."/page".$webPage->getId()));
		print_r ($result);
		return $result;
	}
	
	/**
	 * This method is used to delete a subdomain from hostmonster.
	 * This method can be moved to the SubdomainManager class. but for time constraint we leave it here.
	 * @param String $subdomain
	 */
	public static function deleteSubdomain($subdomain){
		$url = "http://".CPANEL_USER.":".CPANEL_PASS."@".CPANEL_DOMAIN.":2082/frontend/".CPANEL_SKIN."/subdomain/dodeldomain.html?";
		$url = $url . "domain=".$subdomain."_".CPANEL_DOMAIN;
		$result = file_get_contents($url);
		print_r ($result);
		return $result;
		
		/*
		 * This portion of code is kept in kere justa s a reference and for informational purposes.
		 * we already tried to fork the process in the server but didnt work, so
		 * the best alternative is to wait for the response from hostmonster, eventhought it is not a clean response
		 * we can ignore ir
		 * 
		if ( function_exists('pcntl_fork') ){
			$pid = pcntl_fork();
			if($pid) { //parent
				//we do not want the next line because it causes the process to fail
				//pcntl_wait($status);
			}else { //child
				$result = file_get_contents($url);
				if ($result === false) 
				{ 
					//retry
				 	$result = file_get_contents($url);
				} else { 
					exit(0); 
				} 
			} 
		}else{
			file_get_contents($url);
		}*/
		
		
		
		
		/**
		 * @link http://docs.cpanel.net/twiki/bin/view/ApiDocs/WebHome
		 * This also did not work
		 */
		//Deleting a subdomain via xml api
		/*$xmlapi = new xmlapi(CPANEL_DOMAIN,CPANEL_USER,CPANEL_PASS);
		$xmlapi->set_port(2083);
		$result = $xmlapi->api2_query(CPANEL_USER,'SubDomain','delsubdomain',array($subdomain."_".CPANEL_DOMAIN));
		print_r ($result);
		return $result;*/
	}
}
?>