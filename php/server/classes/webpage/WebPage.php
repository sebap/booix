<?php

class WebPage{
	private $id = 0;
	private $userId = 0;
	private $subdomain = null;
	private $source = null; //this will contain the webpage source in xml format
	
	
	public function getId(){
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}
	
	public function getUserId(){
		return $this->userId;
	}

	public function setUserId($userId) {
		$this->userId = $userId;
	}
	
	public function getSubdomain(){
		return $this->subdomain;
	}

	public function setSubdomain($subdomain) {
		$this->subdomain = $subdomain;
	}

	public function getSource(){
		return $this->source;
	}

	public function setSource($source) {
		$this->source = $source;
	}
	
	public function toXML(){
		$xml = "<webpage>";
		$xml .= "<id>".$this->getId()."</id>";
		$xml .= "<userId>".$this->getUserId()."</userId>";
		$xml .= "<subdomain>".$this->getSubdomain()."</subdomain>";
		$xml .= "<source><![CDATA[".$this->getSource()."]]></source>";
		$xml .= "</webpage>";
		return $xml;
	}
	
	
}