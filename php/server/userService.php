<?php
include_once("classes/System.php");
System::import("user/User.php");
System::import("user/UserManager.php");
System::import("enums/SERVICE_ACTION_TYPE.php");

session_start();

//Get Variables
$email = $_POST["email"];
$password = $_POST["password"];
$action = $_POST["action"];
$userType = $_POST["userType"];

$user = new User();
$user->setEmail($email); 
$user->setPassword($password);
$user->setUserType($userType);

if($user->getUserType() == USER_TYPE::$BOOIX){
	if ($action == SERVICE_ACTION_TYPE::$BOOIX_USER_LOGIN){
		$response = UserManager::booixUserLogin($user);
		echo $response->toXML();
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$USER_STORE){
		$response = UserManager::store($user);
		echo $response->toXML();
		exit;
	}
}else if ($user->getUserType() == USER_TYPE::$FACEBOOK){
	if(UserManager::isUserLoggedIn()){
		if ($action == SERVICE_ACTION_TYPE::$FACEBOOK_USER_LOGIN){
			$response = UserManager::facebookUserLogin($user);
			echo $response->toXML();
			exit;
		}
	}else{
		$response = new Response();
		$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
		$response->setMessage("Faceook User not logged in");
		echo $response->toXML();
		exit;
	}
}

$response = new Response();
$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
$response->setMessage("Failed to process request");
echo $response->toXML();
exit;

?>
