<?php

/**
 * This service is only used for checking the 
 */
include_once("classes/System.php");
System::import("user/User.php");
System::import("user/UserManager.php");
System::import("domain/SubDomain.php");
System::import("domain/SubDomainManager.php");
System::import("enums/SERVICE_ACTION_TYPE.php");
System::import("enums/RESPONSE_TYPE.php");

session_start();

$action = $_POST["action"];

if(UserManager::isUserLoggedIn()){
	$subdomainStr = $_POST["subdomain"];
	
	if($action == SERVICE_ACTION_TYPE::$DOMAIN_CHECK){
		$subdomain = new SubDomain();
		$subdomain->setSubDomain($subdomainStr);
		$response = SubDomainManager::check($subdomain);
		echo $response->toXML();
		exit;
	}
}else{
	$response = new Response();
	$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
	echo $response->toXML();
	exit;
}

?>
