<?php
include_once("classes/System.php");
System::import("user/User.php");
System::import("user/UserManager.php");
System::import("enums/SERVICE_ACTION_TYPE.php");

session_start();

if(UserManager::isUserLoggedIn()){

	//get the variables
	$action = $_POST["action"];
	$userId = $_POST["userId"];
	$webPageId = $_POST["webPageId"];
	$subdomain = $_POST["subdomain"];
	$webPageXML = $_POST["webPageXML"];
	$webPageHTML = $_POST["webPageHTML"];
	
	$webPage = new WebPage();
	$webPage->setSubdomain($subdomain);
	$webPage->setUserId($userId);
	$webPage->setId($webPageId);

	if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_STORE){
		$response = WebPageManager::store($webPage, $webPageXML, $webPageHTML);
		echo $response->toXML();
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_CREATE){
		$response = WebPageManager::create($webPage);
		echo $response->toXML();
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_CREATE_SUBDOMAIN){
		$response = WebPageManager::createSubdomain($webPage);
		echo $response;
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_DELETE){
		$response = WebPageManager::delete($webPage);
		echo $response->toXML();
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_DELETE_SUBDOMAIN){
		$response = WebPageManager::deleteSubdomain($webPage->getSubdomain());
		echo $response;
		exit;
	}else if ($action == SERVICE_ACTION_TYPE::$WEBPAGE_LOAD){
		//On this one we cannot send back the response object
		//we need to send the plain xml because of the CDATA section issue.
		//This is an issue that we cannot use CDATA section within CDATA section.
		$response = WebPageManager::load($webPage);
		echo $response;
		exit;
	}

}else{
	$response = new Response();
	$response->setType(RESPONSE_TYPE::$GENERIC_FAILURE);
	$response->setMessage("User is not logged in:");
	echo $response->toXML();
	exit;
}

?>
